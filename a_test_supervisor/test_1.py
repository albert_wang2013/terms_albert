# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/2/25 18:36
@Author  : Albert
@Func    :
@File    : dataPush.py
@Software: PyCharm
"""
import time
from datetime import datetime, timedelta
import logging
import re
import asyncio
import aioredis
from idataapi_transform import ProcessFactory, GetterConfig, WriterConfig


""" 无该条件，就无法输出到控制台 """
logging.basicConfig(level=logging.INFO)


class DataCrawl():
    def __init__(
            self,
            url_type=None,
            push_time=None,
            api_list=None,
            kws_=None,
            if_nohup=None,
            if_html=None):
        self.api_host = 'https://api01.bitspaceman.com/'
        self.apikey = "yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT"
        self.redis_host_local = WriterConfig.WRedisConfig(
            'raw_data', host='127.0.0.1', port=6379, db=1, encoding='utf8')
        self.redis_host_beijing = WriterConfig.WRedisConfig(
            'raw_data',
            host='123.196.116.118',
            port=31001,
            password='wode@beijing',
            db=0,
            encoding='utf8')
        self.push_time = push_time
        self.url_type = url_type
        self.api_list = api_list
        self.kws_ = kws_
        self.if_nohup = if_nohup
        self.if_html = if_html

    async def url_list(self):
        url_list = list()
        if self.url_type == 'kw':
            if not self.kws_:
                redis_ = await aioredis.create_redis_pool('redis://123.196.116.118:31001', db=0,
                                                          password="wode@beijing")
                self.kws_ = await redis_.smembers('weibo_keywords', encoding='utf8')
            for api_ in self.api_list:
                for kw in self.kws_:
                    url = api_.format(kw, self.apikey)
                    url_list.append(url)
            return url_list
        else:
            url_list = self.api_list
            return url_list

    def target_func(self, item):
        """
        :param item: 返回的是一次请求当中的所有items的其中一个item
        func: 判断item的发布时间是否在指定的时间内：是，返回；不是，扔掉
        """
        # logging.info('length of item are:{}'.format(len(item)))   # 判断字典长度
        if item['publishDate']:
            if int(time.time()) - int(item['publishDate']) <= self.push_time:
                return item
        else:
            pass
            # logging.info('error url find:{}'.format())    # 可能错误
            # return item

    def done_if(self, items):
        """
        the APIGetter will automatically fetch next page until max_limit or no more page,
        if you provide a function, APIGetter will terminate fetching next page when done_if(items) return True
        :return: 返回的是一次请求当中的所有items
        func: 判断一次请求当中最后一个item的发布时间是否在指定的时间内：是，继续翻页；不是，停止翻页
        """
        # logging.info('length of items are:{}'.format(len(items)))
        # logging.info(items[-1].keys())
        if items:
            if items[-1]["publishDate"]:
                if int(time.time()) - \
                        int(items[-1]["publishDate"]) <= self.push_time:
                    return False
                return True
        return True

    async def data_deal(self, item):
        """
        判断item当中是否包含html、content字段，如果有html字段，删除原来的content字段，
        将html字段映射到content当中，并将原来的html字段删除。
        e.g: 		del sub['content']
                    item['content'] = item['html']
                    del sub['html']
        """
        dataType, appCode = item['dataType'], item['appCode']
        del item['dataType']
        del item['appCode']
        if self.if_html:
            if str(type(item)) == "<class 'dict'>":
                if 'html' in item:
                    if 'content' in item:
                        del item['content']
                        item['content'] = item['html']
                        del item['html']
            if item['content']:
                if '\n' in item['content']:
                    item['content'] = re.sub(r'\n', '<br>', item['content'])
        if appCode == 'weixinpro3':
            appCode = 'weixinpro'
        rawdata = {
            "interface": "{}/{}".format(dataType, appCode),
            "data": [item],
            "args": {'nodup': 0} if self.if_nohup else {}
        }
        return rawdata

    async def start(self):
        urls_ = []
        datas_ = []
        logging.info(
            'start crawl , time now is: {}'.format(
                datetime.now().replace(
                    microsecond=0)))
        urls = await self.url_list()    # 返回urls列表，用于请求
        logging.info('length of urls are:{}'.format(len(urls)))
        for url in urls:
            # print(url)
            url_generator = GetterConfig.RAPIConfig(
                url,
                keep_other_fields=True,
                max_limit=None,
                done_if=self.done_if,
                filter_=self.target_func,
                trim_to_max_limit=True,
                max_retry=5)
            # 设置了keep_other_fields的时候，每条item数据中都有appcode、datatype，意味着返回的items包含不同的appcode，不能乱搞
            urls_.append(url_generator)
        getter = ProcessFactory.create_getter(GetterConfig.RAPIBulkConfig(
            urls_, concurrency=2))  # Bulk、批量高并发处理程序
        if getter:
            async for items in getter:
                for item in items:
                    data = await self.data_deal(item)
                    if len(datas_) >= 50:  # 每获取50条数据，存储一次进redis
                        await ProcessFactory.create_writer(self.redis_host_beijing).write(datas_)
                        # await
                        # ProcessFactory.create_writer(self.redis_host_local).write(datas_)
                        logging.info(
                            'push datas length {};push-time:{}'.format(
                                len(datas_), time.strftime(
                                    "%Y-%m-%d %H:%M:%S", time.localtime())))
                        datas_ = []
                        datas_.append(data)
                    else:
                        datas_.append(data)     # 包含同一种接口的数据，也可能是不同种接口的数据。
                if len(datas_):
                    await ProcessFactory.create_writer(self.redis_host_beijing).write(datas_)
                    # await
                    # ProcessFactory.create_writer(self.redis_host_local).write(datas_)
                    logging.info(
                        'push datas length {};push-time:{}'.format(
                            len(datas_), time.strftime(
                                "%Y-%m-%d %H:%M:%S", time.localtime())))
                    datas_ = []


if __name__ == '__main__':
    start_ = datetime.now().replace(microsecond=0)
    url_type = 'catid'     # or 走关键词、还是走列表
    push_time = 3600 * 24 * 19 * 30
    api_list = [
        'https://api02.bitspaceman.com/post/facebook?kw={}&apikey={}',
    ]
    api_list = [
        'http://api01.bitspaceman.com:8000/post/weibo?type=all&uid=3579140953&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT',
        'http://api01.bitspaceman.com:8000/post/weixin?uid=gxsky-com&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
        'https://api02.bitspaceman.com/post/twitter?mode=1&source=api&uid=JoeBiden&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT',
        'https://api02.bitspaceman.com/post/facebook?uid=217857556997&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT',
        'https://api02.bitspaceman.com/post/facebook?uid=hhahahhhahaah&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT',
        'https://api02.bitspaceman.com/video/youtube?duration=any&uid=UCLC2sK9RE4qncomjLlEkEqQ&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT',
        'https://api02.bitspaceman.com/post/instagram?uid=753498984&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT',
        'https://api02.bitspaceman.com/post/facebook?uid=46251501064&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT',
        'https://api02.bitspaceman.com/video/youtube?duration=any&uid=UCMpuxj_U02VaHfaPD5Jjx_Q&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT',

    ]
    kws_ = []
    # kws_ = None
    if_nohup = True
    if_html = False
    w = DataCrawl(url_type, push_time, api_list, kws_, if_nohup, if_html)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(w.start())
    end_ = datetime.now().replace(microsecond=0)
    run_times = end_ - start_
    logging.info('schedule run_times: {}'.format(run_times))
    time.sleep(100)

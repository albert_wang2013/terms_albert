# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/5/29 16:19
@Author  : Albert
@Func    : 
@File    : test_3.py
@Software: PyCharm
"""
import time


class Solution(object):
    def uniquePaths(self, m, n):
        """
        :type m: int
        :type n: int
        :rtype: int
        """
        def factorial(num):
            res = 1
            for i in range(1, num+1):
                res *= i
            return res
        return int(factorial(m+n-2) / (factorial(n-1) * factorial(m-1)))



print(Solution().uniquePaths(57,2))
time.sleep(3600*24)
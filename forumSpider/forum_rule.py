# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/4/29 12:19
@Func    :  该脚本的作用，用于为需要采集的论坛指定xpath解析规则，并缓存至redis当中；
            首先，需要存储一个默认的解析规则，然后根据实际采集需求，指定针对性的论坛规则，
            进行相应的存储。
            forum.py脚本先调用指定xpath解析规则，若没定义，则调用默认的解析规则。

            注意事项：防止每一个论坛的解析规则丢失，因为估计论坛有上百个，使用如下两种方式避免
            1、每次添加新论坛时，在forum_rule.json添加解析规则；
            2、将redis设置成rdb持久化（RDB持久化是将当前进程中的数据生成快照保存到硬盘(因此也称作快照持久化)，
            保存的文件后缀是rdb；当Redis重新启动时，可以读取快照文件恢复数据。），命令行输入redis-cli进入客户端，
            输入 bgsave（bgsave命令会创建一个子进程，由子进程来负责创建RDB文件，父进程(即Redis主进程)则继续处理请求）
            详情链接：https://www.cnblogs.com/kismetv/p/9137897.html
@File    : forum_rule.py
@Software: PyCharm
"""
import json
from pprint import pprint
import redis


"""
默认的解析规则：
{
    "host": "http://bbs.wex5.com/",
    "home": "//tbody//dt/a/@href",
    "forumList": "//tbody//th/a[3]/@href",
    "title": "//h1[@class=\"ts\"]//span/text()",
    "contentId": "//div[contains(@id,\"post_\")]/table/@id",
    "publishDate": [
        "//div[@class=\"authi\"]/em/text()",
        "//div[@class=\"authi\"]/em/span/@title"
    ],
    "content": "//div[@class=\"t_fsz\"] | //div[@class=\"pcb\"]",
    "commentCount": "//span[@class=\"xi1\"][2]/text()",
    "viewCount": "//span[@class=\"xi1\"][1]/text()",
    "posterId": "//div[@class=\"pi\"]/div[@class=\"authi\"]/a/@href",
    "posterScreenName": "//div[@class=\"pi\"]/div[@class=\"authi\"]/a/text()",
    "avatarUrl": "//div[@class=\"avatar\"]//img/@src",
    "imageUrl": "//td[@class=\"plc\"]//img/@file",
    "postPage": "//div[@class=\"pgbtn\"]/a/@href",
    "pageSelect": "//div[@class=\"pgbtn\"]//text()"
}
"""


# 为当前论坛设置的解析规则，按照默认的规则模板进行相应的更新
host = 'http://www.discuz.net/'    # 请注意，host需要末尾需要加 /
home = '//tbody//dt/a/@href'
forumList = '//tbody[contains(@id,"normalthread_")]/tr/th/a//@href'
title = '//h1[@class="ts"]//span/text()'
contentId = '//div[contains(@id,"post_")]/table/@id'
publishDates = ['//div[@class="authi"]/em/text()','//div[@class="authi"]/em/span/@title']
content = '//div[@class="t_fsz"] | //div[@class="pcb"]'
commentCount = '//span[@class="xi1"][2]/text()'
viewCount = '//span[@class="xi1"][1]/text()'
posterId = '//div[@class="pi"]/div[@class="authi"]/a/@href'
posterScreenName = '//div[@class="pi"]/div[@class="authi"]/a/text()'
avatarUrl = '//div[@class="avatar"]//img/@src'
imageUrls = '//td[@class="plc"]//img/@file'
postPage = '//div[@class="pgbtn"]/a/@href'
pageSelect = '//div[@class="pgbtn"]//text()'
sectionPageSelect = '//div[@id="pgt"]//div[@class="pg"]/a//text()'


###########################################
key_ = 'forum_rule'     # 指定db的key


forum_rule = {
    "host": host if host else None,   # 主页host
    "home": home,  # 解析提取起始页所有板块的规则
    "forumList": forumList,  # 解析提取每一个板块的urls文章列表规则列表
    "title": title,  # 解析提取文章正文
    "contentId": contentId,  # 解析提取文章各个楼层id列表
    "publishDate": publishDates,  # 解析提取文章各个楼层发布时间列表
    "content": content,  # 解析提取文章各个楼层content列表
    "commentCount": commentCount,  # 解析提取文章评论数量
    "viewCount": viewCount,  # 解析提取文章阅读数量
    "posterId": posterId,  # 解析提取文章各个楼层发布者id列表
    "posterScreenName": posterScreenName,  # 解析提取文章各个楼层发布者名字列表
    "avatarUrl": avatarUrl,  # 解析提取文章各个楼层发布者封面头像列表
    "imageUrl": imageUrls,  # 解析提取文章各个楼层插入的图片列表
    "postPage": postPage,  # 解析提取文章下一页
    "pageSelect": pageSelect,  # 解析提取文章是否有翻页，下一页
    "sectionPageSelect": sectionPageSelect,  # 解析论坛版块是否能翻页，下一页

}
# pprint(forum_rule)


pool = redis.ConnectionPool(host="127.0.0.1", port=6379, db=1)  # 默认指定数据库，使用者可以自行更改
redis_ = redis.Redis(connection_pool=pool)


def rule_to_redis():
    """
    func: 存储解析规则到redis，分别有默认的，当前网站指定的规则
    :return: 请注意当前使用的是hash存储
    """
    redis_.hmset(key_, {'{key}'.format(key=host): json.dumps(forum_rule)})      # 指定当前规则
    redis_.hmset(key_, {'{key}'.format(key='default'): json.dumps(forum_rule)})   # 默认解析规则
    print('set ok')



def rule():
    """
    :func:  查看当前的解析规则
    """
    if redis_.hmget(key_, host)[0]:
        forum_ = redis_.hmget(key_, host)[0]
        print('host', forum_)
    elif redis_.hmget(key_, 'default')[0]:
        forum_ = redis_.hmget(key_, 'default')[0]
        print('default', forum_)
    else:
        raise Exception('NO rule')



if __name__ == '__main__':
    rule_to_redis()
    # rule()
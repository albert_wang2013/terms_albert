# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/4/24 16:18
@Func    : 字段定义
@File    : forum_mapping.py
@Software: PyCharm
"""
import base64
import html
import re
import time
import random
import html2text
from lxml import etree
from lxml.html.clean import Cleaner

cleaner = Cleaner()
cleaner.javascript = True
cleaner.style = True



def forum_post_map():
    """
    :return: 论坛的数据结构
    """
    obj = {
        "id": None,     # 帖子id
        "posterId": None,     # 发布者id
        "posterScreenName": None,     # 发布者名称
        "commentCount": None,     # 评论数 int
        "likeCount": None,     # 点赞数 int
        "shareCount": None,     # 转发数 int
        "viewCount": None,     # 观看数 int
        "publishDate": None,     # 发布时间：UTC时间戳格式： int
        "publishDateStr": None,     # 发布时间：UTC字符串格式，例如2019-02-14T18:19:08 str
        "title": None,     # 标题（最重要的）
        "content": None,     # 帖子内容
        "html": None,     # content对应的html代码，包含格式标签等
        "url": None,     # 本对象的最通用url
        "avatarUrl": None,     # 头像url
        "imageUrls": None,     # 图像url
        "videoUrls": None,     # 视频url
        "comments": None,     # 评论列表

    }
    comments = {
        "id": None,     # 评论id
        "commenterId": None,     # 评论者id
        "commenterScreenName": None,     # 评论者名称
        "avatarUrl": None,     # 评论者头像url
        "publishDateStr": None,     # 评论时间
        "publishDate": None,     # 评论戳
        "content": None,     # 评论内容
        # "imageUrls": None,     # 图像url列表
    }
    return obj

# xpath获取的正文并保留标签转换成字符
def xpath_raw2content_ch(xpath_raw):
    if xpath_raw is not None:
        raw_content = xpath_raw
        html = cleaner.clean_html(
            bytes.decode(
                etree.tostring(
                    raw_content,
                    encoding="utf-8")))
    else:
        html = ''
    return html


def html_content(html):
    html_txt = html2text.HTML2Text()
    html_txt.ignore_links = True
    html_txt.ignore_images = True
    html_txt.ignore_tables = True
    content = html_txt.handle(html)
    return content



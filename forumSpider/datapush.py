# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/4/28 14:49
@Func    : 论坛采集程序，自动化采集，在urls.txt中输入要采集的urls即可，程序会依次采集各个板块数据。（异步高并发）

@File    : datapush.py
@Software: PyCharm
"""
import asyncio
import logging
from transform import ProcessFactory, GetterConfig, WriterConfig
import aiofiles
from logging.handlers import RotatingFileHandler


# 设置log打印信息，保存信息
logging.basicConfig(level=logging.INFO, datefmt='%a, %d %b %Y %H:%M:%S',
                    format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s')
Rthandler = RotatingFileHandler('datapush.log', maxBytes=100*1024*1024, backupCount=5)
Rthandler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
                              datefmt='%a, %d %b %Y %H:%M:%S')
Rthandler.setFormatter(formatter, )
logging.getLogger('').addHandler(Rthandler)



class push():
    def __init__(self):
        self.redis_host_local = WriterConfig.WRedisConfig('datas', host='127.0.0.1', port=6379, db=1,
                                                          encoding='utf8')  # 采集数据后默认保存地址及db
        self.comment_count = 0


    async def start(self):
        """
        func：暂时启弃用，先不用看不用管
        :return:
        """
        async with aiofiles.open('urls.txt', encoding='utf8', mode='r') as f:   # 读取需要采集的版块，从urls中获取
            urls = [i.replace('\n', '') for i in await f.readlines()]
            url_gen = []
            for url in urls:
                url = 'http://localhost:5555/post/forum?sectionurl={}&pageToken=1'.format(url)
                url_generator = GetterConfig.RAPIConfig(url, max_retry=2, trim_to_max_limit=True, keep_other_fields=False,)  # 测试专用，max_limit最大获取条数
                # url_generator = GetterConfig.RAPIConfig(url, max_retry=5, trim_to_max_limit=True, keep_other_fields=False)  # max_retry重试凑数
                url_gen.append(url_generator)
        datas_list = []
        getter = ProcessFactory.create_getter(GetterConfig.RAPIBulkConfig(url_gen, concurrency=20))   # concurrency 并发设置，内网的话可设置最好的100并发
        if getter:
            async for items in getter:
                # print(len(items))
                for item in items:
                    if len(datas_list) >= 50:   # 每获取50条数据，存储一次进redis
                        datas = {
                            "interface": "post/forum",
                            "data": datas_list
                        }
                        await ProcessFactory.create_writer(self.redis_host_local).write([datas])
                        logging.info('writer items to redis length:{}'.format(len(datas['data'])))
                        datas_list = []
                        datas_list.append(item)
                    else:
                        datas_list.append(item)
                if datas_list:
                    datas = {
                        "interface": "post/forum",
                        "data": datas_list
                    }
                    await ProcessFactory.create_writer(self.redis_host_local).write([datas])
                    logging.info('writer items to redis length:{}'.format(len(datas['data'])))
                    datas_list = []



    async def crawl(self):
        """
        func: 处理请求，返回datas数据，存入redis中
        """
        async with aiofiles.open('urls.txt', encoding='utf8', mode='r') as f:   # 读取需要采集的版块，从urls中获取
            urls = [i.replace('\n', '') for i in await f.readlines()]
            url_gen = []
            for url in urls:
                url = 'http://localhost:5555/post/forum?sectionurl={}&pageToken=1'.format(url)
                url_generator = GetterConfig.RAPIConfig(url, max_retry=2, trim_to_max_limit=True, keep_other_fields=False,)  # 测试专用，max_limit最大获取条数
                # url_generator = GetterConfig.RAPIConfig(url, max_retry=5, trim_to_max_limit=True, keep_other_fields=False)  # max_retry重试凑数
                url_gen.append(url_generator)
        datas_list = []
        getter = ProcessFactory.create_getter(GetterConfig.RAPIBulkConfig(url_gen, concurrency=20))   # concurrency 并发设置，内网的话可设置最好的100并发
        if getter:
            async for items in getter:
                for item in items:
                    if len(datas_list) >= 50:   # 每获取50条数据，存储一次进redis
                        datas = {
                            "interface": "post/forum",
                            "data": datas_list
                        }
                        await ProcessFactory.create_writer(self.redis_host_local).write([datas])
                        logging.info('writer post-items to redis length:{}'.format(len(datas['data'])))
                        datas_list = []
                        datas_list.append(item)
                    else:
                        item = await self.deal_item(item)

                        datas_list.append(item)
                if datas_list:
                    datas = {
                        "interface": "post/forum",
                        "data": datas_list
                    }
                    await ProcessFactory.create_writer(self.redis_host_local).write([datas])
                    logging.info('writer post-items to redis length:{}'.format(len(datas['data'])))
                    datas_list = []



    async def deal_item(self, item):
        """
        :func:  处理item数据，将comments进行拆分处理，加入referId并存至指定的redis数据库
        :return: 返回item,删除其中的comments
        """
        comments = []
        if item['comments']:
            referId = item['id']
            for comment in item['comments']:
                if len(comments) >= 50:     # 每获取50条数据，存储一次进redis
                    datas = {
                        "interface": "comment/forum",
                        "data": comments
                    }
                    await ProcessFactory.create_writer(self.redis_host_local).write([datas])
                    logging.info('writer comments-items to redis length:{}'.format(len(datas['data'])))
                    comments = []
                    comments.append(comment)
                else:
                    comment['referId'] = referId
                    comments.append(comment)
            if comments:
                datas = {
                    "interface": "comment/forum",
                    "data": comments
                }
                await ProcessFactory.create_writer(self.redis_host_local).write([datas])
                logging.info('writer comments-items to redis length:{}'.format(len(datas['data'])))
                comments = []
        if 'comments' in item:
            del item['comments']
            return item



if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    # loop.run_until_complete(push().start())
    loop.run_until_complete(push().crawl())
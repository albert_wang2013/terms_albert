# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/4/29 12:18
@Func    :  1、启动程序，将启动tornado服务，该脚本可以直接调用端口的形式运行脚本，默认端口地址：
            http://localhost:5555/post/forum?sectionurl={xxxxx}&pageToken={xxxxx}   （可在浏览器直接打开，方便测试）
            注：sectionurl 为论坛的版块名称（论坛形式要如同：http://www.discuz.net/forum-2-1.html 打开后类似排版格式）
                pageToken  为当前版块的翻页数
            2、运行调用时，将首先调用redis中的论坛xpath解析规则，优先级：当前论坛解析规则 > 默认default解析规则，
            即在当前论坛没有定义解析规则的情况下，才会调用默认的解析规则。
            3、采集sectionurl版块输入之后，会遍历调用所有页面，以及采集所有内容页的所有详情数据
            4、某一解析规则报错后，会在error_warning.log日志中有提示，请按照提示更新论坛的解析规则
            5、具体函数模块细节更改，可由使用者自行校正,比如程序跑内网数据时，并发可以进行调整

@File    : test.py
@Software: PyCharm
"""
import configparser
import hashlib
import json
import logging
import re
import time
from pprint import pprint
import html2text
import aiohttp
import asyncio
import redis
from lxml import etree
# from forumSpider.forum_mapping import forum_post_map, xpath_raw2content_ch, html_content
from forumSpider.forum_mapping import forum_post_map, xpath_raw2content_ch, html_content
import tornado.ioloop
import tornado.web
from logging.handlers import RotatingFileHandler


headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, '
                          'like Gecko) Chrome/73.0.3683.86 Safari/537.36',
            }


# 设置log打印信息，保存信息
logging.basicConfig(level=logging.INFO, datefmt='%a, %d %b %Y %H:%M:%S',
                    format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s')
Rthandler = RotatingFileHandler('error_warning.log', maxBytes=100*1024*1024, backupCount=5)
Rthandler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
                              datefmt='%a, %d %b %Y %H:%M:%S')
Rthandler.setFormatter(formatter, )
logging.getLogger('').addHandler(Rthandler)


""" 配置类，理解就行 """
class configPar(object):
    def __init__(self):
        super().__init__()
        self.conf = configparser.ConfigParser()
        self.conf.read('config.ini')

    def config_func(self):
        max_post_page = self.conf.get('forum_config', 'max_post_page')
        max_commnet_page = self.conf.get('forum_config', 'max_commnet_page')
        max_sema = self.conf.get('forum_config', 'max_sema')
        max_retries = self.conf.get('forum_config', 'max_retries')
        max_conn_time_out = self.conf.get('forum_config', 'max_conn_time_out')
        redis_host = self.conf.get('forum_config', 'redis_host')
        redis_port = self.conf.get('forum_config', 'redis_port')
        redis_db = self.conf.get('forum_config', 'redis_db')
        return int(max_post_page), int(max_commnet_page), int(max_sema), int(
            max_retries), int(max_conn_time_out), str(redis_host), int(redis_port), int(redis_db)


""" 采集程序，最重要"""
class forumSpider(object):
    def __init__(self, sectionurl, page):
        super().__init__()
        config_list = configPar().config_func()
        self.sectionurl = sectionurl    # 论坛板块的url
        self.host = re.match('(.*/).*', sectionurl).group(1)    # 提取当前论坛主页
        self.page = page    # 当前翻页码
        self.sema = asyncio.Semaphore(config_list[2])   # 并发，采集内网数据时，并发可以调整，最大为100并发
        self.time_out = config_list[4]  # 超时时间设置
        self.max_tries = config_list[3]     # 爬虫网络请求的最大重试设置
        self.max_post_page = config_list[0]     # 论坛请求版块的最大翻页数目，比如设置10，代表最大翻页数为10
        self.max_commnet_page = config_list[1]  # 帖子的跟帖最大翻页数目，比如设置200，代表最大跟帖翻页为200
        self.redis_ = redis.Redis(connection_pool=redis.ConnectionPool(
            host=config_list[5], port=config_list[6], db=config_list[7]))  # 存储解析规则的redis地址
        if self.redis_.hmget('forum_rule', self.host)[0]:
            self.forum_rule = self.redis_.hmget('forum_rule', self.host)[0]
            logging.info('current forum_rule:{}'.format(json.loads(self.forum_rule.decode())))
        elif self.redis_.hmget('forum_rule', 'default')[0]:
            self.forum_rule = self.redis_.hmget('forum_rule', 'default')[0]
            logging.info('default forum_rule:{}'.format(json.loads(self.forum_rule.decode())))
        else:
            raise Exception('NO rule')
        self.forum_rule = json.loads(self.forum_rule.decode())
        """
        解析规则形式如下所示：
        {'avatarUrl': '//div[@class="avatar"]//img/@src',       # 解析提取文章各个楼层发布者封面头像列表
         'commentCount': '//span[@class="xi1"][2]/text()',      # 解析提取文章评论数量
         'content': '//div[@class="t_fsz"]',                    # 解析提取文章各个楼层content列表
         'contentId': '//div[contains(@id,"post_")]/table/@id', # 解析提取文章各个楼层id列表
         'forumList': '//tbody//th/a[3]/@href',                 # 解析提取每一个板块的urls文章列表规则列表
         'home': '//tbody//dt/a/@href',                         # 解析提取起始页所有板块的规则
         'host': 'http://www.discuz.net/',                      # 主页host
         'imageUrls': '//div[@id="postlist"]/div//td[@class="plc"]//img/@zoomfile',      # 解析提取文章各个楼层插入的图片列表
         'postPage': '//div[@class="pgbtn"]/a/@href',                                    # 解析提取文章是否有翻页，下一页
         'posterId': '//div[@class="pi"]/div[@class="authi"]/a/@href',                   # 解析提取文章各个楼层发布者id列表
         'posterScreenName': '//div[@class="pi"]/div[@class="authi"]/a/text()',          # 解析提取文章各个楼层发布者名字列表
         'publishDates': ['//div[@class="authi"]/em/text()','//div[@class="authi"]/em/span/@title'],    解析提取文章各个楼层发布时间列表
         'title': '//h1[@class="ts"]//span/text()',                                      # 解析提取文章正文
         'viewCount': '//span[@class="xi1"][1]/text()'}                                  # 解析提取文章阅读数量
        """



    async def start(self):
        if int(self.page) > self.max_post_page:  # 判断是否大于指定的页数
            return {"retcode": "100002", "message": "search no result"}
        # http://www.discuz.net/forum-plugin-2.html     # test
        if 'html' in self.sectionurl:
            start_url = re.sub('-\d\.html', '-{}.html'.format(self.page), self.sectionurl)      # 替换url成要请求的url，这个规则可能会变化
        else:
            start_url = self.sectionurl + '&page={}'.format(self.page)
        logging.info('spider start_url:{}'.format(start_url))
        # start_url = 'http://www.discuz.net/threasd-3845678-1-3.html'      # test
        async with aiohttp.ClientSession(read_timeout=self.time_out,) as session:
            async with session.get(url=start_url, headers=headers,) as response_:
                try:
                    assert response_.status == 200
                    response = await response_.text()
                    # print(response)
                    urls, page_judge = await self.url_extract(response)
                    logging.info('response_urls length to spider:{}, page_judge:{}'.format(len(urls), page_judge))
                    datas = await self.url_request(urls)
                    if page_judge:
                        hasNext = True
                        pageToken = str(int(self.page) + 1)
                    else:
                        hasNext = False
                        pageToken = None
                    result = {
                        "data": datas,
                        "pageToken": pageToken,
                        "hasNext": hasNext,
                        "retcode": "000000",
                        "dataType": "post",
                        "appCode": "forum"
                    }
                    logging.info('data length:{}, appcode: discuz, pageToken:{}'.format(len(datas), pageToken))
                    # print(json.dumps(result))
                    return result
                except Exception as e:
                    logging.info(e)
                    logging.error('Not found request_url(currenturl:{}) or'
                          'status is:{})'.format(start_url, response_.status))
                    # print(response_.content)
                    result = {"retcode": "100000", "message": "network error"}
                    return result


    async def url_extract(self, response):
        """
        func: 正则提取response的url,并且判断是否翻页
        :return: 返回urls列表
        """
        # print(response)
        urls = []
        data = etree.HTML(response)
        ids = data.xpath('{}'.format(self.forum_rule['forumList']))
        if ids:
            for i in ids:
                if not 'javascript' in i:
                    if 'http' in i:
                        urls.append(i)
                    else:
                        url = self.host + i
                        urls.append(url)
        else:
            logging.warning("forumList parse error!!!, please try again!!! {}".format(self.forum_rule['forumList']))
        if urls:
            sectionPageSelect = data.xpath('{}'.format(self.forum_rule['sectionPageSelect']))   # 这个解析一定要对，关系到能否翻页
            if sectionPageSelect:
                for i in sectionPageSelect:
                    if not i.isdigit():
                        sectionPageSelect.remove(i)
            sectionPageSelect_ = max(i for i in sectionPageSelect)
            if '下一页' in response and self.page < sectionPageSelect_:
                page_judge = True
            else:
                page_judge = False
            # print(urls, page_judge)
            if urls:
                return urls, page_judge
        else:
            logging.warning("forumList parse error!!!, please try again!!! {}".format((self.forum_rule['forumList'])))


    async def url_request(self, urls):
        """
        func: 对urls列表分别进行请求，解析详情
        :return: 异步请求，返回result列表
        """
        # print(urls)   # test用
        async with aiohttp.ClientSession(read_timeout=self.time_out * 2) as session:
            tasks = []
            for url in urls:
                task = asyncio.ensure_future(self.start_request(session, url))
                tasks.append(task)
            datas = await asyncio.gather(*tasks, return_exceptions=False)
            datas = [i for i in datas if i]     # 去除空数据none，这些可能被删掉的帖子
            return datas


    async def start_request(self, session, url):
        """
        :param session: 单个会话
        :param url: 对每一个详情页进行请求
        :return: 返回当前url所能得到的所有数据，正文+评论
        """
        logging.info('start_request current url:{}'.format(url))
        tries = 0
        response = None
        while tries < self.max_tries/2:     # 设置错误请求的最大重试次数，默认self.max_tries/2次
            async with self.sema, session.get(headers=headers, url=url) as response:
                if response.status == 200:
                    response = await response.text(errors='ignore')
                    break
                else:
                    tries += 1
            logging.info('post_request tries:{} and response.status:{},url:{}'.format(tries, response.status, url))
        if response:
            data = etree.HTML(response)
            comments_list, comment_url = None, None     # 解析的时候包含了第一个正文内容要删除
            if await self.comment_parse(data, url):  # 空结果返回不能被迭代，否则会报TypeError: 'NoneType' object is not iterable
                comments_list, comment_url = await self.comment_parse(data, url)
            size = 1
            while True:  # 循环翻页遍历评论帖子
                if size < self.max_commnet_page:
                    if comment_url:
                        logging.info('comment_url  comment_request:{}'.format(comment_url))
                        response = await self.comment_request(comment_url)
                        data_ = etree.HTML(response)
                        comments_, comment_url = await self.comment_parse(data_, url)
                        # print(comments_)
                        for i in comments_:
                            comments_list.append(i)
                        size += 1
                    else:
                        break
                else:
                    break
            data = await self.response_parse(data, url)
            # print(len(comments_list))
            if comments_list:
                if comments_list[1:]:
                    data['comments'] = comments_list[1:]
                if data['comments']:    # 如果评论翻页为0，则不返回评论
                    if self.max_commnet_page == 0:
                        del data['comments']
            return data
        else:
            return None


    def time_deal(self, time_str):
        """
        func: 处理时间函数
        :return: 返回publishDate, publishDateStr
        """
        time_arr = time.strptime(time_str, '%Y-%m-%d %H:%M:%S')
        timestamp = int(time.mktime(time_arr))
        struct_time = time.gmtime(timestamp)
        time_str = time.strftime('%Y-%m-%dT%H:%M:%S', struct_time)  # 将时间数组 转换为 时间字符串
        publishDate = timestamp
        publishDateStr = time_str
        return publishDate, publishDateStr


    async def response_parse(self, data, url):
        """
        post数据结构：
        "id": None,     # 帖子id
        "posterId": None,     # 发布者id
        "posterScreenName": None,     # 发布者名称
        "commentCount": None,     # 评论数 int
        "likeCount": None,     # 点赞数 int
        "shareCount": None,     # 转发数 int
        "viewCount": None,     # 观看数 int
        "publishDate": None,     # 发布时间：UTC时间戳格式： int
        "publishDateStr": None,     # 发布时间：UTC字符串格式，例如2019-02-14T18:19:08 str
        "title": None,     # 标题（最重要的）
        "content": None,     # 帖子内容
        "html": None,     # content对应的html代码，包含格式标签等
        "url": None,     # 本对象的最通用url   http://www.discuz.net/thread-3845782-1-1.html
        "avatarUrl": None,     # 头像url
        "imageUrls": None,     # 图像url
        "videoUrls": None,     # 视频url
        "comments": None,     # 评论列表

        comments数据结构：
         "id": None,     # 评论id
        "commenterId": None,     # 评论者id
        "commenterScreenName": None,     # 评论者名称
        "avatarUrl": None,     # 评论者头像url
        "publishDateStr": None,     # 评论时间
        "publishDate": None,     # 评论戳
        "content": None,     # 评论内容
        # "imageUrls": None,     # 图像url列表
        :return: 返回解析数据
        """
        try:
            data_json = forum_post_map()
            data_json['url'] = url
            ids_ = re.match('.*/(.*)\.html|.*/(.*)\.hml', url)
            id_ = None
            if ids_:
                for i in ids_.groups():
                    if i:
                        id_ = i
                        break
            if id_:
                data_json['id'] = id_
            else:
                data_json['id'] = url
            # data_json['id'] = hashlib.md5(url.encode("utf8")).hexdigest()
            """title"""
            if data.xpath('{}'.format(self.forum_rule['title'])):
                data_json['title'] = data.xpath('{}'.format(self.forum_rule['title']))[0]
            else:
                logging.warning('title parse error!!!, please try again!!! {}'.format(url))
                return None
            """commentCount 可能有可无，比较不好判断，所以一定需要重新确认这个解析"""
            if data.xpath('{}'.format(self.forum_rule['commentCount'])):
                data_json['commentCount'] = data.xpath('{}'.format(self.forum_rule['commentCount']))[0]
            else:
                data_json['commentCount'] = None
            """viewCount """
            if data.xpath('{}'.format(self.forum_rule['viewCount'])):
                data_json['viewCount'] = data.xpath('{}'.format(self.forum_rule['viewCount']))[0]
            else:
                data_json['viewCount'] = None
            """publishDate"""
            if any(char.isdigit() for char in data.xpath('{}'.format(self.forum_rule['publishDate'][0]))[0]):   # 判断是否有数字在里面
                date_ = data.xpath('{}'.format(self.forum_rule['publishDate'][0]))[0]
                date_ = re.match('.*?(\d+-\d+-\d+ \d+:\d+:\d+).*?', date_).group(1)
                publishDate, publishDateStr = self.time_deal(date_)
                data_json['publishDate'] = publishDate
                data_json['publishDateStr'] = publishDateStr
            elif any(char.isdigit() for char in data.xpath('{}'.format(self.forum_rule['publishDate'][1]))[0]):
                date_ = data.xpath('{}'.format(self.forum_rule['publishDate'][1]))[0]
                date_ = re.match('.*?(\d+-\d+-\d+ \d+:\d+:\d+).*?', date_).group(1)     # 返回规则时间：2019-4-29 11:34:21
                publishDate, publishDateStr = self.time_deal(date_)
                data_json['publishDate'] = publishDate
                data_json['publishDateStr'] = publishDateStr
            else:
                logging.warning('publishDate parse error!!!, please try again!!! {}'.format(url))
                return None
            """posterId"""
            if data.xpath('{}'.format(self.forum_rule['posterId'])):
                data_json['posterId'] = re.match('.*?(\d+).*?', data.xpath('{}'.format(self.forum_rule['posterId']))[0]).group(1)
            else:
                logging.warning('posterId parse error!!!, please try again!!! {}'.format(url))
                return None
            """posterScreenName"""
            if data.xpath('{}'.format(self.forum_rule['posterScreenName'])):
                data_json['posterScreenName'] = data.xpath('{}'.format(self.forum_rule['posterScreenName']))[0]
            else:
                logging.warning('posterScreenName parse error!!!, please try again!!! {}'.format(url))
                return None
            """avatarUrl 可能有可无，比较不好判断，所以一定需要重新确认这个解析"""
            if data.xpath('{}'.format(self.forum_rule['avatarUrl'])):
                data_json['avatarUrl'] = data.xpath('{}'.format(self.forum_rule['avatarUrl']))[0]
            else:
                data_json['avatarUrl'] = None
            """imageUrls  可能有可无，比较不好判断，所以一定需要重新确认这个解析"""
            if data.xpath('{}'.format(self.forum_rule['imageUrl'])):
                data_json['imageUrls'] = [img for img in data.xpath('{}'.format(self.forum_rule['imageUrl'])) if 'http' in img]
            else:
                data_json['imageUrls'] = None
            """content"""
            if data.xpath('{}'.format(self.forum_rule['content'])):
                data_json['html'] = xpath_raw2content_ch(data.xpath('{}'.format(self.forum_rule['content']))[0])
                data_json['content'] = html_content(data_json['html']).replace('\n', '')
            else:
                logging.warning('content parse error!!!, please try again!!! {}'.format(url))
                return None
            return data_json
        except Exception as e:
            logging.info(e)
            return None


    async def comment_request(self, comment_url):
        """
        :param comment_url: 评论的url进行请求解析
        :return: 返回评论数据
        """
        tries = 0
        response = None
        while tries < self.max_tries:   # 设置错误请求的最大重试次数，默认10次
            try:
                async with aiohttp.ClientSession(read_timeout=self.time_out) as session:
                    async with session.get(url=comment_url, headers=headers) as response:
                        if response.status == 200:
                            response = await response.text()
                            break
                        else:
                            tries += 1
                logging.info('comment_request tries:{} and response.status:{},url:{}'.format(tries, response.status, comment_url))
            except:
                tries += 1
                logging.info('comment_request tries:{} ,url:{}'.format(tries, comment_url))
        return response


    async def comment_parse(self, data, url):
        """
        :param data: 评论的response
        :param url: 评论的url
        :return: 返回解析后的结构性评论数据
        """
        """ list for posterId/posterScreenName/publishDateStr/html 解析返回一个列表 """
        try:
            """publishDates"""
            pubdate_1 = data.xpath('{}'.format(self.forum_rule['publishDate'][0]))
            pubdate_2 = data.xpath('{}'.format(self.forum_rule['publishDate'][1]))
            pubdates = []
            pubdate_num = 0  # 计数pubdate_2
            for i in pubdate_1:
                if any(char.isdigit() for char in i):
                    pubdates.append(i)
                else:
                    i = pubdate_2[pubdate_num]
                    pubdates.append(i)
                    pubdate_num += 1
            """posterIds"""
            if data.xpath('{}'.format(self.forum_rule['posterId'])):
                posterIds = data.xpath('{}'.format(self.forum_rule['posterId']))
            else:
                logging.warning('posterId parse error!!!, please try again!!!  {}'.format(url))
                return None
            """posterScreenNames"""
            if data.xpath('{}'.format(self.forum_rule['posterScreenName'])):
                posterScreenNames = data.xpath('{}'.format(self.forum_rule['posterScreenName']))
            else:
                logging.warning('posterScreenName parse error!!!, please try again!!!  {}'.format(url))
                return None
            """avatarUrls 可能有可无，比较不好判断，所以一定需要重新确认这个解析"""
            if data.xpath('{}'.format(self.forum_rule['avatarUrl'])):
                avatarUrls = data.xpath('{}'.format(self.forum_rule['avatarUrl']))
            else:
                logging.warning('avatarUrls parse error!!!, please try again!!!  {}'.format(url))
                avatarUrls = ['头像被屏蔽']
            """content"""
            if data.xpath('{}'.format(self.forum_rule['content'])):
                contents = data.xpath('{}'.format(self.forum_rule['content']))
            else:
                logging.warning('content parse error!!!, please try again!!!  {}'.format(url))
                return None
            """commentIds"""
            if data.xpath('{}'.format(self.forum_rule['contentId'])):
                commentIds = data.xpath('{}'.format(self.forum_rule['contentId']))
            else:
                logging.warning('content parse error!!!, please try again!!!  {}'.format(url))
                return None
            """ comments 内容 """
            comments_list = []
            # print(len(posterIds),)
            # print(len(posterScreenNames))
            # print(len(avatarUrls))
            # print(len(pubdates),)
            # print(len(commentIds),len(contents),)
            if len(posterIds) >= 1:  # 判断是否有评论
                for posterId, posterScreenName, avatarUrl, pubdate, commentId, content in zip(posterIds,
                                                                                              posterScreenNames,
                                                                                              avatarUrls, pubdates,
                                                                                              commentIds, contents):
                    comments = {}
                    if 'javascript' in posterId:
                        comments['commenterId'] = None
                    else:
                        comments['commenterId'] = re.match('.*?(\d+).*?', posterId).group(1)
                    # print("comments['commenterId']:{}".format(comments['commenterId']))
                    comments['commenterScreenName'] = posterScreenName
                    comments['id'] = commentId
                    comments['avatarUrl'] = avatarUrl
                    pubdate = re.match('.*?(\d+-\d+-\d+ \d+:\d+:\d+).*?', pubdate).group(1)
                    publishDate, publishDateStr = self.time_deal(pubdate)
                    comments['publishDateStr'] = publishDateStr
                    comments['publishDate'] = publishDate
                    comments['content'] = (html_content(xpath_raw2content_ch(content))).replace('\n', '')
                    comments_list.append(comments)
            page_select = data.xpath('{}'.format(self.forum_rule['pageSelect']))   # 判断是否有下一页评论
            # print(comments_list)
            # print(len(comments_list))
            comment_url = None
            if page_select:
                if '下一页' in page_select[0]:
                    page = data.xpath(self.forum_rule['postPage'])
                    if self.host in page[0]:
                        comment_url = page[0]
                    else:
                        comment_url = self.host + page[0]
            return comments_list if comments_list else None, comment_url
        except Exception as e:
            logging.warning('comments error:{}'.format(e))
            return None, None



""" 启动tornado服务监控 """
class MainHandler(tornado.web.RequestHandler):
    """
    启动tornado服务监控
    """
    async def get(self):
        sectionurl = re.match('.*?sectionurl=(.*?)&pageToken.*?', self.request.uri).group(1)
        # print(sectionurl)
        pageToken = self.get_argument('pageToken', None)
        if sectionurl and pageToken:
            logging.info('uri:{}  sectionurl:{}  pageToken:{}'.format(self.request.uri, sectionurl, pageToken))
            start_time = time.time()
            spider_ = forumSpider(sectionurl=sectionurl, page=pageToken)
            data = await spider_.start()
            self.write(data)
            duration = time.time() - start_time
            logging.info(f"deal tasks in {duration} seconds")
        else:
            logging.warning('Missing_param sectionurl or pageToken!!!')
            result = {"retcode": "100005", "message": "param error"}
            self.write(result)



if __name__ == '__main__':
    """
    http://localhost:5555/post/forum?sectionurl=http://www.discuz.net/forum-plugin-1.html&pageToken=1
    http://localhost:5555/post/forum?sectionurl=http://bbs.polchina.com.cn/forum.php?mod=forumdisplay&fid=151&pageToken=2
    """
    logging.info('program  running!!!')
    application = tornado.web.Application([
        (r"/post/forum", MainHandler),
    ])
    application.listen(5555)
    tornado.ioloop.IOLoop.instance().start()



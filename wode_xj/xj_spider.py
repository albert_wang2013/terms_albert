import asyncio
import hashlib
import logging
import os
import sys
import time
import zipfile
from urllib.parse import quote
import oss2
from idataapi_transform import ProcessFactory, GetterConfig, WriterConfig
import aiofiles

logging.basicConfig(level=logging.INFO)


class XJSpider():
    def __init__(self):
        self.kws = []
        self.google_url = 'https://api02.bitspaceman.com/news/google?kw={}&sort=2&size=50&apikey='
        self.wiki_url = 'https://api02.bitspaceman.com/baike/wikipedia?type=1&kw={}&apikey='
        self.wiki_url_ori = 'https://zh.wikipedia.org/wiki/{}'
        self.diffbot_url = 'https://api01.bitspaceman.com/news/diffbot?url={}&type=analyze&timeout=30000&apikey='
        self.apikey = 'yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
        self.con_ = 5
        self.max_retry = 5
        self.data_list = {}
        self.host = '123.196.116.97'
        self.file_ = 'wode_xj'

    async def get_kws(self):
        file_dir = os.path.split(os.path.realpath(sys.argv[0]))[0]
        file = 'kw.txt'
        file_path = file_dir + '/' + file
        logging.info('file path is:{}'.format(file_path))
        async with aiofiles.open(file_path, encoding='utf8', mode='r') as f:
            kws_ = await f.readlines()
            for id_ in kws_:
                self.kws.append(id_.strip())

    @staticmethod
    def id_hash_func(item):
        if "id" in item and item["id"]:
            value = (item["id"]).encode("utf8")
        else:
            value = str(item).encode("utf8")
        return hashlib.md5(value).hexdigest()

    def mongo_sel(self, name_):
        col_name = '{}'.format(name_)
        mongo_ = WriterConfig.WMongoConfig(
            col_name,
            host=self.host,
            port=20020,
            username='wm_terms',
            password='qwer',
            database='wm',
            encoding='utf8',
            id_hash_func=self.id_hash_func)
        return mongo_

    async def wiki_req(self):
        async def req(i):
            url = self.wiki_url.format(i) + self.apikey
            api_getter = GetterConfig.RAPIConfig(
                url,
                max_retry=self.max_retry,
                max_limit=1
            )
            getter = ProcessFactory.create_getter(GetterConfig.RAPIBulkConfig(
                [api_getter], concurrency=self.con_))  # 采集文章
            datas_list = []
            async for items in getter:
                for item in items:
                    item['url'] = self.wiki_url_ori.format(item['title'])
                    item['publishDateStr'] = item['updateDate'].replace('z', '').replace('Z', '')
                    item['publishDate'] = int(time.mktime(time.strptime(item['publishDateStr'], "%Y-%m-%dT%H:%M:%S")))
                    del item['updateDate']
                    datas_list.append(item)
                await ProcessFactory.create_writer(self.mongo_sel('wiki')).write([i for i in datas_list if i])
            if not datas_list:
                return i

        tasks = []
        for i in self.kws:
            tasks.append(req(i))
        no_data_kw = await asyncio.gather(*tasks)
        # logging.info('wiki kw no_data:\n {}'.format(no_data_kw))
        return {'wiki_kw': [i for i in no_data_kw if i]}

    async def google_req1(self):
        async def req(i):
            url = self.google_url.format(i) + self.apikey
            api_getter = GetterConfig.RAPIConfig(
                url,
                max_retry=self.max_retry,
                max_limit=150
            )
            getter = ProcessFactory.create_getter(GetterConfig.RAPIBulkConfig(
                [api_getter], concurrency=1))  # 采集文章
            datas_list = []
            async for items in getter:
                for item in items:
                    datas_list.append(item)
                await ProcessFactory.create_writer(self.mongo_sel('google')).write([i for i in datas_list if i])
            if not datas_list:
                return i

        tasks = []
        for i in self.kws:
            tasks.append(req(i))
        no_data_kw = await asyncio.gather(*tasks)
        # logging.info('google kw no_data:\n {}'.format(no_data_kw))
        return {'google_kw': [i for i in no_data_kw if i]}

    async def google_req(self):
        tasks = []
        for i in self.kws:
            url = self.google_url.format(i) + self.apikey
            api_getter = GetterConfig.RAPIConfig(
                url,
                max_retry=self.max_retry,
                max_limit=200
            )
            tasks.append(api_getter)
        getter = ProcessFactory.create_getter(GetterConfig.RAPIBulkConfig(
            tasks, concurrency=2))  # 采集文章
        datas_list = []
        async for items in getter:
            for item in items:
                datas_list.append(item)
                if len(datas_list) >= 50:  # 每获取50条数据，存储一次进redis
                    await ProcessFactory.create_writer(self.mongo_sel('google')).write([i for i in datas_list if i])
                    logging.info(
                        'writer google-items to mongo length:{}'.format(len(datas_list)))
                    datas_list.clear()
                    datas_list.append(item)
                else:
                    datas_list.append(item)
        if datas_list:
            await ProcessFactory.create_writer(self.mongo_sel('google')).write([i for i in datas_list if i])
            logging.info(
                'writer google-items to redis length:{}'.format(len(datas_list)))
            datas_list.clear()

    async def diffbot_req(self, urls):
        urls_ = []
        for i in urls:
            # url = self.diffbot_url.format(i) + self.apikey
            url = self.diffbot_url.format(quote(i, encoding='utf8')) + self.apikey
            url_generator = GetterConfig.RAPIConfig(
                url, max_retry=self.max_retry, http_timeout=100)  # , max_limit=10
            urls_.append(url_generator)
        datas_list = []
        getter = ProcessFactory.create_getter(
            GetterConfig.RAPIBulkConfig(
                urls_, concurrency=self.con_))  # 采集文章
        if getter:
            async for items in getter:
                for item in items:
                    data_dict = {}
                    data_dict['id'] = item['id']
                    try:
                        data_dict["imageUrls"] = item['imageUrls']
                    except:
                        print('error id', item['id'])
                    try:
                        data_dict['content'] = item['content']
                    except:
                        print('error id', item['id'])
                    try:
                        data_dict['title'] = item['title']
                    except:
                        print('error id', item['id'])
                    # print(len(datas_list))
                    if len(datas_list) >= 50:  # 每获取50条数据，存储一次进redis
                        await ProcessFactory.create_writer(self.mongo_sel('google')).write([i for i in datas_list if i])
                        logging.info(
                            'writer diffbot-items to mongo length:{}'.format(len(datas_list)))
                        datas_list.clear()
                        datas_list.append(data_dict)
                    else:
                        datas_list.append(data_dict)
        if datas_list:
            await ProcessFactory.create_writer(self.mongo_sel('google')).write([i for i in datas_list if i])
            logging.info(
                'writer diffbot-items to redis length:{}'.format(len(datas_list)))
            datas_list.clear()

    async def main(self):
        await self.get_kws()  # 获取kws
        logging.info('post-schedule-kws length:{}'.format(len(self.kws)))
        tasks = [
            self.wiki_req(),
            self.google_req(),
        ]
        # no_data_kws = await asyncio.gather(*tasks)
        # await self.deal_posts('no_data_kws', no_data_kws)  # 存储无数据的kw
        # logging.info('no_data_kws:\n {}'.format(no_data_kws))
        # urls = await self.get_url()
        # await self.diffbot_req(urls)
        # await self.mongo_to_json()
        # await self.zip_files()
        await self.oss_deal()

    async def oss_deal(self):
        logging.info('oss upload start, please wait a min')
        auth = oss2.Auth('7JT8SvX8zSsudTIb', 'XawzZPcAXLdapOx5Rn1nptjD7r3Z5U')
        bucket = oss2.Bucket(
            auth,
            'oss-cn-shenzhen.aliyuncs.com',
            'common-data')
        file_ = '{}.zip'.format('wode_xj')
        filename = os.getcwd() + '/' + file_
        # oss2.resumable_upload(
        #     bucket=bucket,
        #     key=file_,
        #     filename=filename,
        #     store=oss2.ResumableStore(
        #         root='/tmp'),
        #     multipart_threshold=4 *
        #                         1024 *
        #                         1024 *
        #                         1024,
        #     part_size=1024 *
        #               1024 *
        #               1024,
        #     num_threads=1,
        # )
        # logging.info('oss upload stop: %s file is ok' % (file_,))
        bucket = oss2.Bucket(
            auth,
            'oss-cn-shenzhen.aliyuncs.com', 'common-data')
        url = bucket.sign_url('GET', key=file_, expires=3600 * 24 * 10)
        logging.info('oss_url :\n {} \n'.format(url))

    async def mongo_to_json(self):
        for i in ['google', 'wiki']:
            self.data_list['{}'.format(i)] = GetterConfig.RMongoConfig('{}'.format(i),
                                                                       host=self.host,
                                                                       port=20020,
                                                                       username='wm_terms',
                                                                       password='qwer',
                                                                       database='wm',
                                                                       encoding='utf8',
                                                                       per_limit=1000)
        for i, j in self.data_list.items():
            redis_getter = ProcessFactory.create_getter(j)
            async for items in redis_getter:
                for item in items:
                    item.pop('_id')
                    await self.deal_posts(i, item)

    async def zip_files(self):
        zipf = zipfile.ZipFile(
            '{}.zip'.format(self.file_), 'w')
        file_ = os.getcwd() + '/{}'.format(self.file_)
        pre_len = len(os.path.dirname(file_))
        for parent, dir_names, file_names in os.walk(file_):
            # print(parent, dir_names, file_names)
            for filename in file_names:
                path_file = os.path.join(parent, filename)
                arc_name = path_file[pre_len:].strip(os.path.sep)  # 相对路径
                zipf.write(path_file, arc_name)
        zipf.close()

    async def deal_posts(self, i, item):
        file_ = os.getcwd()
        file_1 = file_ + '/{}/'.format(self.file_)
        if not os.path.exists(file_1):  # 判断当前路径是否存在，没有则创建file_1文件夹
            os.makedirs(file_1)
        file_post = file_1 + '/' + '{}.txt'.format(i)
        async with aiofiles.open(file=file_post, mode='a+', encoding='utf8') as f:
            await f.write(str(item) + '\n')
            await f.flush()

    async def get_url(self):
        mongo_col = GetterConfig.RMongoConfig('google',
                                              host=self.host,
                                              port=20020,
                                              username='wm_terms',
                                              password='qwer',
                                              database='wm',
                                              encoding='utf8',
                                              per_limit=1000,
                                              )
        redis_getter = ProcessFactory.create_getter(mongo_col)
        urls = []
        async for items in redis_getter:
            for item in items:
                url = item['url']
                urls.append(url)
        return urls


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(XJSpider().main())
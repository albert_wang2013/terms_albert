# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/4/23 14:58
@Author  : Albert
@Func    : 
@File    : hotel_test.py
@Software: PyCharm
"""
# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/4/10 11:23
@Author  : Albert
@Func    :
@File    : test.py
@Software: PyCharm
"""
import asyncio
import logging
from idataapi_transform import ProcessFactory, GetterConfig
from idataapi_transform.DataProcess.Config.ConfigUtil import WriterConfig
from decimal import Decimal

logging.basicConfig(level=logging.INFO)


class es_mongo_es():
    def __init__(self):
        t = 'in'
        if t == 'out':
            self.es_read = ["120.76.205.241:8000"]
            self.es_writer = ['10.29.71.97:9900']
        else:
            self.es_read = ['10.29.71.97:9900']
            self.es_writer = ['10.29.71.97:9900']

    async def read_es(self):
        """
        :return: 读取es中数据，返回结果
        """
        # query_body = {"size": "1000",
        #               "query": {"bool": {
        #                   "filter": [
        #                       {"terms": {"appCode": ["qunar", "meituan", "dossen", "ctrip", "elong"]}} #"qunar", "meituan", "dossen", "ctrip",
        #                   ]}
        #               }
        #               }  # "ctrip","qunar","elong","meituan","dossen"
        query_body = {  "size": "100",
                        "query": {
                            "bool": {
                                "filter": [
                                    {"term": {"commentCount": 0}}
                                ]
                            }
                        }
                    }
        # headers = {"Host": "newes", "apikey": "42aadd74e8ef0d26676a2cff74ce6923", "Content-Type": "application/json"}
        headers = {"Content-Type": "application/json"}
        collect_ = 'poi_unified_hotel_v7_05m07d'
        es_config = GetterConfig.RESConfig(collect_, 'hotel', hosts=self.es_read,
                                           headers=headers, query_body=query_body, scroll='50m')
        es_getter = ProcessFactory.create_getter(es_config)  # 10.26.222.219
        async for items in es_getter:  # 每次去特定size返回
            datas = []
            for item in items:
                # print(item)
                if item:
                    # item = await self.deal_sentimentDist(item)
                    item = await self.deal_commentcount(item)
                    if item:
                        datas.append(item)
                # print('\n\n\n\n')
                # print(datas)
            if datas:
                await self.data_es(datas)

    async def deal_sentimentDist(self, item):
        """
        :param item:
        :return: add sentimentDist
        """
        if not item['sentimentDist']:
            if item['comments']:
                comments_len = len(item['comments'])
                sum_confidence = 0
                sum_negative = 0
                sum_positive = 0
                for i in item['comments']:
                    if 'sentimentDist' in i:
                        if i['sentimentDist']:
                            # print(i['sentimentDist']['confidence'])
                            sum_confidence += i['sentimentDist']['confidence']
                            sum_negative += i['sentimentDist']['negative']
                            sum_positive += i['sentimentDist']['positive']
                # print(Decimal(sum_confidence / comments_len).quantize(Decimal('0.0000')))
                item['sentimentDist'] = {
                    "confidence": float(Decimal(sum_confidence / comments_len).quantize(Decimal('0.0000'))),
                    "negative": float(Decimal(sum_negative / comments_len).quantize(Decimal('0.0000'))),
                    "positive": float(Decimal(sum_positive / comments_len).quantize(Decimal('0.0000'))),
                }
            else:
                item['sentimentDist'] = None
            return item
        else:
            return None

    async def deal_commentcount(self, item):
        # 90923818
        if item['commentCount'] == 0:
            if item['comments']:
                nums = len(item['comments'])
                item['commentCount'] = nums
                return item
        else:
            return None

    async def data_es(self, datas):
        try:
            logging.info('writer es_datas to es length:{}'.format(len(datas)))
            # es_config = WriterConfig.WESConfig("poi_unified_hotel_v7", "hotel", hosts=["127.0.0.1:9200"])
            es_config = WriterConfig.WESConfig("poi_unified_hotel_v7_05m07d", "hotel", hosts=self.es_writer, )
            es_writer = ProcessFactory.create_writer(es_config)
            await es_writer.write(datas)
        except Exception as e:
            print('error:{}'.format(e))


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(es_mongo_es().read_es())
    print('run_end')



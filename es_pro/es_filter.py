# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/3/29 15:04
@Author  : Albert
@Func    : 
@File    : es_filter.py
@Software: PyCharm
"""
import asyncio
import json
import logging
import time
from datetime import datetime
from idataapi_transform import ProcessFactory, GetterConfig
from idataapi_transform.DataProcess.Config.ConfigUtil import WriterConfig
from tornado import escape

""" 无该条件，就无法输出到控制台 """
logging.basicConfig(level=logging.INFO)


class es_mongo_es():
    def __init__(self):
        t = 'out'
        if t == 'out':
            self.es_read = ["120.76.205.241:8000"]
            self.es_writer = ['10.29.71.97:9900']
            self.mongo_ = '120.79.97.220'
            self.host = 'http://api01.bitspaceman.com:8000'
        else:
            self.es_read = ["10.26.222.219:8000"]
            self.es_writer = ['10.29.71.97:9900']
            self.mongo_ = '10.30.0.53'
            self.host = 'http://10.26.222.219:8000'
        self.ctrip_url = self.host + '/comment/ctrip?id={}&parent=hotel&sort=0&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
        self.qunar_url = self.host + '/comment/qunar?id={}&parent=hotel&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
        self.elong_url = self.host + '/comment/elong?id={}&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
        self.meituan_url = self.host + '/comment/meituan?id={}&cityid={}&sortType=time&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
        self.dossen_url = self.host + '/comment/dossen?id={}&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
        self.api_ = 'http://api01.bitspaceman.com:8000/nlp/sentiment/bitspaceman?apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT&industry=hotel&text={}'

    async def deal_item(self, data):
        """
        :param data:
        :return: 评论去重
        """
        if 'comments' in data:
            if data['comments']:
                comments_ = []
                ids_ = []
                for comment in data['comments']:
                    id_ = comment['id']
                    if id_ not in ids_:
                        ids_.append(id_)
                        comments_.append(comment)
                ids_ = []
                data['comments'] = comments_
        return data

    async def read_es(self):
        """
        :return: 读取es中数据，返回结果
        """
        query_body = {"size": "100",
                      }
        collect_ = 'product'
        es_config = GetterConfig.RESConfig(collect_, 'product', hosts=['10.29.71.97:9900'],
                                           query_body=query_body, max_retry=3, scroll='25m')  # , max_limit=2
        es_getter = ProcessFactory.create_getter(es_config)  # 10.26.222.219
        async for items in es_getter:  # 每次去特定size返回
            datas = []
            for item in items:
                item = await self.deal_item(item)
                datas.append(item)
            await self.data_es(datas)

    async def data_es(self, datas):
        try:
            logging.info('writer es_datas to es length:{}'.format(len(datas)))
            es_config = WriterConfig.WESConfig("product1", "product", hosts=['10.29.71.97:9900'], )
            es_writer = ProcessFactory.create_writer(es_config)
            await es_writer.write(datas)
        except Exception as e:
            print('error:{}'.format(e))


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(es_mongo_es().read_es())
    print('run_end')
    logging.info('run_end')
    time.sleep(20 * 60 * 60)
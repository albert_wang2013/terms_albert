# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/3/26 15:09
@Author  : Albert
@Func    :
@File    : es_product.py
@Software: PyCharm
"""
import hashlib
import time
from pprint import pprint
import aiohttp
import asyncio
import json
import logging
import os
import pathlib
import re
import aiofiles
from aiohttp import ClientSession
from idataapi_transform import ProcessFactory
from idataapi_transform.DataProcess.Config.ConfigUtil import GetterConfig, WriterConfig
from tornado import escape

""" 无该条件，就无法输出到控制台 """
logging.basicConfig(level=logging.INFO)



class esProduct():
    def __init__(self):
        # self.basepath_product = '/root/jd/product/'
        # self.basepath_comment = '/root/jd/comment/'
        # self.basepath_product = '/root/tmall/product/'
        # self.basepath_comment = '/root/tmall/comment/'
        # self.basepath_product = '/Users/wangalbert/Downloads/jd/product/'
        # self.basepath_comment = '/Users/wangalbert/Downloads/jd/comment'
        self.basepath_product = '/Users/wangalbert/Downloads/tmall/product/'
        self.basepath_comment = '/Users/wangalbert/Downloads/tmall/comment'
        self.api_ = 'http://api01.bitspaceman.com:8000/nlp/sentiment/bitspaceman?apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT&industry=restaurant&text={}'
        self.es_writer = ['10.29.71.97:9900']


    async def aio_files_deal(self):
        dirs_p = []     # 商品
        dirs_c = []     # 评论
        dir_p = pathlib.Path(self.basepath_product).iterdir()
        for dir_p1 in dir_p:
            dir_p1 = pathlib.Path(dir_p1).iterdir()
            for dir_p2 in dir_p1:
                dirs_p.append(str(dir_p2))  # 转成/User.....age_0.json这种形式
        dir_c = pathlib.Path(self.basepath_comment).iterdir()
        for dir_c1 in dir_c:
            if 'DS_Store' not in str(dir_c1):
                dir_c1 = pathlib.Path(dir_c1).iterdir()
                for dir_c2 in dir_c1:
                    dirs_c.append(str(dir_c2))
        print(len(dirs_p), len(dirs_c))
        # dirs_p = ['/Users/wangalbert/Downloads/tmall/product/剑南春酒/page_5.json',
        #           '/Users/wangalbert/Downloads/tmall/product/剑南春酒/page_1.json']
        for file_ in dirs_p:
            brandNameU = re.match('.*product/(.*?)/page.*?', str(file_)).groups()
            brandNameU = brandNameU[0]
            async with aiofiles.open(file_, encoding='utf8', mode='r') as f:
                datas = json.loads(await f.read())
                appCode = datas['appCode']
                datas = datas['data']
                es_datas = []
                for data in datas:
                    # if '1115572416' in data['id']:
                    data['brandNameU'] = brandNameU
                    data['appCode'] = appCode
                    es_data = await self.deal_data(data, dirs_c)
                    logging.info('fiel:{}, id:{}'.format(file_, data['id']))
                    es_datas.append(es_data)
                await self.data_es(es_datas)


    async def data_es(self, es_datas):
        logging.info('writer es_datas to es length:{}'.format(len(es_datas)))
        for data in es_datas:
            # es_config = WriterConfig.WESConfig("product", "product", hosts=["127.0.0.1:9200"], id_hash_func=self.default_id_hash_func)
            es_config = WriterConfig.WESConfig("product", "product", hosts=self.es_writer, id_hash_func=self.default_id_hash_func)
            es_writer = ProcessFactory.create_writer(es_config)
            await es_writer.write([data])


    @staticmethod
    def default_id_hash_func(item):
        if "id" in item and item["id"]:
            value = (item["id"]).encode("utf8")
            # print(hashlib.md5(value).hexdigest())
            return hashlib.md5(value).hexdigest()


    async def deal_data(self, data, dirs_c):
        if 'commentCount' in data:
            if data['commentCount'] > 0:
                product_id = data['id']
                comment_files = [x for x in dirs_c if product_id in x]
                if len(comment_files) > 0:
                    logging.info('product_id:{}, comments:{}, list:{}, comments_files_length:{}'.
                                 format(product_id, data['commentCount'], 1, len(comment_files)))
                    comments_data1 = await self.deal_comment(comment_files)
                    comments_data = [i for i in comments_data1 if i['sentimentDist']]
                    logging.info('len comments_data before:{}; len comments_data after:{}'.
                                 format(len(comments_data1), len(comments_data)))
                    print(comments_data)
                    data['comments'] = comments_data
                    return data
                else:
                    data['comments'] = None
                    return data
            else:
                data['comments'] = None
                return data
        else:
            data['comments'] = None
            return data



    async def deal_comment(self, comment_files):
        comments = list()
        for comment_file in comment_files:
            async with aiofiles.open(comment_file, encoding='utf8', mode='r') as f:
                datas = json.loads(await f.read())
                datas = datas['data']
                for i in datas:
                    comments.append(i)
        logging.info('deal_comment func() comments_length:{}'.format(len(comments)))
        # logging.info('start time:{}'.format(time.time()))
        # comments = await self.download_all_sites(comments)
        comments = await self.comments_url(comments)
        # logging.info('end time:{}'.format(time.time()))
        return comments


    async def comments_url(self, comments):
        for comment in comments:
            content = comment['content']
            url = self.api_.format(escape.url_escape(content))
            url_generator = GetterConfig.RAPIConfig(url, max_retry=5)    #  keep_other_fields=True, keep_fields=("confidence", "sentimentDist")
            getter = ProcessFactory.create_getter(GetterConfig.RAPIBulkConfig([url_generator]))
            if getter:
                async for items in getter:
                    for item in items:
                        sentimentDist = {}
                        sentimentDist['confidence'] = item['confidence']
                        item = item['sentimentDist']
                        for i in item:
                            if i['type'] == 'positive':
                                sentimentDist['positive'] = i['prob']
                            elif i['type'] == 'negative':
                                sentimentDist['negative'] = i['prob']
                        if 'rating' in comment:
                            if comment['rating']:
                                if comment['rating'] >= 3:
                                    if sentimentDist['positive'] - sentimentDist['negative'] < 0:
                                        sentimentDist['positive'], sentimentDist['negative'] = sentimentDist['negative'], sentimentDist['positive']
                        comment['sentimentDist'] = sentimentDist
            if 'sentimentDist' not in comment:
                comment['sentimentDist'] = None
        return comments


    async def url_request(self, session, comment):
        content = comment['content']
        url = self.api_.format(content)
        async with session.get(url) as response:
            # try:
                response = json.loads(await response.read())
                if 'retcode' in response:
                    if str(response['retcode']) in ['100005']:
                        logging.info('error1:{}'.format(response))
                        comment['sentimentDist'] = None
                        return comment
                if 'retcode' in response:
                    if str(response['retcode']) == '100703':
                        # raise aiohttp.ClientConnectionError('hahahah')
                        raise 1
                sentimentDist = {}
                sentimentDist['confidence'] = response['confidence']
                for i in response['sentimentDist']:
                    if i['type'] == 'positive':
                        sentimentDist['positive'] = i['prob']
                    elif i['type'] == 'negative':
                        sentimentDist['negative'] = i['prob']
                if 'rating' in comment:
                    if comment['rating'] >= 3:
                        if sentimentDist['positive'] - sentimentDist['negative'] < 0:
                            sentimentDist['positive'], sentimentDist['negative'] = sentimentDist['negative'], sentimentDist['positive']
                comment['sentimentDist'] = sentimentDist
                return comment
            # except:
            #     logging.info('error except:{}'.format(response))
            #     comment['sentimentDist'] = None
            #     return comment


    async def download_all_sites(self, comments):
        sema = asyncio.BoundedSemaphore(20)
        async with sema, aiohttp.ClientSession() as session:
            tasks = []
            for comment in comments:
                task = asyncio.ensure_future(self.url_request(session, comment))
                tasks.append(task)
            # logging.info('tasks length:{}'.format(len(tasks)))
            datas = await asyncio.gather(*tasks, return_exceptions=True)
            return datas


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(esProduct().aio_files_deal())
    print('run_end')
    logging.info('run_end')
    time.sleep(20*60*60)
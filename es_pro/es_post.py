# -*- coding: utf-8 -*-


import asyncio
import json
import logging
import time
from datetime import datetime
from idataapi_transform import ProcessFactory, GetterConfig
from idataapi_transform.DataProcess.Config.ConfigUtil import WriterConfig


""" 无该条件，就无法输出到控制台 """
logging.basicConfig(level=logging.INFO)



class es_mongo_es():
    def __init__(self):
        t = 'in'
        if t == 'out':
            self.es_read = ["120.76.205.241:8000"]
            self.es_writer = ['10.29.71.97:9900']
            self.mongo_ = '120.79.97.220'
        else:
            self.es_read = ["10.26.222.219:8000"]
            self.es_writer = ['10.29.71.97:9900']
            self.mongo_ = '10.30.0.53'



    async def deal_item(self, item):
        item['topkeyword'] = item['content']
        item.pop('content')
        query_id = item['id']
        # print(item)
        date_ = item['createDate'] + 3600*8     # 确保取到的时间正确
        date_str, _ = datetime.utcfromtimestamp(date_).strftime('%Y-%m-%dT%H:%M:%S').replace('-', '').split('T')
        collection_spider = 'data{}'.format(date_str)
        # print(collection_spider)
        query_body = {'_id': query_id}
        mongo_config = GetterConfig.RMongoConfig(collection_spider, host=self.mongo_, port=55555,
                                             username='writer', password='writer', database='spider',
                                              max_retry=2, query_body=query_body)  # 120.79.97.220   10.30.0.53
        mongo_getter = ProcessFactory.create_getter(mongo_config)
        async for items in mongo_getter:    # catId1, catName1, imageUrls, html(不空)，content
            # print(len(items))
            for i in items:
                if 'html' in i:
                    if i['html']:
                        item['catId1'] = None
                        item['catName1'] = None
                        if 'catId1' in i:
                            item['catId1'] = i['catId1']
                        if 'catName1' in i:
                            item['catName1'] = i['catName1']
                        item['imageUrls'] = i['imageUrls']
                        item['content'] = i['content']
                        item['html'] = i['html']
            # print(json.dumps(item))
            # return json.dumps(item)
            return item




    async def read_es(self):
        query_body = {"size": "100",
        "query": {

            "bool":{

                "filter":[

                {"terms":{"spamCode": [0]}},

                {"terms":{"appCode": ["toutiao.com","sina.cn","qq.com","eastday.com","yidianzixun.com","ifeng.com", "sohu.com","163.com"]}},

                {"exists":{"field":"title"}},

                {"exists":{"field":"content"}},

                {"exists":{"field":"entities"}},

                {"exists":{"field":"catLabel1"}},

                {"exists":{"field":"catLabel2"}},

                {"exists":{"field":"sentimentDist"}},

                {"exists":{"field":"sentimentDistTitle"}},

                {"exists":{"field":"posterId"}},

                {"exists":{"field":"posterScreenName"}},

                {"exists":{"field":"publishDate"}},

                {"exists":{"field":"likeCount"}},

                {"exists":{"field":"url"}}

                ]

            }

        }

    }
        headers = {"Host": "datasearch", "apikey": "42aadd74e8ef0d26676a2cff74ce6923", "Content-Type": "application/json"}
        for i in range(20190301, 20190331):
            collect_ = 'article' + str(i)
            es_config = GetterConfig.RESConfig(collect_, "post", hosts=self.es_read,
                                               headers=headers, query_body=query_body)
            es_getter = ProcessFactory.create_getter(es_config)     # 10.26.222.219
            json_datas = []
            try:
                async for items in es_getter:
                    for item in items:
                        item = await self.deal_item(item)
                        json_datas.append(item)
                    await self.data_es(json_datas)
                    json_datas = []
            except:
                pass



    async def data_es(self, json_datas):
        logging.info(len(json_datas))
        # es_config = WriterConfig.WESConfig("article201903", "post", hosts=["127.0.0.1:9200"])
        es_config = WriterConfig.WESConfig("article201903", "post", hosts=self.es_writer)
        es_writer = ProcessFactory.create_writer(es_config)
        await es_writer.write(json_datas)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(es_mongo_es().read_es())
    # print('start sleep')
    time.sleep(3600*24)

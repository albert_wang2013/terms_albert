# # -*- coding: utf-8 -*-
#
# """
# @Python version:3.6+
# @Time    : 2019/4/10 11:23
# @Author  : Albert
# @Func    :
# @File    : test.py
# @Software: PyCharm
# """
# # import asyncio
# #
# #
# # from idataapi_transform import ProcessFactory, GetterConfig
# # from idataapi_transform.DataProcess.Config.ConfigUtil import WriterConfig
# #
# #
# # async def read_es():
# #     """
# #     :return: 读取es中数据，返回结果
# #     """
# #     datas = set()
# #     query_body = {
# #                     "size": "1000",
# #                     "_source": ["brandNameU"]
# #                 }
# #     headers = {"Content-Type": "application/json"}
# #     collect_ = 'poi_unified_hotel_v7'
# #     es_config = GetterConfig.RESConfig(collect_, 'hotel', hosts=["10.29.71.97:9900"],
# #                                        headers=headers, query_body=query_body, scroll='5m')
# #     es_getter = ProcessFactory.create_getter(es_config)  # 10.26.222.219
# #     async for items in es_getter:  # 每次去特定size返回
# #         for item in items:
# #             name = item['brandNameU']
# #             datas.add(name)
# #     with open('test.txt', 'a+') as f:
# #         for i in datas:
# #             if i:
# #                 f.write(i+'\n')
# #                 f.flush()
# #
# # if __name__ == "__main__":
# #     loop = asyncio.get_event_loop()
# #     loop.run_until_complete(read_es())
# #     print('run_end')
# # -*- coding: utf-8 -*-
# # from decimal import Decimal
# #
# # import aiohttp
# # import asyncio
# # import json
# # import logging
# # import time
# # from datetime import datetime
# # from idataapi_transform import ProcessFactory, GetterConfig
# # from idataapi_transform.DataProcess.Config.ConfigUtil import WriterConfig
# # from tornado import escape
# #
# # from elasticsearch import Elasticsearch
# # from elasticsearch import helpers
# #
# # """ 无该条件，就无法输出到控制台 """
# # logging.basicConfig(level=logging.INFO)
# #
# #
# # class es_to_es():
# #     def __init__(self):
# #         t = 'in'
# #         if t == 'out':
# #             self.es_read = ["120.76.205.241:8000"]
# #             self.es_writer = ['10.29.71.97:9900']
# #             self.mongo_ = '120.79.97.220'
# #         else:
# #             self.es_read = ["10.29.71.97:9900"]
# #             self.es_writer = ['10.29.71.97:9900']
# #             self.mongo_ = '10.30.0.53'
# #
# #
# #     async def deal_item(self, item):
# #         if item['commentCount'] and item['comments']:
# #             comments_len = len(item['comments'])
# #             sum_confidence = 0
# #             sum_negative = 0
# #             sum_positive = 0
# #             for i in item['comments']:
# #                 if 'sentimentDist' in i:
# #                     if i['sentimentDist']:
# #                         # print(i['sentimentDist']['confidence'])
# #                         sum_confidence += i['sentimentDist']['confidence']
# #                         sum_negative += i['sentimentDist']['negative']
# #                         sum_positive += i['sentimentDist']['positive']
# #             # print(Decimal(sum_confidence / comments_len).quantize(Decimal('0.0000')))
# #             item['sentimentDist'] = {
# #                 "confidence": float(Decimal(sum_confidence / comments_len).quantize(Decimal('0.0000'))),
# #                 "negative": float(Decimal(sum_negative / comments_len).quantize(Decimal('0.0000'))),
# #                 "positive": float(Decimal(sum_positive / comments_len).quantize(Decimal('0.0000'))),
# #             }
# #         else:
# #             item['sentimentDist'] = None
# #         return item
# #
# #
# #     async def read_es(self):
# #         """
# #         :return: 读取es中数据，返回结果
# #         """
# #         query_body = {"size": "200",
# #                       }
# #         collect_ = 'poi_unified_hotel_v7_23d'
# #         headers = {"Host": "datasearch", "apikey": "42aadd74e8ef0d26676a2cff74ce6923",
# #                    "Content-Type": "application/json"}
# #         # es_config = GetterConfig.RESConfig(collect_, 'product', hosts=["120.76.205.241:8000"], query_body=query_body, max_limit=100, max_retry=3, scroll='25m', headers=headers)  # , max_limit=2
# #         es_config = GetterConfig.RESConfig(collect_, 'hotel', hosts=['10.29.71.97:9900'],
# #                                            query_body=query_body, max_retry=3, scroll='25m')  # , max_limit=2
# #         es_getter = ProcessFactory.create_getter(es_config)  # 10.26.222.219
# #         async for items in es_getter:  # 每次去特定size返回
# #             datas = []
# #             for item in items:
# #                 item = await self.deal_item(item)
# #                 # print((item))
# #                 datas.append(item)
# #             await self.data_es(datas)
# #
# #     async def data_es(self, datas):
# #         try:
# #             logging.info('writer es_datas to es length:{}'.format(len(datas)))
# #             es_config = WriterConfig.WESConfig("poi_unified_hotel_v7_29d", "hotel", hosts=['10.29.71.97:9900'], )
# #             es_writer = ProcessFactory.create_writer(es_config)
# #             await es_writer.write(datas)
# #         except Exception as e:
# #             print('error:{}'.format(e))
# #
# #
# # if __name__ == "__main__":
# #     loop = asyncio.get_event_loop()
# #     loop.run_until_complete(es_to_es().read_es())
# #     print('run_end')
# #     logging.info('run_end')
# #     time.sleep(20 * 60 * 60)
# # -*- coding: utf-8 -*-
#
# """
# @Python version:3.6+
# @Time    : 2019/4/10 11:23
# @Author  : Albert
# @Func    :
# @File    : test.py
# @Software: PyCharm
# """
# import asyncio
# import logging
# import time
#
# from elasticsearch import Elasticsearch
# from idataapi_transform import ProcessFactory, GetterConfig
# from idataapi_transform.DataProcess.Config.ConfigUtil import WriterConfig
# logging.basicConfig(level=logging.INFO)
#
#
# class es_mongo_es():
#     def __init__(self):
#         t = 'in'
#         if t == 'out':
#             self.es_read = ["120.76.205.241:8000"]
#             self.es_writer = ['10.29.71.97:9900']
#         else:
#             self.es_read = ['10.29.71.97:9900']
#             self.es_writer = ['10.29.71.97:9900']
#
#     async def read_es(self):
#         """
#         :return: 读取es中数据，返回结果
#         """
#         query_body = {"size": "100",
#                       "query": {"bool": {
#                           "filter": [
#                               {"terms": {"appCode": ["qunar", "meituan", "dossen"]}}
#                           ]}
#                       }
#                       }  # "ctrip","qunar","elong","meituan","dossen"
#         # headers = {"Host": "datasearch", "apikey": "42aadd74e8ef0d26676a2cff74ce6923", "Content-Type": "application/json"}
#         headers = {"Content-Type": "application/json"}
#         collect_ = 'poi_unified_hotel_v7_29d'
#         es_config = GetterConfig.RESConfig(collect_, 'hotel', hosts=self.es_read,
#                                            headers=headers, query_body=query_body, scroll='50m')
#         es_getter = ProcessFactory.create_getter(es_config)  # 10.26.222.219
#         async for items in es_getter:  # 每次去特定size返回
#             datas = items
#             if datas:
#                 await self.data_es(datas)
#
#
#     async def data_es(self, datas):
#         try:
#             logging.info('writer es_datas to es length:{}'.format(len(datas)))
#             # es_config = WriterConfig.WESConfig("poi_unified_hotel_v7", "hotel", hosts=["127.0.0.1:9200"])
#             es_config = WriterConfig.WESConfig("poi_unified_hotel_v7_05m07d", "hotel", hosts=self.es_writer, )
#             es_writer = ProcessFactory.create_writer(es_config)
#             await es_writer.write(datas)
#         except Exception as e:
#             print('error:{}'.format(e))
#
#
#
#
# if __name__ == "__main__":
#     loop = asyncio.get_event_loop()
#     loop.run_until_complete(es_mongo_es().read_es())
#     print('run_end')
#
#
# -*- coding: utf-8 -*-
import aiohttp
import asyncio
import json
import logging
import time
from datetime import datetime
from idataapi_transform import ProcessFactory, GetterConfig
from idataapi_transform.DataProcess.Config.ConfigUtil import WriterConfig
from tornado import escape

from elasticsearch import Elasticsearch
from elasticsearch import helpers
""" 无该条件，就无法输出到控制台 """
logging.basicConfig(level=logging.INFO)

brand_list = ['和酒',
'舍得酒',
'五粮春酒',
'金徽酒',
'枝江酒',
'北大仓酒',
'白水杜康酒',
'沙洲优黄酒',
'古井贡酒',
'郎酒',
'卡思黛乐葡萄酒',
'董酒',
'太白酒',
'金种子酒',
'新天葡萄酒',
'西塘老酒',
'迎驾贡酒',
'梁丰酒',
'沱牌酒',
'国窖1573酒',
'尼雅葡萄酒',
'石库门酒',
'云南红葡萄酒',
'乌毡帽酒',
'金六福酒',
'红星二锅头酒',
'茅台酒',
'山城啤酒',
'通天葡萄酒',
'天佑德酒',
'香格里拉葡萄酒',
'金门高粱酒',
'琅琊台酒',
'劲酒',
'张裕葡萄酒',
'扳倒井酒',
'稻花香酒',
'嘉士伯啤酒',
'通化葡萄酒',
'剑南春酒',
'伊力特酒',
'酒鬼酒',
'鸭溪窖酒',
'五粮液酒',
'威龙葡萄酒',
'王朝葡萄酒',
'小糊涂仙酒',
'双沟大曲酒',
'龙徽葡萄酒',
'四特酒',
'雪花啤酒',
'河套王酒',
'江小白酒',
'孔乙己酒',
'古越龙山酒',
'杏花村酒',
'即墨老酒',
'口子酒',
'西夏王葡萄酒',
'奔富葡萄酒',
'赖永初酒',
'今世缘酒',
'唐宋黄酒',
'五粮醇酒',
'西凤酒',
'珠江啤酒',
'嘉善黄酒',
'燕京啤酒',
'女儿红酒',
'绍兴师爷酒',
'宁夏红葡萄酒',
'咸亨酒',
'青岛啤酒',
'衡水老白干酒',
'水井坊酒',
'赖茅酒',
'莫高葡萄酒',
'口子窖酒',
'泸州老窖酒',
'哈尔滨啤酒',
'椰岛鹿龟酒',
'浏阳河酒',
'杜康酒',
'贵州习酒',
'洋河大曲酒',
'绵竹大曲酒',
'金威啤酒',
'金沙窖酒',
'会稽山酒',
'唐宋酒',
'牛栏山酒',
'塔牌酒',
'百威啤酒',
'长城葡萄酒',
'丰收葡萄酒',
'平安宾馆',
'兴华宾馆',
'Q加',
'金海宾馆',
'岭南佳园连锁酒店',
'首旅如家酒店',
'凤凰宾馆',
'铂涛菲诺',
'希尔顿欢朋',
'肯定酒店',
'龙达宾馆',
'维也纳三好酒店',
'汉庭酒店',
'天一宾馆',
'威斯汀酒店',
'素柏·云酒店',
'阳光假日宾馆',
'富华酒店',
'华邑',
'艳阳天时尚旅店',
'鑫悦宾馆',
'君澜度假酒店',
'丽亭酒店',
'科逸连锁酒店',
'中油阳光',
'南苑宾馆',
'锦江之星品尚酒店',
'龙海宾馆',
'阳光纳里',
'威尼斯酒店',
'祥和宾馆',
'天隆宾馆',
'天天如家自助服务式公寓',
'巴里岛假日酒店',
'金利宾馆',
'山城宾馆',
'CitiGO',
'悦榕庄酒店',
'鸿祥宾馆',
'大唐宾馆',
'新品汉庭',
'旅居',
'鑫园宾馆',
'凯莱大饭店',
'仁和宾馆',
'天马酒店',
'金豪宾馆',
'柏高酒店',
'假日宾馆',
'华都商务酒店',
'城家酒店',
'派柏酒店',
'南都酒店',
'百时快捷酒店',
'维景国际酒店',
'星月宾馆',
'最佳西方酒店',
'佳捷酒店',
'金海湾宾馆',
'南方酒店',
'西苑宾馆',
'胜高酒店',
'索菲特酒店',
'星豪鸿连锁酒店',
'辉盛阁酒店',
'迎商精品酒店',
'朝阳宾馆',
'橙子酒店',
'万爱情侣酒店',
'维也纳公寓',
'永泰宾馆',
'锦江都城酒店',
'城市便捷酒店',
'桔子精选酒店',
'喜来登酒店',
'银河宾馆',
'三和商务宾馆',
'维纳斯国际酒店',
'睿士酒店',
'素柏.云',
'瑾程',
'富驿商旅酒店',
'丽景大酒店',
'驿家酒店',
'万达文华酒店',
'她他公寓',
'水月清华宾馆',
'宏运宾馆',
'格子微酒店',
'金港宾馆',
'向阳宾馆',
'雅高酒店',
'丽江宾馆',
'建国饭店',
'天福宾馆',
'金辉大酒店',
'书香世家酒店',
'佳缘宾馆',
'鸿泰宾馆',
'尚俭太空舱',
'春天连锁宾馆',
'阳光假日酒店',
'星程酒店',
'诚信宾馆',
'东方假日酒店',
'港湾印象',
'诗莉莉',
'福星宾馆',
'现代宾馆',
'瑞丰宾馆',
'芒果酒店',
'桔子水晶酒店',
'和颐至尚',
'和颐酒店',
'罗马假日宾馆',
'华宇宾馆',
'华美达酒店',
'速7宾馆',
'希岸DELUXE',
'驿家365酒店',
'雅阁宾馆',
'银都酒店',
'佰翔',
'豪门宾馆',
'天华宾馆',
'尚一特酒店',
'星豪鸿酒店',
'居家宾馆',
'桃源宾馆',
'华驿酒店',
'格林东方酒店',
'银海宾馆',
'幸福宾馆',
'雷克泰酒店',
'辰茂酒店',
'英迪格酒店',
'龙泉宾馆',
'希岸·轻雅',
'7天优品',
'中州商务酒店',
'贝壳酒店',
'首旅南苑',
'家和宾馆',
'海天大酒店',
'从化碧水新村别墅',
'斑斓.家',
'皇冠大酒店',
'驿云客栈',
'嘉利华连锁酒店',
'月友连锁酒店',
'星期八宾馆',
'花间堂酒店',
'开元曼居',
'祥龙宾馆',
'金海湾大酒店',
'天和大酒店',
'衡山酒店',
'三江宾馆',
'鸿福宾馆',
'丽都商务酒店',
'77连锁酒店',
'红旗宾馆',
'昆仑乐居商务酒店',
'大世界宾馆',
'如家商旅(金标)',
'优程八桂酒店',
'桔子酒店',
'蓝海酒店',
'东城宾馆',
'米兰风尚',
'精途酒店',
'吉泰酒店',
'金都大酒店',
'星河宾馆',
'阳光商务酒店',
'如家精选酒店',
'运7酒店',
'雅客宾馆',
'中旅酒店',
'雅居酒店',
'维也纳好眠酒店',
'黄河宾馆',
'维也纳国际酒店',
'东湖宾馆',
'睿柏.云',
'金鹏宾馆',
'金华宾馆',
'万里路国际青年旅舍',
'欣燕都酒店',
'锦绣宾馆',
'纽宾凯酒店',
'康莱德酒店',
'简爱时尚宾馆',
'安缦酒店',
'旺角宾馆',
'天成宾馆',
'安逸158连锁酒店',
'欢墅酒店',
'明珠商务宾馆',
'鑫旺宾馆',
'天骄宾馆',
'盛景怡家酒店',
'海韵宾馆',
'阳光宾馆',
'恒8连锁酒店',
'八一宾馆',
'泰和宾馆',
'和颐至格',
'凯旋门大酒店',
'怡园宾馆',
'如一连锁',
'马哥孛罗酒店',
'漫心酒店',
'凯悦酒店',
'隐沫',
'锦华宾馆',
'有间客栈',
'中天宾馆',
'名都宾馆',
'八号连锁酒店',
'凯旋宾馆',
'君莱酒店',
'平安旅馆',
'馨乐庭',
'小憩驿站',
'新东方宾馆',
'逸米酒店',
'汉驿连锁旅店',
'鑫源宾馆',
'国际青年旅舍',
'喆啡',
'天天宾馆',
'99优选酒店',
'唐山鑫源大酒店',
'豪庭宾馆',
'郁锦香',
'派酒店',
'天顺宾馆',
'华兴宾馆',
'瑞祥宾馆',
'华府宾馆',
'诺庭连锁酒店',
'亲的客栈',
'民政宾馆',
'诗柏.云',
'望江宾馆',
'登巴国际连锁客栈',
'52连锁',
'金泉宾馆',
'悦家宾馆',
'漫心度假',
'如家酒店',
'岷山酒店',
'新世界酒店',
'格盟',
'宏基宾馆',
'非繁',
'富都宾馆',
'鑫海宾馆',
'京华连锁',
'如家酒店·neo',
'怡家酒店',
'都市118酒店',
'艳阳天酒店',
'豪庭商务宾馆',
'君豪酒店',
'佳和宾馆',
'粤海酒店',
'金瑞宾馆',
'格林豪泰酒店',
'岭南佳园',
'雅乐轩酒店',
'天鹅恋酒店',
'福容酒店',
'都市花园酒店',
'怡和宾馆',
'艾居连锁酒店',
'紫金宾馆',
'星光宾馆',
'豪庭商务酒店',
'ZMAX潮漫',
'朋来宾馆',
'仟那酒店',
'布丁严选',
'榴莲小星',
'三友宾馆',
'白云宾馆',
'7天酒店',
'名典商旅酒店',
'都市客栈',
'怡佳宾馆',
'加利利',
'长虹宾馆',
'大众宾馆',
'丽都酒店',
'顺和商务宾馆',
'禧龙时钟旅馆',
'华庭宾馆',
'雷迪森酒店',
'银泉宾馆',
'白玉兰',
'戴斯酒店',
'睿柏酒店',
'宝隆居家酒店',
'华美宾馆',
'鑫豪宾馆',
'瑞吉酒店',
'学苑宾馆',
'田园宾馆',
'富丽华酒店',
'嘉源宾馆',
'吉祥旅馆',
'爱尊客',
'金星酒店',
'雅阁',
'快8连锁',
'万怡酒店',
'宜家酒店',
'萨维尔',
'同福客栈',
'汇源宾馆',
'东方宾馆',
'宝丰宾馆',
'柏悦酒店',
'天和宾馆',
'爆米花影院酒店',
'金丰宾馆',
'铂涛酒店',
'皇朝商务酒店',
'京伦扉缦',
'维景酒店',
'云栖酒店',
'青年阳光连锁酒店',
'M家社区',
'如家酒店·NEO',
'假日商务宾馆',
'远洋宾馆',
'希尔顿酒店',
'红星宾馆',
'青藤酒店',
'雅斯特酒店',
'三元宾馆',
'彦霖快捷酒店',
'雅轩宾馆',
'党校宾馆',
'金龙宾馆',
'去哪儿Q+酒店',
'一呆',
'红楼宾馆',
'希尔顿花园酒店',
'四海宾馆',
'便宜居',
'宏达宾馆',
'友谊宾馆',
'华苑宾馆',
'豪华精选',
'中州国际酒店',
'福源宾馆',
'沁园宾馆',
'好如家连锁',
'永安宾馆',
'便宜居连锁',
'天河宾馆',
'怡程酒店',
'恒大酒店',
'西城宾馆',
'登巴连锁酒店',
'恒源宾馆',
'富驿时尚酒店',
'华都大酒店',
'九九宾馆',
'IU酒店',
'舒心宾馆',
'天然居宾馆',
'派柏·云酒店',
'吉泰连锁酒店',
'中安之家',
'漫果公寓',
'贝斯特韦斯特酒店',
'乐居宾馆',
'佳捷连锁酒店',
'宏都宾馆',
'我的地盘主题酒店',
'假日酒店',
'水利宾馆',
'和平宾馆',
'悦榕庄',
'金龙大酒店',
'速8酒店',
'南山休闲会馆',
'世纪金源酒店',
'桃园宾馆',
'昆仑乐居',
'诺富特酒店',
'华泰宾馆',
'景泰宾馆',
'天地仁和',
'丽橙酒店',
'凯宾斯基酒店',
'华天宾馆',
'驿捷酒店',
'开元宾馆',
'非繁城品',
'城家精选民宿',
'君澜酒店',
'丽思卡尔顿酒店',
'尊茂酒店',
'觅你',
'朗豪酒店',
'八方连锁酒店',
'新城宾馆',
'柏丽酒店',
'安逸宾馆',
'天鹅恋情侣主题酒店',
'西湖宾馆',
'美居酒店',
'安泰宾馆',
'锐思特酒店',
'芙蓉宾馆',
'乐家酒店',
'华悦宾馆',
'天虹宾馆',
'华鑫宾馆',
'维也纳酒店',
'快乐驿站',
'华阳宾馆',
'金宇宾馆',
'华丽宾馆',
'智选假日酒店',
'遇见时尚',
'星月时尚连锁酒店',
'三和宾馆',
'明珠大酒店',
'嘉立连锁酒店',
'瑞鑫宾馆',
'龙都宾馆',
'华都宾馆',
'天地仁和酒店',
'城家公寓',
'鑫都宾馆',
'佳泰酒店',
'洲际酒店',
'金桥宾馆',
'宜居宾馆',
'福瑞宾馆',
'华龙宾馆',
'禧龙酒店',
'开元度假村酒店',
'金盾宾馆',
'长城宾馆',
'君悦酒店',
'金凤凰酒店',
'康乐宾馆',
'好客商务宾馆',
'顺达宾馆',
'商城宾馆',
'88新天地酒店',
'A家连锁酒店',
'龙门客栈',
'诚悦租住',
'金帝商务宾馆',
'南湖宾馆',
'新悦宾馆',
'宜程',
'悉本酒店',
'君怡酒店',
'99连锁酒店',
'途窝酒店',
'凯悦嘉轩',
'金泰宾馆',
'彩虹宾馆',
'宏泰宾馆',
'千里行客栈',
'美宜家连锁酒店',
'学府商务宾馆',
'喜洋洋宾馆',
'维也纳国际',
'家馨宾馆',
'宏源宾馆',
'舒悦宾馆',
'城市客栈',
'皇廷宾馆',
'中意宾馆',
'飘HOME连锁酒店',
'鑫盛宾馆',
'金地宾馆',
'康年',
'私享家',
'九洲宾馆',
'宜佳宾馆',
'五洲大酒店',
'雅鑫宾馆',
'华盛宾馆',
'柏曼酒店',
'旭日宾馆',
'锦江之星酒店',
'皇冠假日酒店',
'幸福公寓',
'华丰宾馆',
'丽都宾馆',
'一呆公寓',
'天天快捷酒店',
'金鼎宾馆',
'汉姆连锁',
'盛世宾馆',
'华侨城酒店',
'加利利连锁酒店',
'金叶宾馆',
'建国璞隐',
'新世界大酒店',
'海逸酒店',
'兄弟宾馆',
'维纳斯皇家酒店',
'佳鑫宾馆',
'永盛宾馆',
'鼎盛宾馆',
'华住酒店',
'格兰云天酒店',
'商业宾馆',
'名致酒店',
'佳佳宾馆',
'昆仑',
'舒雅宾馆',
'宜必思尚品酒店',
'白天鹅酒店',
'莫林风尚酒店',
'有家客栈',
'驿居酒店-金牌',
'阳光公寓',
'麗枫',
'驿居酒店',
'天元宾馆',
'宏盛宾馆',
'兰花花旺元快捷酒店',
'五彩今天酒店',
'星河湾',
'岭南精品',
'广源宾馆',
'皇冠宾馆',
'玉渊潭酒店',
'凯莱酒店',
'金园宾馆',
'全季酒店',
'温馨旅馆',
'布乙',
'南苑e家',
'天天快捷宾馆',
'锦江宾馆',
'五彩今天连锁酒店',
'悦来酒店',
'富豪宾馆',
'东方商务宾馆',
'朗廷酒店',
'如家商旅酒店',
'派柏.云',
'岭南精选',
'温馨宾馆',
'青年宾馆',
'开元大酒店',
'希岸',
'新天地酒店',
'鑫隆宾馆',
'都市花园',
'凯宾连锁',
'永兴宾馆',
'恒丰商务宾馆',
'红日宾馆',
'光明宾馆',
'丽景宾馆',
'汉庭优佳酒店',
'海友4.0',
'明宇酒店',
'松赞酒店',
'万豪',
'佳豪宾馆',
'金泰之家',
'方圆酒店',
'友好宾馆',
'金陵酒店',
'铂尔曼酒店',
'龙腾宾馆',
'都市宾馆',
'心悦宾馆',
'好家酒店',
'隐居酒店',
'尚一特连锁酒店',
'金诚宾馆',
'吉楚酒店',
'凯宾酒店',
'泛太平洋',
'豪生酒店',
'凯宾连锁酒店',
'亚米连锁酒店',
'山水宾馆',
'椰子水晶酒店',
'华天酒店',
'宜必思酒店',
'爱情公寓',
'东升宾馆',
'兴达宾馆',
'军供宾馆',
'一米阳光酒店公寓',
'金狮100连锁宾馆',
'文苑宾馆',
'优程酒店',
'金马宾馆',
'友家宾馆',
'亿旺',
'坤逸酒店',
'温德姆酒店',
'佳园宾馆',
'长城大酒店',
'万丽酒店',
'百合宾馆',
'天悦商务宾馆',
'静雅宾馆',
'锦江之星风尚酒店',
'东方大酒店',
'首旅建国',
'半岛酒店',
'雅阁酒店',
'OYO酒店',
'和家宾馆',
'君亭酒店',
'康铂',
'泊捷时尚酒店',
'格子微',
'唐拉雅秀',
'万里路连锁',
'凯瑞大酒店',
'斯维登酒店',
'鑫源旅馆',
'兴旺宾馆',
'途客中国酒店',
'新世纪酒店',
'爱航快捷酒店',
'亚朵酒店',
'龙城宾馆',
'和谐宾馆',
'世纪星连锁',
'喜达屋酒店',
'国贸大酒店',
'科逸酒店',
'君安宾馆',
'汇丰宾馆',
'云海宾馆',
'三星宾馆',
'南方宾馆',
'丽笙酒店',
'九龙宾馆',
'锦江商务酒店',
'美爵酒店',
'雅悦酒店',
'友缘宾馆',
'金都宾馆',
'香格里拉酒店',
'阳光大酒店',
'优程',
'城市之家',
'荣华宾馆',
'金悦宾馆',
'途家斯维登酒店',
'银座佳悦',
'格兰云天国际',
'兴隆宾馆',
'东盛宾馆',
'金河宾馆',
'嘉立酒店',
'龙凤宾馆',
'维多利亚大酒店',
'鑫苑宾馆',
'金沙宾馆',
'金城宾馆',
'W酒店',
'新星宾馆',
'万顺宾馆',
'爱尊客酒店',
'东海宾馆',
'鹏程宾馆',
'福临宾馆',
'文华东方',
'莫泰酒店',
'江南宾馆',
'锦城宾馆',
'骏怡酒店',
'春天宾馆',
'驿雲酒店',
'加州旅馆',
'京都宾馆',
'凤城宾馆',
'凯旋龙连锁',
'福鑫宾馆',
'福朋酒店',
'古南都',
'花筑酒店',
'华坤宾馆',
'途家酒店',
'茉莉花开连锁酒店',
'名人',
'汇鑫宾馆',
'景江宾馆',
'玉龙宾馆',
'华安宾馆',
'天缘宾馆',
'金顺宾馆',
'首旅如家',
'银丰宾馆',
'龙翔宾馆',
'万福宾馆',
'枫林宾馆',
'布丁酒店',
'皇都宾馆',
'青年·都市迷你酒店',
'Suite Novotel',
'99旅馆',
'铂乐精选',
'新时代商务宾馆',
'清沐酒店',
'金源宾馆',
'蓝天宾馆',
'吉祥宾馆',
'白天鹅宾馆',
'明珠宾馆',
'爱家公寓',
'海航大酒店',
'幸福旅馆',
'帝盛酒店',
'万隆宾馆',
'乐居酒店',
'锦江酒店',
'滨江宾馆',
'鑫鑫宾馆',
'窝趣',
'金苑宾馆',
'艾美酒店',
'凯莱度假',
'南山宾馆',
'海友酒店',
'碧桂园酒店',
'鑫泰宾馆',
'宜尚PLUS',
'可临宁连锁酒店',
'瓦当瓦舍',
'泊捷酒店',
'天源宾馆',
'HyattRegency',
'中山宾馆',
'xbed',
'睿柏·云酒店',
'金鹰酒店',
'天悦宾馆',
'恒泰宾馆',
'胜利宾馆',
'AAroom',
'鸿运宾馆',
'时代商务宾馆',
'北方宾馆',
'天龙宾馆',
'日航国际酒店',
'盛源宾馆',
'永生现代',
'她他会酒店公寓',
'澳斯特酒店',
'建国',
'云上四季民宿',
'亚朵轻居',
'华山宾馆',
'阿富尔',
'富源酒店',
'嘉利华连锁',
'云上四季酒店',
'迎商酒店',
'尚客优酒店',
'禧玥',
'古北水镇旅业',
'怡莱酒店',
'盛捷酒店',
'春天时尚',
'远洲酒店',
'良友宾馆',
'千禧酒店',
'游多多酒店',
'新龙门客栈',
'华联宾馆',
'凯悦大酒店',
'银河大酒店',
'东风宾馆',
'文化宾馆',
'佳家宾馆',
'香格里拉',
'帝豪大酒店',
'天海连锁酒店',
'开元名都酒店',
'爱家快捷酒店',
'一路同行',
'富驿酒店',
'金山宾馆',
'五洲宾馆',
'格林联盟酒店',
'欣悦宾馆',
'凯瑞宾馆',
'富豪酒店',
'瑞廷酒店',
'世纪星酒店',
'苹果酒店',
'6+1商务宾馆',
'金凯瑞宾馆',
'吉楚连锁酒店',
'金湖宾馆',
'嘉华酒店',
'东呈酒店',
'首旅京伦',
'易佰酒店',
'山水时尚酒店',
'青皮树酒店',
'新兴宾馆',
'邮电宾馆',
'八方宾馆',
'皇庭商务酒店',
'金桥商务宾馆',
'e家连锁酒店',
'金帝宾馆',
'雅客e家',
'铁路宾馆',
'安康宾馆',
'祥瑞宾馆',
'祥云宾馆',
'振兴宾馆',
'佳泰连锁宾馆',
'乌镇旅业酒店',
'开心宾馆',
'索特来酒店',
'绿洲宾馆',
'天宏宾馆',
'家悦宾馆',
'人和宾馆',
'嘉和宾馆',
'久栖酒店',
'雅诗阁酒店',
'城市之星',
'尚客快捷酒店',
'华亭宾馆',
'百度宾馆',
'万达嘉华',
'和颐至尊',
'潮漫',
'富丽宾馆',
'宜尚酒店',
'开元·曼居酒店',
'银座佳驿酒店',
'海天宾馆',
'同福宾馆',
'智尚酒店',
'文星连锁酒店',
'城市名人酒店',
'瓦当瓦舍酒店',
'万信酒店',
'红叶宾馆',
'金茂宾馆',
'金秋宾馆',
'金鑫宾馆',
'如意酒店',
'鸿达宾馆',
'万豪酒店',
'阳光商务宾馆',
'都市迷你连锁酒店',
'红茶馆酒店',
'云天宾馆',
'布丁驿',
'莲花宾馆',
'兴源宾馆',
'尚俭太空舱公寓',
'丰源宾馆',
'爱客宾馆',
'京伦饭店',
'锦都宾馆',
'精通酒店',
'星辰宾馆',
'驿居酒店-蓝牌',
'水木58',
'民族宾馆',
'圆正酒店',
'龙华宾馆',
'金广快捷酒店',
'阳光旅馆',
'皇朝宾馆',
'美豪酒店',
'枫叶宾馆',
'金福宾馆',
'交通宾馆',
'四季酒店',
'五悦景区连锁酒店',
'聚鑫宾馆',
'美宜家',
'万枫',
'私享家连锁酒店公寓',
'布丁精选',
'富华宾馆',
'天鸿宾馆',
'茉莉花开',
'云之尚酒店',
'双龙宾馆',
'铂顿',
'安逸158酒店',
'中州快捷酒店',
'爆米花酒店',
'凤凰大酒店',
'亚朵S酒店',
'禧龙宾馆',
'一家快捷酒店',
'YUNIK',
'世纪星连锁酒店',
'鸿源宾馆',
'飘HOME',
'鑫达宾馆',
'熊猫王子',
'柏丽艾尚',
]
# brand_list = ['茅台', ]   # '舍得酒','五粮春酒','金徽酒','英迪格酒店','龙泉宾馆','希岸·轻雅', '7天优品',
# data_list = ['20190411',
#             '20190412',
#             '20190413',
#             '20190414',
#             '20190415',
#             '20190416',
#             '20190417',
#             '20190418',
#             '20190419',
#             '20190420',
#             '20190421',
#             '20190422',
#             '20190423',
#             '20190424',
#             '20190425',
#             '20190426',
#             '20190427',
#             '20190428',
#             '20190429',
#             '20190430',]
data_list = [
            '20190427',
            '20190428',
            '20190429',
            '20190430','20190501', '20190502', '20190503', '20190504', '20190505', '20190506', '20190507', '20190508', '20190509', '20190510', '20190511', '20190512', '20190513', '20190514', '20190515', '20190516', '20190517', '20190518', '20190519', '20190520', '20190521', '20190522', '20190523', '20190524', '20190525', '20190526', '20190527']


class es_mongo_es():
    def __init__(self):
        t = 'in'
        if t == 'out':
            self.es_read = ["120.76.205.241:8000"]
            self.es_writer = ['10.29.71.97:9900']
            self.mongo_ = '120.79.97.220'
        else:
            self.es_read = ["10.26.222.219:8000"]
            self.es_writer = ['10.29.71.97:9900']
            self.mongo_ = '10.30.0.53'


    async def deal_item(self, item):
        item['topkeyword'] = item['content']
        item.pop('content')
        if 'copyDate' in item:
            item.pop('copyDate')
        if 'domain' in item:
            item.pop('domain')
        query_id = item['id']
        # print(item)
        date_ = item['createDate'] + 3600*8     # 确保取到的时间正确
        date_str, _ = datetime.utcfromtimestamp(date_).strftime('%Y-%m-%dT%H:%M:%S').replace('-', '').split('T')
        collection_spider = 'data{}'.format(date_str)
        # print(collection_spider)
        query_body = {'_id': query_id}  # { $in : [ 1, 2, 3, 4] }
        mongo_config = GetterConfig.RMongoConfig(collection_spider, host=self.mongo_, port=55555,
                                             username='writer', password='writer', database='spider',
                                              max_retry=2, query_body=query_body)  # 120.79.97.220   10.30.0.53
        mongo_getter = ProcessFactory.create_getter(mongo_config)
        async for items in mongo_getter:    # catId1, catName1, imageUrls, html(不空)，content
            # print(len(items))
            for i in items:
                if 'html' in i:
                    if i['html']:
                        item['catId1'] = None
                        item['catName1'] = None
                        if 'catId1' in i:
                            item['catId1'] = i['catId1']
                        if 'catName1' in i:
                            item['catName1'] = i['catName1']
                        item['imageUrls'] = i['imageUrls']
                        item['content'] = i['content']
                        item['html'] = i['html']
            # print(json.dumps(item))
            # return json.dumps(item)
            return item




    async def read_es(self):
        for brand in brand_list:
            logging.info('brand:{}'.format(escape.url_escape(brand)))
            query_body = {
                "size": "100",
                "query": {
                "bool":{
                "filter":[
                    {"terms":{"spamCode": [0,2]}},

                    {"exists":{"field":"content"}},

                    {"exists":{"field":"publishDate"}},

                    {"exists":{"field":"catLabel1"}},

                    {"exists":{"field":"sentimentDist"}},

                    {"bool":{"should":[

                    {"match_phrase":{"title":brand}},

                    {"match_phrase":{"content":brand}}

                    ]}}
                    ]
            }}}
            headers = {"Host": "datasearch", "apikey": "42aadd74e8ef0d26676a2cff74ce6923", "Content-Type": "application/json"}
            for i in data_list:
                collect_ = 'article' + str(i)
                es_config = GetterConfig.RESConfig(collect_, "post", hosts=self.es_read,
                                                   headers=headers, query_body=query_body)
                es_getter = ProcessFactory.create_getter(es_config)     # 10.26.222.219
                try:
                    json_datas = []
                    async for items in es_getter:
                        # print(len(items))
                        for item in items:
                            item = await self.deal_item(item)
                            json_datas.append(item)
                        if json_datas:
                            await self.data_es(json_datas)
                except:
                    pass



    async def data_es(self, json_datas):
        logging.info('writer es_datas to es length:{}'.format(len(json_datas)))
        # es_config = WriterConfig.WESConfig("article201904", "post", hosts=["127.0.0.1:9200"])
        es_config = WriterConfig.WESConfig("article201904", "post", hosts=self.es_writer)
        es_writer = ProcessFactory.create_writer(es_config)
        await es_writer.write(json_datas)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(es_mongo_es().read_es())
    print('run_end')
    logging.info('run_end')
    time.sleep(20 * 60 * 60)




# -*- coding: utf-8 -*-
# from decimal import Decimal
#
# import logging
# import aiohttp
# import asyncio
# import json
# import time
# from datetime import datetime
# from idataapi_transform import ProcessFactory, GetterConfig
# from idataapi_transform.DataProcess.Config.ConfigUtil import WriterConfig
# from tornado import escape
#
# from elasticsearch import Elasticsearch
# from elasticsearch import helpers
# """ 无该条件，就无法输出到控制台 """
# logging.basicConfig(level=logging.INFO)
#
#
#
# class es_mongo_es():
#     def __init__(self):
#         t = 'in'
#         if t == 'out':
#             self.es_read = ["120.76.205.241:8000"]
#             self.host = 'http://api01.bitspaceman.com:8000'
#         else:
#             self.es_read = ["10.29.71.97:9900"]
#             self.es_writer = ['10.29.71.97:9900']
#             self.host = 'http://10.26.222.219:8000'
#         self.elong_url = self.host + '/comment/elong?id={}&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
#         self.ctrip_url = self.host + '/comment/ctrip?id={}&parent=hotel&sort=0&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
#         self.ctrip_hotel_url = self.host + '/hotel/ctrip?checkInDate=2019-05-09&checkOutDate=2019-05-12&no_flat=0&id={}&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
#         self.elong_hotel_url = self.host + '/hotel/elong?id={}&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
#
#         self.api_ = 'http://api01.bitspaceman.com:8000/nlp/sentiment/bitspaceman?apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT&industry=hotel&text={}'
#         self.max_retry = 5
#
#
#     async def deal_comment(self, datas):
#         urls = []
#         for data in datas:
#             data['comments'] = []
#             if data['appCode'] == 'ctrip':
#                 url_ = self.ctrip_url.format(data['id'])
#             else:
#                 url_ = self.elong_url.format(data['id'])
#             url_generator = GetterConfig.RAPIConfig(url_, max_limit=200, max_retry=3, trim_to_max_limit=True)
#             urls.append(url_generator)
#         getter = ProcessFactory.create_getter(GetterConfig.RAPIBulkConfig(urls, concurrency=20))
#         if getter:
#             async for items in getter:  # 持续处理多个url_generator，
#                 # print('getter:{}'.format(len(items)))
#                 for item in items:
#                     for data in datas:
#                         # print(item)
#                         if item['referId'] == data['id']:   # 是对当前id对应的data中插入评论数据item
#                             item = await self.deal_sentimentdist(item)
#                             data['comments'].append(item)
#         for data in datas:
#             # print(len(data['comments']))
#             # print(data)
#             # print('length of data["comments"]:{}'.format(len(data['comments'])))
#             if len(data['comments']) == 0:
#                 data['comments'] = None
#             await self.deal_sentimentDist(data)
#         return datas
#
#
#     async def deal_sentimentdist(self, comment):
#         content = comment['content']
#         if content:
#             if len(content) > 900:
#                 content = content[:900]
#             url = self.api_.format(escape.url_escape(content))
#             url_generator = GetterConfig.RAPIConfig(url, max_retry=5)
#             getter = ProcessFactory.create_getter(GetterConfig.RAPIBulkConfig([url_generator]))
#             if getter:
#                 async for items in getter:
#                     for item in items:
#                         sentimentDist = {}
#                         sentimentDist['confidence'] = item['confidence']
#                         item = item['sentimentDist']
#                         for i in item:
#                             if i['type'] == 'positive':
#                                 sentimentDist['positive'] = i['prob']
#                             elif i['type'] == 'negative':
#                                 sentimentDist['negative'] = i['prob']
#                         comment['sentimentDist'] = sentimentDist
#         else:
#             if 'sentimentDist' not in comment:
#                 comment['sentimentDist'] = None
#         # print('comment:', comment)
#         return comment
#
#
#     async def deal_sentimentDist(self, item):
#         if item['commentCount'] and item['comments']:
#             comments_len = len(item['comments'])
#             sum_confidence = 0
#             sum_negative = 0
#             sum_positive = 0
#             for i in item['comments']:
#                 if 'sentimentDist' in i:
#                     if i['sentimentDist']:
#                         # print(i['sentimentDist']['confidence'])
#                         sum_confidence += i['sentimentDist']['confidence']
#                         sum_negative += i['sentimentDist']['negative']
#                         sum_positive += i['sentimentDist']['positive']
#             # print(Decimal(sum_confidence / comments_len).quantize(Decimal('0.0000')))
#             item['sentimentDist'] = {
#                 "confidence": float(Decimal(sum_confidence / comments_len).quantize(Decimal('0.0000'))),
#                 "negative": float(Decimal(sum_negative / comments_len).quantize(Decimal('0.0000'))),
#                 "positive": float(Decimal(sum_positive / comments_len).quantize(Decimal('0.0000'))),
#             }
#         else:
#             item['sentimentDist'] = None
#         return item
#
#
#     async def deal_items(self, datas):
#         urls = []
#         for data in datas:
#             # print(json.dumps(data),'\n')
#             # print('初始的：',(data['commentCount']),'\n')
#             if data['appCode'] == 'ctrip':
#                 url_ = self.ctrip_hotel_url.format(data['id'])
#             else:
#                 url_ = self.elong_hotel_url.format(data['id'])
#             url_generator = GetterConfig.RAPIConfig(url_, max_retry=3, trim_to_max_limit=True)
#             urls.append(url_generator)
#         getter = ProcessFactory.create_getter(GetterConfig.RAPIBulkConfig(urls, concurrency=30))
#         if getter:
#             async for items in getter:  # 持续处理多个url_generator，
#                 # print('getter:{}'.format(len(items)))
#                 for item in items:
#                     for data in datas:
#                         if item['id'] == data['id']:  # 是对当前id对应的data中插入评论数据item
#                             data['commentCount'] = item['commentCount']
#         if datas:
#             return datas
#
#
#
#
#     async def read_es(self):
#         """
#         :return: 读取es中数据，返回结果
#         """
#         query_body = {
#                     "size": "20",
#                     "query":{
#                         "bool":{
#                             "filter":[
#                                 {
#                                     "terms":{
#                                         "appCode":["ctrip",'elong']
#                                     }
#                                 }
#                             ]
#                         }
#                     }
#                 }   # "ctrip","qunar","elong","meituan","dossen"
#         headers = {"Host": "newes", "apikey": "42aadd74e8ef0d26676a2cff74ce6923", "Content-Type": "application/x-www-form-urlencoded"}
#         collect_ = 'poi_unified_hotel_v7_29d'
#         # es_config = GetterConfig.RESConfig(collect_, 'hotel', hosts=self.es_read,
#         #                                    headers=headers, max_limit=10, query_body=query_body, scroll='50m')
#         es_config = GetterConfig.RESConfig(collect_, 'hotel', hosts=self.es_read, query_body=query_body, scroll='50m')
#         es_getter = ProcessFactory.create_getter(es_config)     # 10.26.222.219
#         async for items in es_getter:   # 每次去特定size返回
#             datas = await self.deal_items(items)
#             # print((datas),'\n')
#             if datas:
#                 datas = await self.deal_comment(datas)
#                 await self.data_es(datas)
#
#
#     async def data_es(self, datas):
#         try:
#             logging.info('writer es_datas to es length:{}'.format(len(datas)))
#             # es_config = WriterConfig.WESConfig("poi_unified_hotel_v7_05d", "hotel", hosts=["127.0.0.1:9200"])
#             es_config = WriterConfig.WESConfig("poi_unified_hotel_v7_05m07d", "hotel", hosts=self.es_writer, )
#             es_writer = ProcessFactory.create_writer(es_config)
#             await es_writer.write(datas)
#         except Exception as e:
#             print('error:{}'.format(e))
#
#
# if __name__ == "__main__":
#     loop = asyncio.get_event_loop()
#     loop.run_until_complete(es_mongo_es().read_es())
#     print('run_end')
#     logging.info('run_end')
#     time.sleep(20 * 60 * 60)

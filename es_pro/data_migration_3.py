import asyncio
import json
import logging
import time
from elasticsearch import Elasticsearch
from idataapi_transform import ProcessFactory, GetterConfig
from idataapi_transform.DataProcess.Config.ConfigUtil import WriterConfig
logging.basicConfig(level=logging.INFO)


class es_mongo_es():
    def __init__(self):
        self.es_read = ["120.76.205.241:8000"]
        self.es_writer = ['elasticsearch:9200']

    async def read_es(self):
        """
        :return: 读取es中数据，返回结果
        """
        query_body = {"size": "1",
                      }
        headers = {"Host": "newes", "apikey": "42aadd74e8ef0d26676a2cff74ce6923", "Content-Type": "application/json"}
        collect_ = 'product05'
        es_config = GetterConfig.RESConfig(collect_, 'product', hosts=self.es_read,
                                           headers=headers, query_body=query_body, scroll='50m', max_limit=None)
        es_getter = ProcessFactory.create_getter(es_config)
        async for datas in es_getter:
            if datas:
                print(json.dumps(datas))
                # await self.data_es(datas)

    async def data_es(self, datas):
        try:
            logging.info('writer es_datas to es length:{}'.format(len(datas)))
            es_config = WriterConfig.WESConfig("product05", "product", hosts=self.es_writer, )
            es_writer = ProcessFactory.create_writer(es_config)
            await es_writer.write(datas)
        except Exception as e:
            print('error:{}'.format(e))


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(es_mongo_es().read_es())
    print('run_end')
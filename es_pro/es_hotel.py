# -*- coding: utf-8 -*-
import aiohttp
import asyncio
import json
import logging
import time
from datetime import datetime
from idataapi_transform import ProcessFactory, GetterConfig
from idataapi_transform.DataProcess.Config.ConfigUtil import WriterConfig
from tornado import escape

from elasticsearch import Elasticsearch
from elasticsearch import helpers
""" 无该条件，就无法输出到控制台 """
logging.basicConfig(level=logging.INFO)



class es_mongo_es():
    def __init__(self):
        t = 'out'
        if t == 'out':
            self.es_read = ["120.76.205.241:8000"]
            self.es_writer = ['10.29.71.97:9900']
            self.mongo_ = '120.79.97.220'
            self.host = 'http://api01.bitspaceman.com:8000'
        else:
            self.es_read = ["10.26.222.219:8000"]
            self.es_writer = ['10.29.71.97:9900']
            self.mongo_ = '10.30.0.53'
            self.host = 'http://10.26.222.219:8000'
        self.ctrip_url = self.host + '/comment/ctrip?id={}&parent=hotel&sort=0&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
        self.qunar_url = self.host + '/comment/qunar?id={}&parent=hotel&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
        self.elong_url = self.host + '/comment/elong?id={}&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
        self.meituan_url = self.host + '/comment/meituan?id={}&cityid={}&sortType=time&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
        self.dossen_url = self.host + '/comment/dossen?id={}&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
        self.api_ = 'http://api01.bitspaceman.com:8000/nlp/sentiment/bitspaceman?apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT&industry=hotel&text={}'
        self.max_retry = 5


    async def deal_item(self, data):
        """
        :param data:
        :return: 通过unifiedId查询，获取"brandName", "city", "star", "level"数据
        """
        try:
            id_ = data['unifiedId']
            query_body = {
                "_source": ["brandName", "city", "star", "level"],
                "query": {
                    "bool": {
                        "filter": [
                            {"term": {"id": id_}}
                        ]
                    }
                }
            }
            headers = {"Host": "datasearch", "apikey": "42aadd74e8ef0d26676a2cff74ce6923",
                       "Content-Type": "application/json"}
            collect_ = 'poi_merge_hotel_v7'
            es_config = GetterConfig.RESConfig(collect_, 'poiunified', hosts=self.es_read,
                                               headers=headers, query_body=query_body, scroll="5m")
            es_getter = ProcessFactory.create_getter(es_config)  # 10.26.222.219
            async for items in es_getter:
                for item in items:
                    data['brandNameU'] = item['brandName'] if 'brandName' in item else None
                    data['star'] = item['star'] if 'star' in item else None
                    data['cityU'] = item['city'] if 'city' in item else None
                    data['level'] = item['level'] if 'level' in item else None
                    data['comments'] = []
                if data['appCode'] == 'dossen':
                    data['ratingU'] = data['rating'] / 100
                else:
                    data['ratingU'] = data['rating'] / 5
                return data
        except Exception as e:
            print('now:{}'.format(e))


    async def deal_comment(self, datas):
        comments_data = []
        for data in datas:
            if data['commentCount']:
                if data['commentCount'] > 0:
                    comments_data.append(data)
        logging.info('length of comments_data:{}'.format(len(comments_data)))
        urls = []
        for data in comments_data:
            if data['appCode'] == 'ctrip':
                url_ = self.ctrip_url.format(data['id'])
            elif data['appCode'] == 'qunar':
                url_ = self.qunar_url.format(data['id'])
            elif data['appCode'] == 'elong':
                url_ = self.elong_url.format(data['id'])
            elif data['appCode'] == 'meituan':
                url_ = self.meituan_url.format(data['id'], data['cityId'])
            else:
                url_ = self.dossen_url.format(data['id'])
            url_generator = GetterConfig.RAPIConfig(url_, max_limit=200, max_retry=3, trim_to_max_limit=True)
            urls.append(url_generator)
        getter = ProcessFactory.create_getter(GetterConfig.RAPIBulkConfig(urls, concurrency=20))
        if getter:
            async for items in getter:  # 持续处理多个url_generator，
                # print('getter:{}'.format(len(items)))
                for item in items:
                    for data in comments_data:
                        if item['referId'] == data['id']:   # 是对当前id对应的data中插入评论数据item
                            item = await self.deal_sentimentdist(item)
                            data['comments'].append(item)
        for data in datas:
            # print(data)
            # print('length of data["comments"]:{}'.format(len(data['comments'])))
            if len(data['comments']) == 0:
                data['comments'] = None
        return datas


    async def deal_sentimentdist(self, comment):
        content = comment['content']
        # body = {
        #     "text": escape.url_escape(content)
        # }
        if content:
            if len(content) > 900:
                content = content[:900]
            url = self.api_.format(escape.url_escape(content))
            url_generator = GetterConfig.RAPIConfig(url, max_retry=5)
            getter = ProcessFactory.create_getter(GetterConfig.RAPIBulkConfig([url_generator]))
            if getter:
                async for items in getter:
                    for item in items:
                        sentimentDist = {}
                        sentimentDist['confidence'] = item['confidence']
                        item = item['sentimentDist']
                        for i in item:
                            if i['type'] == 'positive':
                                sentimentDist['positive'] = i['prob']
                            elif i['type'] == 'negative':
                                sentimentDist['negative'] = i['prob']
                        comment['sentimentDist'] = sentimentDist
        else:
            if 'sentimentDist' not in comment:
                comment['sentimentDist'] = None
        return comment


    async def read_es(self):
        """
        :return: 读取es中数据，返回结果
        """
        query_body = {"size": "100",
                    "query": {"bool":{
                        "filter":[
                        {"terms":{"appCode":["ctrip","qunar","elong","meituan"]}},
                        {"exists":{"field":"brandName"}},
                        {"exists":{"field":"address"}},
                        {"exists":{"field":"city"}},
                        {"exists":{"field":"rating"}},
                        {"exists":{"field":"geoPoint"}}
                        ]}
                        }
                    }   # "ctrip","qunar","elong","meituan","dossen"
        headers = {"Host": "datasearch", "apikey": "42aadd74e8ef0d26676a2cff74ce6923", "Content-Type": "application/json"}
        collect_ = 'poi_unified_hotel_v7'
        es_config = GetterConfig.RESConfig(collect_, 'poiunified', hosts=self.es_read,
                                           headers=headers, query_body=query_body, scroll='50m')
        es_getter = ProcessFactory.create_getter(es_config)     # 10.26.222.219
        async for items in es_getter:   # 每次去特定size返回
            datas = []
            for item in items:
                item = await self.item_find(item)
                if item:
                    item = await self.deal_item(item)
                    datas.append(item)
            if datas:
                datas = await self.deal_comment(datas)
                await self.data_es(datas)


    async def item_find(self, item):
        id_ = item['id']
        query_body = {
            "query": {
                "terms": {
                    "id": [
                        id_
                    ]}
            }
        }
        es_config = GetterConfig.RESConfig("poi_unified_hotel_v7", 'hotel', hosts=self.es_writer,
                                           query_body=query_body, scroll='5m', return_source=False)    # ['127.0.0.1:9200', ]
        es_getter = ProcessFactory.create_getter(es_config)  # 10.26.222.219
        async for items in es_getter:  # 每次去特定size返回
            total = items['hits']['total']
            logging.info("t['hits']['total']:{}".format(total))
            if total < 1:
                return item
            else:
                return None


    async def data_es(self, datas):
        try:
            logging.info('writer es_datas to es length:{}'.format(len(datas)))
            # es_config = WriterConfig.WESConfig("poi_unified_hotel_v7", "hotel", hosts=["127.0.0.1:9200"])
            es_config = WriterConfig.WESConfig("poi_unified_hotel_v7", "hotel", hosts=self.es_writer, )
            es_writer = ProcessFactory.create_writer(es_config)
            await es_writer.write(datas)
        except Exception as e:
            print('error:{}'.format(e))


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(es_mongo_es().read_es())
    print('run_end')
    logging.info('run_end')
    time.sleep(20 * 60 * 60)

from datetime import datetime
from elasticsearch import Elasticsearch

# 连接elasticsearch,默认是9200   curl "10.29.71.97:9900"
# es = Elasticsearch(['10.29.71.97:9900', ])
# es = Elasticsearch(['127.0.0.1:9200', ])
es = Elasticsearch(['elasticsearch:9200', ])


mapping_post = {"mappings": {
            "post": {
                "dynamic": "strict",
                "_all": {
                    "enabled": False
                },
                "properties": {
                    "abstract": {
                        "type": "text"
                    },
                    "appCode": {
                        "type": "keyword"
                    },
                    "appName": {
                        "type": "keyword"
                    },
                    "audioUrls": {
                        "type": "keyword"
                    },
                    "biz": {
                        "type": "keyword"
                    },
                    "brandId": {
                        "type": "keyword"
                    },
                    "catDist1": {
                        "properties": {
                            "label": {
                                "type": "keyword"
                            },
                            "score": {
                                "type": "float"
                            }
                        }
                    },
                    "catDist2": {
                        "properties": {
                            "label": {
                                "type": "keyword"
                            },
                            "score": {
                                "type": "float"
                            }
                        }
                    },
                    "catId1": {
                        "type": "keyword"
                    },
                    "catId2": {
                        "type": "keyword"
                    },
                    "catId3": {
                        "type": "keyword"
                    },
                    "catId4": {
                        "type": "keyword"
                    },
                    "catId5": {
                        "type": "keyword"
                    },
                    "catLabel1": {
                        "type": "keyword"
                    },
                    "catLabel2": {
                        "type": "keyword"
                    },
                    "catName1": {
                        "type": "keyword"
                    },
                    "catName2": {
                        "type": "keyword"
                    },
                    "catName3": {
                        "type": "keyword"
                    },
                    "catName4": {
                        "type": "keyword"
                    },
                    "catName5": {
                        "type": "keyword"
                    },
                    "catPathKey": {
                        "type": "keyword"
                    },
                    "city": {
                        "type": "keyword"
                    },
                    "cityRank": {
                        "type": "integer"
                    },
                    "clusterId": {
                        "type": "keyword"
                    },
                    "commentCount": {
                        "type": "integer"
                    },
                    "comments": {
                        "properties": {
                            "avatarUrl": {
                                "type": "keyword"
                            },
                            "commentCount": {
                                "type": "integer"
                            },
                            "commenterGradeName": {
                                "type": "keyword"
                            },
                            "commenterId": {
                                "type": "keyword"
                            },
                            "commenterIdGrade": {
                                "type": "keyword"
                            },
                            "commenterScreenName": {
                                "type": "keyword"
                            },
                            "commenterType": {
                                "type": "keyword"
                            },
                            "content": {
                                "type": "text"
                            },
                            "dislikeCount": {
                                "type": "integer"
                            },
                            "id": {
                                "type": "keyword"
                            },
                            "idGrade": {
                                "type": "keyword"
                            },
                            "imageUrls": {
                                "type": "keyword"
                            },
                            "likeCount": {
                                "type": "integer"
                            },
                            "publishDate": {
                                "type": "integer"
                            },
                            "publishDateStr": {
                                "type": "keyword"
                            },
                            "replies": {
                                "properties": {
                                    "content": {
                                        "type": "text"
                                    },
                                    "id": {
                                        "type": "keyword"
                                    },
                                    "likeCount": {
                                        "type": "integer"
                                    },
                                    "publishDate": {
                                        "type": "integer"
                                    },
                                    "publishDateStr": {
                                        "type": "keyword"
                                    },
                                    "referId": {
                                        "type": "keyword"
                                    },
                                    "replierId": {
                                        "type": "keyword"
                                    },
                                    "replierScreenName": {
                                        "type": "keyword"
                                    }
                                }
                            }
                        }
                    },
                    "content": {
                        "type": "text"
                    },
                    "country": {
                        "type": "keyword"
                    },
                    "coverUrl": {
                        "type": "keyword"
                    },
                    "createDate": {
                        "type": "integer"
                    },
                    "dataType": {
                        "type": "keyword"
                    },
                    "description": {
                        "type": "text"
                    },
                    "dislikeCount": {
                        "type": "integer"
                    },
                    "district": {
                        "type": "keyword"
                    },
                    "entities": {
                        "properties": {
                            "catName1": {
                                "type": "keyword"
                            },
                            "catName2": {
                                "type": "keyword"
                            },
                            "confidence": {
                                "type": "float"
                            },
                            "description": {
                                "type": "text"
                            },
                            "offset": {
                                "type": "integer"
                            },
                            "pos": {
                                "type": "keyword"
                            },
                            "word": {
                                "type": "keyword"
                            }
                        }
                    },
                    "favoriteCount": {
                        "type": "integer"
                    },
                    "fileOptions": {
                        "properties": {
                            "downloadCount": {
                                "type": "integer"
                            },
                            "format": {
                                "type": "keyword"
                            },
                            "height": {
                                "type": "integer"
                            },
                            "id": {
                                "type": "keyword"
                            },
                            "kbps": {
                                "type": "float"
                            },
                            "name": {
                                "type": "keyword"
                            },
                            "os": {
                                "type": "keyword"
                            },
                            "sizeM": {
                                "type": "float"
                            },
                            "url": {
                                "type": "keyword"
                            },
                            "version": {
                                "type": "keyword"
                            },
                            "width": {
                                "type": "integer"
                            }
                        }
                    },
                    "fileUrls": {
                        "type": "keyword"
                    },
                    "geoDist1": {
                        "properties": {
                            "label": {
                                "type": "keyword"
                            },
                            "score": {
                                "type": "float"
                            }
                        }
                    },
                    "geoDist2": {
                        "properties": {
                            "label": {
                                "type": "keyword"
                            },
                            "score": {
                                "type": "float"
                            }
                        }
                    },
                    "geoLabel1": {
                        "type": "keyword"
                    },
                    "geoLabel2": {
                        "type": "keyword"
                    },
                    "geoPoint": {
                        "type": "geo_point"
                    },
                    "hasCopyright": {
                        "type": "boolean"
                    },
                    "honourPost": {
                        "type": "boolean"
                    },
                    "html": {
                        "type": "text"
                    },
                    "id": {
                        "type": "keyword"
                    },
                    "idx": {
                        "type": "keyword"
                    },
                    "imageUrls": {
                        "type": "keyword"
                    },
                    "industryRank": {
                        "type": "integer"
                    },
                    "initialIndustry": {
                        "type": "keyword"
                    },
                    "isFree": {
                        "type": "boolean"
                    },
                    "isTop": {
                        "type": "boolean"
                    },
                    "keyValues": {
                        "properties": {
                            "key": {
                                "type": "keyword"
                            },
                            "value": {
                                "type": "text"
                            }
                        }
                    },
                    "likeCount": {
                        "type": "integer"
                    },
                    "location": {
                        "type": "keyword",
                        "fields": {
                            "text": {
                                "type": "text"
                            }
                        }
                    },
                    "mediaType": {
                        "type": "keyword"
                    },
                    "memberOnly": {
                        "type": "boolean"
                    },
                    "mid": {
                        "type": "keyword"
                    },
                    "origin": {
                        "type": "boolean"
                    },
                    "originContent": {
                        "type": "text"
                    },
                    "originUrl": {
                        "type": "keyword"
                    },
                    "parentAppCode": {
                        "type": "keyword"
                    },
                    "parentPostId": {
                        "type": "keyword"
                    },
                    "parentPosterId": {
                        "type": "keyword"
                    },
                    "parentPosterScreenName": {
                        "type": "keyword"
                    },
                    "popularity": {
                        "type": "integer"
                    },
                    "postBoardId": {
                        "type": "keyword"
                    },
                    "postBoardName": {
                        "type": "keyword"
                    },
                    "posterId": {
                        "type": "keyword"
                    },
                    "posterIdGrade": {
                        "type": "keyword"
                    },
                    "posterOriginId": {
                        "type": "keyword"
                    },
                    "posterScreenName": {
                        "type": "keyword"
                    },
                    "price": {
                        "type": "float"
                    },
                    "publishDate": {
                        "type": "integer"
                    },
                    "publishDateStr": {
                        "type": "keyword"
                    },
                    "queryWord": {
                        "type": "keyword"
                    },
                    "rank": {
                        "type": "integer"
                    },
                    "rating": {
                        "type": "float"
                    },
                    "ratingCount": {
                        "type": "integer"
                    },
                    "ratingDist": {
                        "properties": {
                            "key": {
                                "type": "keyword"
                            },
                            "value": {
                                "type": "float"
                            }
                        }
                    },
                    "reactions": {
                        "properties": {
                            "count": {
                                "type": "integer"
                            },
                            "type": {
                                "type": "keyword"
                            }
                        }
                    },
                    "readCount": {
                        "type": "integer"
                    },
                    "referId": {
                        "type": "keyword"
                    },
                    "relevance": {
                        "properties": {
                            "avgScore": {
                                "type": "float"
                            },
                            "maxScore": {
                                "type": "float"
                            }
                        }
                    },
                    "relevantIndustries": {
                        "type": "nested",
                        "properties": {
                            "industry": {
                                "type": "keyword"
                            },
                            "score": {
                                "type": "float"
                            }
                        }
                    },
                    "rootId": {
                        "type": "keyword"
                    },
                    "sentimentDist": {
                        "properties": {
                            "confidence": {
                                "type": "float"
                            },
                            "negative": {
                                "type": "float"
                            },
                            "positive": {
                                "type": "float"
                            }
                        }
                    },
                    "sentimentDistTitle": {
                        "properties": {
                            "confidence": {
                                "type": "float"
                            },
                            "negative": {
                                "type": "float"
                            },
                            "positive": {
                                "type": "float"
                            }
                        }
                    },
                    "shareCount": {
                        "type": "integer"
                    },
                    "sortId": {
                        "type": "integer"
                    },
                    "source": {
                        "type": "keyword"
                    },
                    "sourceRegion": {
                        "type": "keyword"
                    },
                    "sourceType": {
                        "type": "keyword"
                    },
                    "spamCode": {
                        "type": "integer"
                    },
                    "spamDist": {
                        "properties": {
                            "hits": {
                                "type": "keyword"
                            },
                            "label": {
                                "type": "keyword"
                            },
                            "score": {
                                "type": "float"
                            }
                        }
                    },
                    "spamLabel": {
                        "type": "keyword"
                    },
                    "state": {
                        "type": "keyword"
                    },
                    "subtitle": {
                        "type": "text"
                    },
                    "tags": {
                        "type": "keyword",
                        "fields": {
                            "text": {
                                "type": "text"
                            }
                        }
                    },
                    "thankCount": {
                        "type": "integer"
                    },
                    "title": {
                        "type": "text"
                    },
                    "token": {
                        "type": "keyword"
                    },
                    "topicWords": {
                        "type": "nested",
                        "properties": {
                            "score": {
                                "type": "float"
                            },
                            "word": {
                                "type": "keyword"
                            }
                        }
                    },
                    "topkeyword": {
                        "type": "text"
                    },
                    "travelDate": {
                        "type": "keyword"
                    },
                    "updateDate": {
                        "type": "integer"
                    },
                    "url": {
                        "type": "keyword"
                    },
                    "videoUrls": {
                        "type": "keyword"
                    },
                    "viewCount": {
                        "type": "long"
                    },
                    "writers": {
                        "properties": {
                            "id": {
                                "type": "keyword"
                            },
                            "name": {
                                "type": "keyword"
                            }
                        }
                    }
                }
            }
        },
    "settings": {
        "number_of_shards": '1',
        "number_of_replicas": '1'
    }
}

mapping_product = {"mappings": {
                "product": {
                    "_all": {"enabled": False},
                    "dynamic": "strict",
                     "properties": {"3cNo": {"type": "keyword"},
                                         "abstract": {"type": "text"},
                                         "activityPrice": {"type": "keyword"},
                                         "address": {"fields": {"text": {"type": "text"}},
                                                     "type": "keyword"},
                                         "agentCount": {"type": "integer"},
                                         "agents": {"properties": {"address": {"type": "keyword"},
                                                                   "country": {"type": "keyword"},
                                                                   "emails": {"type": "keyword"},
                                                                   "geoPoint": {"type": "geo_point"},
                                                                   "homeUrls": {"type": "keyword"},
                                                                   "id": {"type": "keyword"},
                                                                   "minOrder": {"type": "keyword"},
                                                                   "name": {"type": "keyword"},
                                                                   "price": {"type": "float"},
                                                                   "rating": {"type": "float"},
                                                                   "skuUnit": {"type": "keyword"},
                                                                   "taxInfo": {"type": "keyword"},
                                                                   "telephones": {"type": "keyword"},
                                                                   "updateDate": {"type": "integer"},
                                                                   "url": {"type": "keyword"},
                                                                   "year": {"type": "keyword"}}},
                                         "appCode": {"type": "keyword"},
                                         "appPrice": {"type": "float"},
                                         "avgPrice": {"type": "float"},
                                         "awards": {"properties": {"date": {"type": "keyword"},
                                                                   "name": {"type": "keyword"},
                                                                   "org": {"type": "keyword"}}},
                                         "badRatingRatio": {"type": "float"},
                                         "badTagDist": {"properties": {"id": {"type": "keyword"},
                                                                       "key": {"type": "keyword"},
                                                                       "value": {"type": "float"}}},
                                         "barcode": {"type": "keyword"},
                                         "bookingInfo": {"type": "text"},
                                         "brandId": {"type": "keyword"},
                                         "brandName": {"type": "text"},
                                         "brandNameU": {"type": "keyword"},
                                         "btcPrice": {"type": "float"},
                                         "buyMaxCount": {"type": "integer"},
                                         "catId1": {"type": "keyword"},
                                         "catId10": {"type": "keyword"},
                                         "catId2": {"type": "keyword"},
                                         "catId3": {"type": "keyword"},
                                         "catId4": {"type": "keyword"},
                                         "catId5": {"type": "keyword"},
                                         "catId6": {"type": "keyword"},
                                         "catId7": {"type": "keyword"},
                                         "catId8": {"type": "keyword"},
                                         "catId9": {"type": "keyword"},
                                         "catName1": {"type": "keyword"},
                                         "catName10": {"type": "keyword"},
                                         "catName2": {"type": "keyword"},
                                         "catName3": {"type": "keyword"},
                                         "catName4": {"type": "keyword"},
                                         "catName5": {"type": "keyword"},
                                         "catName6": {"type": "keyword"},
                                         "catName7": {"type": "keyword"},
                                         "catName8": {"type": "keyword"},
                                         "catName9": {"type": "keyword"},
                                         "catPathKey": {"type": "keyword"},
                                         "cityRank": {"type": "integer"},
                                         "commentCount": {"type": "integer"},
                                        "sentimentDist": {
                                                                "properties": {
                                                                    "confidence": {
                                                                        "type": "float"
                                                                    },
                                                                    "negative": {
                                                                        "type": "float"
                                                                    },
                                                                    "positive": {
                                                                        "type": "float"
                                                                    }
                                                                }
                                                            },
                                         "comments": {"properties": {"appendComments": {"properties": {"content": {"type": "text"},
                                                                                                       "date": {"type": "keyword"},
                                                                                                       "imageUrls": {"type": "keyword"}}},
                                                                     "avatarUrl": {"type": "keyword"},
                                                                     "commentCount": {"type": "integer"},
                                                                     "commenterGradeName": {"type": "keyword"},
                                                                     "commenterId": {"type": "keyword"},
                                                                     "commenterIdGrade": {"type": "keyword"},
                                                                     "commenterScreenName": {"type": "keyword"},
                                                                     "commenterType": {"type": "keyword"},
                                                                     "content": {"type": "text"},
                                                                     "dislikeCount": {"type": "integer"},
                                                                     "id": {"type": "keyword"},
                                                                     "idGrade": {"type": "keyword"},
                                                                     "imageUrls": {"type": "keyword"},
                                                                     "likeCount": {"type": "integer"},
                                                                     "publishDate": {"type": "integer"},
                                                                     "publishDateStr": {"type": "keyword"},
                                                                     "rating": {"type": "float"},
                                                                     "referId": {"type": "keyword"},
                                                                     "replies": {"properties": {"content": {"type": "text"},
                                                                                                "id": {"type": "keyword"},
                                                                                                "likeCount": {"type": "integer"},
                                                                                                "publishDate": {"type": "integer"},
                                                                                                "publishDateStr": {"type": "keyword"},
                                                                                                "referId": {"type": "keyword"},
                                                                                                "replierId": {"type": "keyword"},
                                                                                                "replierScreenName": {"type": "keyword"}}},
                                                                     "sentimentDist": {"properties": {"confidence": {"type": "float"},
                                                                                                      "negative": {"type": "float"},
                                                                                                      "positive": {"type": "float"}}},
                                                                     "source": {"type": "keyword"},
                                                                     "subobjects": {"properties": {"id": {"type": "keyword"},
                                                                                                   "name": {"type": "keyword"},
                                                                                                   "url": {"type": "keyword"}}},
                                                                     "tags": {"fields": {"text": {"type": "text"}},
                                                                              "type": "keyword"},
                                                                     "url": {"type": "keyword"}}},
                                         "competitors": {"properties": {"id": {"type": "keyword"},
                                                                        "name": {"type": "keyword"}}},
                                         "content": {"type": "text"},
                                         "createDate": {"type": "integer"},
                                         "dataType": {"type": "keyword"},
                                         "deliverInfo": {"type": "text"},
                                         "deliverTime": {"type": "float"},
                                         "descRating": {"type": "float"},
                                         "description": {"type": "text"},
                                         "detailImageUrls": {"type": "keyword"},
                                         "discount": {"type": "float"},
                                         "discountInfo": {"type": "text"},
                                         "discountPrice": {"type": "float"},
                                         "dislikeCount": {"type": "integer"},
                                         "favoriteCount": {"type": "integer"},
                                         "flavors": {"type": "keyword"},
                                         "geoPoint": {"type": "geo_point"},
                                         "goodRatingRatio": {"type": "float"},
                                         "goodTagDist": {"properties": {"id": {"type": "keyword"},
                                                                        "key": {"type": "keyword"},
                                                                        "value": {"type": "float"}}},
                                         "grapeTypes": {"type": "keyword"},
                                         "highPrice": {"type": "float"},
                                         "hotScore": {"type": "float"},
                                         "id": {"type": "keyword"},
                                         "imageBase64s": {"type": "keyword"},
                                         "imageUrls": {"type": "keyword"},
                                         "isOTC": {"type": "boolean"},
                                         "keyValues": {"properties": {"key": {"type": "keyword"},
                                                                      "value": {"type": "text"}}},
                                         "lastPrice": {"type": "float"},
                                         "level": {"type": "keyword"},
                                         "libId": {"type": "keyword"},
                                         "libSim": {"type": "float"},
                                         "likeCount": {"type": "integer"},
                                         "logiRating": {"type": "float"},
                                         "lowPrice": {"type": "float"},
                                         "mallType": {"type": "integer"},
                                         "marketPrice": {"type": "float"},
                                         "maxPrice": {"type": "float"},
                                         "minPrice": {"type": "float"},
                                         "monthSaleCount": {"type": "integer"},
                                         "openDate": {"type": "keyword"},
                                         "openingHours": {"type": "keyword"},
                                         "originPlace": {"type": "keyword"},
                                         "packFee": {"type": "float"},
                                         "packInfo": {"type": "text"},
                                         "payInfo": {"type": "text"},
                                         "popularity": {"type": "integer"},
                                         "presales": {"properties": {"beginDate": {"type": "keyword"},
                                                                     "count": {"type": "integer"},
                                                                     "deposit": {"type": "float"},
                                                                     "endDate": {"type": "keyword"},
                                                                     "info": {"type": "text"},
                                                                     "price": {"type": "float"}}},
                                         "price": {"type": "float"},
                                         "priceInfo": {"type": "text"},
                                         "priceOptions": {"properties": {"price": {"type": "float"},
                                                                         "type": {"type": "keyword"}}},
                                         "priceUnit": {"type": "keyword"},
                                         "producerId": {"type": "keyword"},
                                         "producerName": {"type": "keyword"},
                                         "promotions": {"type": "text"},
                                         "publishDate": {"type": "integer"},
                                         "publishDateStr": {"type": "keyword"},
                                         "qrcodeUrl": {"type": "keyword"},
                                         "qualRating": {"type": "float"},
                                         "queryWord": {"type": "keyword"},
                                         "rank": {"type": "integer"},
                                         "rating": {"type": "float"},
                                         "ratingCount": {"type": "integer"},
                                         "ratingDist": {"properties": {"key": {"type": "keyword"},
                                                                       "value": {"type": "float"}}},
                                         "readCount": {"type": "integer"},
                                         "reciptInfo": {"type": "text"},
                                         "recommendations": {"properties": {"coverUrl": {"type": "keyword"},
                                                                            "id": {"type": "keyword"},
                                                                            "price": {"type": "float"},
                                                                            "title": {"type": "text"},
                                                                            "url": {"type": "keyword"}}},
                                         "relevantProducts": {"properties": {"coverUrl": {"type": "keyword"},
                                                                             "id": {"type": "keyword"},
                                                                             "price": {"type": "float"},
                                                                             "title": {"type": "text"},
                                                                             "url": {"type": "keyword"}}},
                                         "saleCount": {"type": "integer"},
                                         "saleRank": {"type": "integer"},
                                         "saleStatus": {"type": "keyword"},
                                         "selfSale": {"type": "boolean"},
                                         "sellerId": {"type": "keyword"},
                                         "sellerScreenName": {"type": "keyword"},
                                         "servRating": {"type": "float"},
                                         "shareCount": {"type": "integer"},
                                         "shopUrl": {"type": "keyword"},
                                         "shortTitle": {"type": "keyword"},
                                         "skuAmount": {"type": "keyword"},
                                         "skuOptions": {"properties": {"id": {"type": "keyword"},
                                                                       "imageUrls": {"type": "keyword"},
                                                                       "keyValues": {"properties": {"key": {"type": "keyword"},
                                                                                                    "value": {"type": "text"}}},
                                                                       "marketPrice": {"type": "float"},
                                                                       "name": {"type": "keyword"},
                                                                       "packFee": {"type": "float"},
                                                                       "price": {"type": "float"},
                                                                       "rating": {"type": "float"},
                                                                       "saleCount": {"type": "integer"},
                                                                       "services": {"properties": {"id": {"type": "keyword"},
                                                                                                   "marketPrice": {"type": "float"},
                                                                                                   "name": {"type": "keyword"},
                                                                                                   "price": {"type": "float"},
                                                                                                   "saleCount": {"type": "integer"},
                                                                                                   "skuOptions": {"properties": {"id": {"type": "keyword"},
                                                                                                                                 "name": {"type": "keyword"},
                                                                                                                                 "price": {"type": "float"}}}}},
                                                                       "soldout": {"type": "boolean"},
                                                                       "stockSize": {"type": "integer"},
                                                                       "year": {"type": "keyword"}}},
                                         "skuUnit": {"type": "keyword"},
                                         "soldout": {"type": "boolean"},
                                         "sortId": {"type": "integer"},
                                         "spuId": {"type": "keyword"},
                                         "stockSize": {"type": "integer"},
                                         "storehouse": {"type": "keyword"},
                                         "subtitle": {"type": "text"},
                                         "tags": {"fields": {"text": {"type": "text"}},
                                                  "type": "keyword"},
                                         "taxInfo": {"type": "text"},
                                         "tieInSale": {"properties": {"buyMaxCount": {"type": "integer"},
                                                                      "discount": {"type": "float"},
                                                                      "id": {"type": "keyword"},
                                                                      "price": {"type": "float"},
                                                                      "saleCount": {"type": "integer"}}},
                                         "title": {"type": "text"},
                                         "toLib": {"type": "boolean"},
                                         "unitPrice": {"type": "float"},
                                         "updateDate": {"type": "integer"},
                                         "url": {"type": "keyword"},
                                         "userId": {"type": "keyword"},
                                         "userScreenName": {"type": "keyword"},
                                         "videoUrls": {"type": "keyword"},
                                         "viewCount": {"type": "long"},
                                         "warranties": {"type": "keyword"},
                                         "wholesalePrices": {"properties": {"count": {"type": "integer"},
                                                                            "id": {"type": "keyword"},
                                                                            "price": {"type": "float"}}},
                                         "wineType": {"type": "keyword"}}}},
        "settings": {
                "number_of_shards": '1',
                "number_of_replicas": '1'
            }
}

mapping_poi ={
    "mappings":{
        "hotel":{
            "dynamic":"strict",
            "_all":{
                "enabled":False
            },
            "properties":{
                "appCode":{
                    "type":"keyword"
                },
                "dataType":{
                    "type":"keyword"
                },
                "sortId":{
                    "type":"integer"
                },
                "navigation":{
                    "type":"keyword"
                },
                "id":{
                    "type":"keyword"
                },
                "sentimentDist": {
                    "properties": {
                        "confidence": {
                            "type": "float"
                        },
                        "negative": {
                            "type": "float"
                        },
                        "positive": {
                            "type": "float"
                        }
                    }
                },
                "idVerified":{
                    "type":"boolean"
                },
                "idVerifiedInfo":{
                    "type":"keyword"
                },
                "catPathKey":{
                    "type":"keyword"
                },
                "catId1":{
                    "type":"keyword"
                },
                "catId2":{
                    "type":"keyword"
                },
                "catId3":{
                    "type":"keyword"
                },
                "catId4":{
                    "type":"keyword"
                },
                "catId5":{
                    "type":"keyword"
                },
                "catName1":{
                    "type":"keyword"
                },
                "catName2":{
                    "type":"keyword"
                },
                "catName3":{
                    "type":"keyword"
                },
                "catName4":{
                    "type":"keyword"
                },
                "catName5":{
                    "type":"keyword"
                },
                "telephones":{
                    "type":"keyword"
                },
                "mobilePhones":{
                    "type":"keyword"
                },
                "emails":{
                    "type":"keyword"
                },
                "fansCount":{
                    "type":"integer"
                },
                "postCount":{
                    "type":"integer"
                },
                "commentCount":{
                    "type":"integer"
                },
                "honorCommentCount":{
                    "type":"integer"
                },
                "favoriteCount":{
                    "type":"integer"
                },
                "likeCount":{
                    "type":"integer"
                },
                "dislikeCount":{
                    "type":"integer"
                },
                "shareCount":{
                    "type":"integer"
                },
                "viewCount":{
                    "type":"long"
                },
                "readCount":{
                    "type":"integer"
                },
                "hearCount":{
                    "type":"integer"
                },
                "playCount":{
                    "type":"integer"
                },
                "visitCount":{
                    "type":"integer"
                },
                "bookingCount":{
                    "type":"integer"
                },
                "branchStoreCount":{
                    "type":"integer"
                },
                "historyCouponCount":{
                    "type":"integer"
                },
                "staffCount":{
                    "type":"integer"
                },
                "caseCount":{
                    "type":"integer"
                },
                "popularity":{
                    "type":"integer"
                },
                "retryCount":{
                    "type":"integer"
                },
                "createDate":{
                    "type":"integer"
                },
                "publishDate":{
                    "type":"integer"
                },
                "publishDateStr":{
                    "type":"keyword"
                },
                "updateDate":{
                    "type":"integer"
                },
                "openDate":{
                    "type":"keyword"
                },
                "openingHours":{
                    "type":"keyword"
                },
                "decorationDate":{
                    "type":"keyword"
                },
                "lastCommentDate":{
                    "type":"keyword"
                },
                "country":{
                    "type":"keyword"
                },
                "state":{
                    "type":"keyword"
                },
                "regionId":{
                    "type":"keyword"
                },
                "cityId":{
                    "type":"keyword"
                },
                "countryId":{
                    "type":"keyword"
                },
                "stateId":{
                    "type":"keyword"
                },
                "cityPinyin":{
                    "type":"keyword"
                },
                "city":{
                    "type":"keyword"
                },
                "district":{
                    "type":"keyword"
                },
                "location":{
                    "type":"keyword",
                    "fields":{
                        "text":{
                            "type":"text"
                        }
                    }
                },
                "address":{
                    "type":"keyword",
                    "fields":{
                        "text":{
                            "type":"text"
                        }
                    }
                },
                "geoPoint":{
                    "type":"geo_point"
                },
                "busInfo":{
                    "type":"text"
                },
                "metroInfo":{
                    "type":"text"
                },
                "driveInfo":{
                    "type":"text"
                },
                "parkingInfo":{
                    "type":"text"
                },
                "price":{
                    "type":"float"
                },
                "priceInfo":{
                    "type":"text"
                },
                "unitPrice":{
                    "type":"float"
                },
                "appPrice":{
                    "type":"float"
                },
                "marketPrice":{
                    "type":"float"
                },
                "lowPrice":{
                    "type":"float"
                },
                "highPrice":{
                    "type":"float"
                },
                "avgPrice":{
                    "type":"float"
                },
                "minPrice":{
                    "type":"float"
                },
                "maxPrice":{
                    "type":"float"
                },
                "discount":{
                    "type":"float"
                },
                "discountInfo":{
                    "type":"text"
                },
                "rank":{
                    "type":"integer"
                },
                "cityRank":{
                    "type":"integer"
                },
                "rating":{
                    "type":"float"
                },
                "ratingCount":{
                    "type":"integer"
                },
                "ratingRank":{
                    "type":"float"
                },
                "ratingDist":{
                    "properties":{
                        "key":{
                            "type":"keyword"
                        },
                        "value":{
                            "type":"float"
                        }
                    }
                },
                "qualRating":{
                    "type":"float"
                },
                "servRating":{
                    "type":"float"
                },
                "descRating":{
                    "type":"float"
                },
                "enviRating":{
                    "type":"float"
                },
                "priceRating":{
                    "type":"float"
                },
                "infraRating":{
                    "type":"float"
                },
                "goodRatingRatio":{
                    "type":"float"
                },
                "saleCount":{
                    "type":"integer"
                },
                "monthSaleCount":{
                    "type":"integer"
                },
                "saleRank":{
                    "type":"integer"
                },
                "soldout":{
                    "type":"boolean"
                },
                "stockSize":{
                    "type":"integer"
                },
                "bookingInfo":{
                    "type":"text"
                },
                "promotions":{
                    "type":"text"
                },
                "reciptInfo":{
                    "type":"text"
                },
                "ticketInfo":{
                    "type":"text"
                },
                "depositInfo":{
                    "type":"text"
                },
                "payInfo":{
                    "type":"text"
                },
                "taxInfo":{
                    "type":"text"
                },
                "salePhones":{
                    "type":"keyword"
                },
                "salePermit":{
                    "type":"keyword"
                },
                "saleStatus":{
                    "type":"keyword"
                },
                "title":{
                    "type":"text"
                },
                "titleAliases":{
                    "type":"text"
                },
                "subtitle":{
                    "type":"text"
                },
                "branchTitle":{
                    "type":"text"
                },
                "description":{
                    "type":"text"
                },
                "abstract":{
                    "type":"text"
                },
                "content":{
                    "type":"text"
                },
                "tags":{
                    "type":"keyword",
                    "fields":{
                        "text":{
                            "type":"text"
                        }
                    }
                },
                "tagDist":{
                    "properties":{
                        "id":{
                            "type":"keyword"
                        },
                        "key":{
                            "type":"keyword"
                        },
                        "value":{
                            "type":"float"
                        }
                    }
                },
                "goodTagDist":{
                    "properties":{
                        "id":{
                            "type":"keyword"
                        },
                        "key":{
                            "type":"keyword"
                        },
                        "value":{
                            "type":"float"
                        }
                    }
                },
                "badTagDist":{
                    "properties":{
                        "id":{
                            "type":"keyword"
                        },
                        "key":{
                            "type":"keyword"
                        },
                        "value":{
                            "type":"float"
                        }
                    }
                },
                "keyValues":{
                    "properties":{
                        "key":{
                            "type":"keyword"
                        },
                        "value":{
                            "type":"text"
                        }
                    }
                },
                "url":{
                    "type":"keyword"
                },
                "imageUrls":{
                    "type":"keyword"
                },
                "addressWords":{
                    "type":"keyword"
                },
                "titleWords":{
                    "type":"keyword"
                },
                "videoUrls":{
                    "type":"keyword"
                },
                "homeUrls":{
                    "type":"keyword"
                },
                "businessStatus":{
                    "type":"keyword"
                },
                "star":{
                    "type":"integer"
                },
                "level":{
                    "type":"keyword"
                },
                "menus":{
                    "type":"text"
                },
                "tipInfo":{
                    "type":"text"
                },
                "bulletin":{
                    "type":"text"
                },
                "environment":{
                    "type":"text"
                },
                "peculiarity":{
                    "type":"text"
                },
                "bestVisitTime":{
                    "type":"text"
                },
                "bestVisitDuration":{
                    "type":"text"
                },
                "hasWifi":{
                    "type":"boolean"
                },
                "hasBooking":{
                    "type":"boolean"
                },
                "hasMembership":{
                    "type":"boolean"
                },
                "isChainStore":{
                    "type":"boolean"
                },
                "isShutdown":{
                    "type":"boolean"
                },
                "brandName":{
                    "type":"text"
                },
                "brandNameU":{
                    "type":"keyword"
                },
                "cityU":{
                    "type":"keyword"
                },
                "ratingU":{
                    "type":"float"
                },
                "unifiedId":{
                    "type":"integer"
                },
                "brandId":{
                    "type":"keyword"
                },
                "floorCount":{
                    "type":"integer"
                },
                "roomCount":{
                    "type":"integer"
                },
                "infrastructures":{
                    "type":"keyword"
                },
                "facilities":{
                    "type":"keyword"
                },
                "services":{
                    "properties":{
                        "id":{
                            "type":"keyword"
                        },
                        "name":{
                            "type":"keyword"
                        },
                        "price":{
                            "type":"float"
                        },
                        "marketPrice":{
                            "type":"float"
                        },
                        "saleCount":{
                            "type":"integer"
                        }
                    }
                },
                "businessDistrict":{
                    "type":"keyword"
                },
                "assistServices":{
                    "type":"keyword"
                },
                "nearbySights":{
                    "type":"keyword"
                },
                "nearbyRestaurants":{
                    "type":"keyword"
                },
                "nearbyEntertainments":{
                    "type":"keyword"
                },
                "nearbyHotels":{
                    "properties":{
                        "id":{
                            "type":"keyword"
                        },
                        "distance":{
                            "type":"keyword"
                        },
                        "title":{
                            "type":"text"
                        },
                        "url":{
                            "type":"keyword"
                        },
                        "price":{
                            "type":"float"
                        },
                        "rating":{
                            "type":"float"
                        },
                        "level":{
                            "type":"keyword"
                        },
                        "star":{
                            "type":"integer"
                        },
                        "district":{
                            "type":"keyword"
                        },
                        "location":{
                            "type":"keyword",
                            "fields":{
                                "text":{
                                    "type":"text"
                                }
                            }
                        },
                        "commentCount":{
                            "type":"integer"
                        },
                        "geoPoint":{
                            "type":"geo_point"
                        },
                        "tags":{
                            "type":"keyword",
                            "fields":{
                                "text":{
                                    "type":"text"
                                }
                            }
                        }
                    }
                },
                "languages":{
                    "type":"keyword"
                },
                "pros":{
                    "type":"text"
                },
                "cons":{
                    "type":"text"
                },
                "flatOptions":{
                    "properties":{
                        "id":{
                            "type":"keyword"
                        },
                        "sortId":{
                            "type":"integer"
                        },
                        "referId":{
                            "type":"keyword"
                        },
                        "checkinDate":{
                            "type":"keyword"
                        },
                        "url":{
                            "type":"keyword"
                        },
                        "description":{
                            "type":"text"
                        },
                        "imageUrls":{
                            "type":"keyword"
                        },
                        "buildingName":{
                            "type":"keyword"
                        },
                        "buildingType":{
                            "type":"keyword"
                        },
                        "hasLift":{
                            "type":"boolean"
                        },
                        "staircaseInfo":{
                            "type":"text"
                        },
                        "decoration":{
                            "type":"keyword"
                        },
                        "floor":{
                            "type":"keyword"
                        },
                        "floorCount":{
                            "type":"integer"
                        },
                        "unitName":{
                            "type":"keyword"
                        },
                        "roomName":{
                            "type":"keyword"
                        },
                        "type":{
                            "type":"keyword"
                        },
                        "bedroom":{
                            "type":"integer"
                        },
                        "livingroom":{
                            "type":"integer"
                        },
                        "toilet":{
                            "type":"integer"
                        },
                        "kitchen":{
                            "type":"integer"
                        },
                        "balcony":{
                            "type":"integer"
                        },
                        "orientation":{
                            "type":"keyword"
                        },
                        "facilities":{
                            "type":"keyword"
                        },
                        "builtupArea":{
                            "type":"float"
                        },
                        "usableArea":{
                            "type":"float"
                        },
                        "usableAreaRatio":{
                            "type":"float"
                        },
                        "price":{
                            "type":"float"
                        },
                        "saleStatus":{
                            "type":"keyword"
                        },
                        "saleCount":{
                            "type":"integer"
                        },
                        "stockSize":{
                            "type":"integer"
                        },
                        "sight":{
                            "type":"keyword"
                        },
                        "bedTypes":{
                            "type":"keyword"
                        },
                        "cancelInfo":{
                            "type":"text"
                        },
                        "capacityInfo":{
                            "type":"text"
                        },
                        "extraBedInfo":{
                            "type":"text"
                        },
                        "assistServices":{
                            "type":"keyword"
                        },
                        "cashBack":{
                            "type":"integer"
                        },
                        "marketPrice":{
                            "type":"float"
                        },
                        "taxValue":{
                            "type":"float"
                        },
                        "promotions":{
                            "type":"text"
                        },
                        "isLowestPrice":{
                            "type":"boolean"
                        },
                        "keyValues":{
                            "properties":{
                                "key":{
                                    "type":"keyword"
                                },
                                "value":{
                                    "type":"text"
                                }
                            }
                        },
                        "commentCount":{
                            "type":"integer"
                        },
                        "tags":{
                            "type":"keyword",
                            "fields":{
                                "text":{
                                    "type":"text"
                                }
                            }
                        },
                        "priceUnit":{
                            "type":"keyword"
                        },
                        "baseId":{
                            "type":"keyword"
                        },
                        "hasBreakfast":{
                            "type":"boolean"
                        },
                        "sellerScreenName":{
                            "type":"keyword"
                        },
                        "priceOptions":{
                            "properties":{
                                "type":{
                                    "type":"keyword"
                                },
                                "price":{
                                    "type":"float"
                                },
                                "hasBreakfast":{
                                    "type":"boolean"
                                }
                            }
                        },
                        "memberTerms":{
                            "properties":{
                                "type":{
                                    "type":"keyword"
                                },
                                "price":{
                                    "type":"float"
                                }
                            }
                        }
                    }
                },
                "comments":{
                    "properties":{
                        "referId":{
                            "type":"keyword"
                        },
                        "subobjects":{
                            "properties":{
                                "id":{
                                    "type":"keyword"
                                },
                                "name":{
                                    "type":"keyword"
                                },
                                "url":{
                                    "type":"keyword"
                                }
                            }
                        },
                        "id":{
                            "type":"keyword"
                        },
                        "commenterId":{
                            "type":"keyword"
                        },
                        "commenterScreenName":{
                            "type":"keyword"
                        },
                        "idGrade":{
                            "type":"keyword"
                        },
                        "commenterIdGrade":{
                            "type":"keyword"
                        },
                        "commenterGradeName":{
                            "type":"keyword"
                        },
                        "honourComment":{
                            "type":"boolean"
                        },
                        "avatarUrl":{
                            "type":"keyword"
                        },
                        "publishDateStr":{
                            "type":"keyword"
                        },
                        "publishDate":{
                            "type":"integer"
                        },
                        "checkinDate":{
                            "type":"keyword"
                        },
                        "checkoutDate":{
                            "type":"keyword"
                        },
                        "title":{
                            "type":"text"
                        },
                        "content":{
                            "type":"text"
                        },
                        "topicWords": {

                            "properties": {

                                "word": {

                                    "type": "keyword"

                                },

                                "score": {

                                    "type": "float"

                                }

                            }

                        },
                        "sentimentDist":{
                            "properties":{
                                "negative":{
                                    "type":"float"
                                },
                                "positive":{
                                    "type":"float"
                                },
                                "confidence":{
                                    "type":"float"
                                }
                            }
                        },
                        "rating":{
                            "type":"float"
                        },
                        "likeCount":{
                            "type":"integer"
                        },
                        "dislikeCount":{
                            "type":"integer"
                        },
                        "viewCount":{
                            "type":"long"
                        },
                        "commentCount":{
                            "type":"integer"
                        },
                        "commenterType":{
                            "type":"keyword"
                        },
                        "url":{
                            "type":"keyword"
                        },
                        "imageUrls":{
                            "type":"keyword"
                        },
                        "source":{
                            "type":"keyword"
                        },
                        "tags":{
                            "type":"keyword",
                            "fields":{
                                "text":{
                                    "type":"text"
                                }
                            }
                        },
                        "replies":{
                            "properties":{
                                "id":{
                                    "type":"keyword"
                                },
                                "content":{
                                    "type":"text"
                                },
                                "likeCount":{
                                    "type":"integer"
                                },
                                "publishDateStr":{
                                    "type":"keyword"
                                },
                                "publishDate":{
                                    "type":"integer"
                                },
                                "replierId":{
                                    "type":"keyword"
                                },
                                "replierScreenName":{
                                    "type":"keyword"
                                },
                                "referId":{
                                    "type":"keyword"
                                }
                            }
                        }
                    }
                }
            }
        }
    },
    "settings":{
        "number_of_shards":"1",
        "number_of_replicas":"1"
    }
}

mapping_log = {
"settings": {
                "number_of_shards": '1',
                "number_of_replicas": '1'
            },
    "mappings":{
        "log":{
            "dynamic":"strict",
            "_all":{
                "enabled":False
            },
            "properties":{
                "count":{
                    "type":"integer"
                },
                "createDate":{
                    "type":"integer"
                },
                "id":{
                    "type":"keyword"
                },
                "ids":{
                    "type":"keyword"
                },
                "interface":{
                    "type":"keyword"
                },
                "lastPublishDate":{
                    "type":"keyword"
                },
                "pushDate":{
                    "type":"integer"
                }
            }
        }
    }
}


# 创建索引，索引的名字是my-index,如果已经存在了，就返回个400，
# 这个索引可以现在创建，也可以在后面插入数据的时候再临时创建
es.indices.create(index='article201907', body=mapping_post, ignore=[400, 404])
# es.indices.create(index='product05', body=mapping_product, ignore=[400, 404])
# es.indices.create(index='data_push_log_20190416', body=mapping_log, ignore=[400, 404])
# es.indices.create(index='poi_unified_hotel_v7_07m29d', body=mapping_poi)
# es.indices.delete(index='product05', ignore=[400, 404])



# curl -XGET "http://10.29.71.97:9900/product/post/_delete_by_query" -H 'Content-Type: application/json' -d'
# {
#   "query":{
#     "match_all":{}
#   }
# }'




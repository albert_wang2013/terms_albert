# -*- coding: utf-8 -*-
import json
import time
import aiohttp
from tornado import escape
import asyncio
import logging
from idataapi_transform import ProcessFactory, GetterConfig
from idataapi_transform.DataProcess.Config.ConfigUtil import WriterConfig
from decimal import Decimal

logging.basicConfig(level=logging.INFO)


class es_mongo_es():
    def __init__(self):
        t = 'in'
        if t == 'out':
            self.es_read = ["120.76.205.241:8000"]
            self.es_writer = ['10.29.71.97:9900']
        else:
            self.es_read = ['10.29.71.97:9900']
            self.es_writer = ['10.29.71.97:9900']

    async def read_es(self):
        """
        :return: 读取es中数据，返回结果
        """
        query_body = {"size": "30",
                    }
        # headers = {"Host": "newes", "apikey": "42aadd74e8ef0d26676a2cff74ce6923", "Content-Type": "application/json"}
        headers = {"Content-Type": "application/json"}
        collect_ = 'poi_unified_hotel_v7_07m29d'
        es_config = GetterConfig.RESConfig(collect_, 'hotel', hosts=self.es_read,
                                           headers=headers, query_body=query_body, scroll='50m')
        es_getter = ProcessFactory.create_getter(es_config)  # 10.26.222.219
        async for items in es_getter:  # 每次去特定size返回
            datas = []
            for item in items:
                if item:
                    item = await self.deal_comment(item)
                    # print(json.dumps(item))
                    if item:
                        datas.append(item)
            if datas:
                await self.data_es(datas)

    async def deal_comment(self, item):
        if item['comments']:
            await self.textraction(item['comments'])
            return item
        else:
            return item

    async def textraction(self, comments):
        async with aiohttp.ClientSession(read_timeout=60*10) as session:
            tasks = []
            for comment in comments:
                task = asyncio.ensure_future(self.start_request(session, comment))
                tasks.append(task)
            await asyncio.gather(*tasks, return_exceptions=False)

    async def start_request(self, session, comment):
        if 'topicWords' not in comment:
            # await asyncio.sleep(0.5)
            # url = 'http://api01.bitspaceman.com:8000/nlp/textraction/bitspaceman?apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
            # url = 'http://10.26.222.219:8000/nlp/textraction/bitspaceman?apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
            url = 'http://10.29.250.189:8889/nlp/textraction/bitspaceman'
            if comment['content']:
                body = {
                    'type':1,
                    'index':'article_cluster',
                    'text':comment['content'][:900],
                }
                tries = 0
                response = None
                while tries < 2:
                    async with asyncio.Semaphore(2), session.post(url=url, data=body) as response:
                        if response.status == 200:
                            response = await response.text()
                            break
                        else:
                            tries += 1
                    logging.info('post_request tries:{} and response.status:{},url:{}'.format(tries, response.status, url))
                if response:
                    # print(response)
                    response = json.loads(response)
                    comment['topicWords'] = response['wordList'] if response['wordList'] else None
                else:
                    comment['topicWords'] = None
            else:
                comment['topicWords'] = None

    async def data_es(self, datas):
        try:
            logging.info('writer es_datas to es length:{}'.format(len(datas)))
            # es_config = WriterConfig.WESConfig("poi_unified_hotel_v7_07m24d", "hotel", hosts=["127.0.0.1:9200"])
            es_config = WriterConfig.WESConfig("poi_unified_hotel_v7_07m29d", "hotel", hosts=self.es_writer, )
            es_writer = ProcessFactory.create_writer(es_config)
            await es_writer.write(datas)
        except Exception as e:
            print('error:{}'.format(e))


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(es_mongo_es().read_es())
    print('run_end')
    time.sleep(24*3600)



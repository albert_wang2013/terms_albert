# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2018/11/22 16:55
@Author  : Albert
@Func    : 
@File    : post_deal.py
@Software: PyCharm
"""
import time
from datetime import datetime, timedelta
import json
import asyncio
from idataapi_transform import ProcessFactory, GetterConfig, WriterConfig
from idataapi_transform.DataProcess.Config.ConnectorConfig import session_manger
import logging



""" 无该条件，就无法输出到控制台 """
logging.basicConfig(level=logging.INFO, filename='../logs/post_deal_log.log', filemode='a+')

class PostDeal(object):
    def __init__(self):
        self.session = session_manger.get_session()
        self.num = 0
        self.sum_num = 0


    def comment_deal(self, doc):
        return doc


    def post_deal(self, doc):
        if 'originalAuthor' in doc:
            if 'trade' in doc['originalAuthor']:
                del doc['originalAuthor']['trade']
        if 'author' in doc:
            del doc['author']['trade']
        return doc


    def weibo_ids(self):
        """
        func: 将有效的id，比如出现84次，进行判断，进行 mongo 数据筛选
        :return:
        """
        weibo_set = set()
        with open(file='../ids_data/mid.txt', mode='r') as f:
            lines = f.readlines()
            for line in lines:
                id = line.replace("\n", "").strip()
                weibo_set.add(id)
        return weibo_set

    async def user_deal(self, mongos):
        weibo_set = self.weibo_ids()
        for mongo_ in mongos:
            """ 统计每一个collection的data数据 """
            logging.info('pre mongo_collection deal num: %s' % (self.num,))
            self.num = 0
            mongo_config = GetterConfig.RMongoConfig(mongo_, per_limit=50000)
            mongo_getter = ProcessFactory.create_getter(mongo_config)
            async for items in mongo_getter:
                for item in items:
                    """ 处理item方法 """
                    item = self.item_deal(item, weibo_set, mongo_)
                    if item:
                        self.num += 1
                        self.sum_num += self.num
                        self.data_storage(item, mongo_)
        """ 记录所有collections的data数据 """
        logging.info('all_mongo_collection deal sum_num: %s ' % (self.sum_num,))
        
        
    def data_storage(self, item, mongo_):
        filename = mongo_ + '_' + item['requestId']
        if item['requestId'] == '20181023009':
            with open('../weibo_post_data/{}.txt'.format(filename), 'a+') as f1:
                f1.write(json.dumps(item, ensure_ascii=False) + '\n')
                f1.flush()
        if item['requestId'] == '20181023008':
            with open('../weibo_post_data/{}.txt'.format(filename), 'a+') as f2:
                f2.write(json.dumps(item, ensure_ascii=False) + '\n')
                f2.flush()
        if item['requestId'] == '20181023007':
            with open('../weibo_post_data/{}.txt'.format(filename), 'a+') as f3:
                f3.write(json.dumps(item, ensure_ascii=False) + '\n')
                f3.flush()
        if item['requestId'] == '20181023005':
            with open('../weibo_post_data/{}.txt'.format(filename), 'a+') as f4:
                f4.write(json.dumps(item, ensure_ascii=False) + '\n')
                f4.flush()
        if item['requestId'] == '20181023006':
            with open('../weibo_post_data/{}.txt'.format(filename), 'a+') as f5:
                f5.write(json.dumps(item, ensure_ascii=False) + '\n')
                f5.flush()


    def item_deal(self, item, weibo_set, mongo_):
        item.pop('_id')
        """ 判断当前item 的数据是否属于要保留的数据 """
        if 'weiboId' in item:
            if item['weiboId'] in weibo_set:
                item = item
        elif 'id' in item:
            if item['id'] in weibo_set:
                item = item
        """ 数据清洗 """
        if 'post' in mongo_:
            item = self.post_deal(item)
        elif 'comment' in mongo_:
            item = self.comment_deal(item)
        return item


    def mongo_col(self, mongo_list):
        """
        :param mongo_list: 需要处理的mongo列表
        :return: 返回 mongos 列表，可以直接用于处理
        """
        mongos = []
        for mongo_ in mongo_list:
            for data_ in range(20181113, 20181121):
                mongo_i = mongo_ + str(data_)
                mongos.append(mongo_i)
        return mongos



if __name__ == "__main__":
    logging.info('run start: %s' % (datetime.now().replace(microsecond=0),))
    mongo_list = ['post', 'comment_hot', 'comment_time']
    mongos = PostDeal().mongo_col(mongo_list)
    task_ = PostDeal().user_deal(mongos)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(task_)
    logging.info('its stop: %s' % (datetime.now().replace(microsecond=0),))
    time.sleep(100000)
# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2018/11/22 15:53
@Author  : Albert
@Func    : 
@File    : user_deal.py
@Software: PyCharm
"""
import time
from datetime import datetime, timedelta
import json
import asyncio
from idataapi_transform import ProcessFactory, GetterConfig, WriterConfig
from idataapi_transform.DataProcess.Config.ConnectorConfig import session_manger
import logging


""" 无该条件，就无法输出到控制台 """
logging.basicConfig(level=logging.INFO, filename='../logs/user_deal_log.log', filemode='a+')

class UserDeal(object):
    def __init__(self):
        self.session = session_manger.get_session()
        self.num = 0
        self.sum_num = 0


    def profile_deal(self, doc):
        return doc


    def relation_deal(self, doc):
        del doc['objectScreenName']
        del doc['fansCount']
        del doc['followCount']
        del doc['id']
        return doc


    def post_deal(self, doc):
        if 'originalAuthor' in doc:
            if 'trade' in doc['originalAuthor']:
                del doc['originalAuthor']['trade']
        if 'author' in doc:
            del doc['author']['trade']
        return doc


    async def user_deal(self, mongo_list):
        for mongo_ in mongo_list:
            """ 统计每一个collection的data数据 """
            logging.info('pre mongo_collection deal num: %s' % (self.num,))
            self.num = 0
            mongo_config = GetterConfig.RMongoConfig(mongo_, per_limit=50000)
            mongo_getter = ProcessFactory.create_getter(mongo_config)
            async for items in mongo_getter:
                for item in items:
                    item.pop('_id')
                    if 'post' in mongo_:
                        item = self.post_deal(item)
                    elif 'relation' in mongo_:
                        item = self.relation_deal(item)
                    elif 'profile' in mongo_:
                        item = self.profile_deal(item)
                    if item:
                        self.num += 1
                        self.sum_num += self.num
                        with open('../weibo_user_data/{}.txt'.format(mongo_), 'a+') as f:
                            f.write(json.dumps(item, ensure_ascii=False) + '\n')
                            f.flush()
        """ 记录所有collections的data数据 """
        logging.info('all_mongo_collection deal sum_num: %s ' % (self.sum_num,))



if __name__ == "__main__":
    logging.info('run start: %s' % (datetime.now().replace(microsecond=0),))
    mongo_list = ['user', 'relation', 'post', ]
    loop = asyncio.get_event_loop()
    task_ = UserDeal().user_deal(mongo_list)
    loop.run_until_complete(task_)
    logging.info('its stop: %s' % (datetime.now().replace(microsecond=0),))
    time.sleep(100000)

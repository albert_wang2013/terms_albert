# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2018/11/22 11:30
@Author  : Albert
@Func    : 
@File    : parse_weibo_func.py
@Software: PyCharm
"""
import logging
import re
from datetime import datetime, timedelta
import time
from dateutil import parser



""" 无该条件，就无法输出到控制台 """
logging.basicConfig(level=logging.INFO)

def detail_parse(item, request_id, collect_seq, type_):
    sub1 = dict()
    sub1['id'] = str(item['id'])
    sub1['name'] = item['screenName']
    sub1['userAvatar'] = item['avatarUrl']
    sub1['description'] = item['biography']
    sub1['followCount'] = item['followCount']
    sub1['friendCount'] = item['friendCount']
    sub1['fansCount'] = item['fansCount']
    sub1['postCount'] = item['postCount']
    sub1['idVerified'] = item['idVerified']
    sub1['idVerifiedType'] = item['idVerifiedType']
    sub1['idVerifiedInfo'] = item['favorMotto']
    sub1['idGrade'] = item['idGrade']
    sub1['gender'] = item['gender']
    sub1['birthday'] = item['birthday']
    sub1['tags'] = item['tags']
    sub1['trade'] = item['industries']
    """ deal educations field """
    if item['educations']:
        if item['educations'] == None:
            item['educations'] = None
        else:
            educations = item['educations']
            educations.append(item['educations'][0]['schoolName'])
            del educations[0]
    sub1['educations'] = item['educations']
    sub1['location'] = item['location']
    """ deal works field """
    if item['works']:
        if item['works'] == None:
            work = []
            work_ = {
                'position': None,
                'location': None,
                'employer': None
            }
            work.append(work_)
        else:
            work = item['works'][0]
            if 'position' not in work:
                work['position'] = None
            if 'location' not in work:
                work['location'] = None
    sub1['works'] = item['works']
    sub1['registerDate'] = item['registerDate']
    sub1['collectSeq'] = str(collect_seq)  # 采集序号
    sub1['collectTime'] = int(time.time())  # 采集时间
    sub1['requestId'] = str(request_id)  # 采集ID
    sub1["type"] = type_
    return sub1


def parse_follower(item, request_id, collect_seq, type_):  # 解析粉丝
    """
        id == 2803301701 的粉丝
        {
        "type": "follower",
        "createDate": null,
        "fansCount": 1,
        "objectScreenName": "千哥不会飞拈脑",
        "publishDate": null,
        "publishDateStr": null,
        "followCount": 0,
        "subjectId": "2803301701",
        "objectId": "5714686989"
        }
    """
    sub1 = dict()
    sub1['id'] = str(item['objectId'])  # 拿到的是关注者的信息        # id字段我先保留  比较重要  需要做个区分
    sub1['userId'] = str(item['objectId'])  # 关注者id
    sub1['followUserId'] = str(item['subjectId'])  # 被关注者的id
    sub1['objectScreenName'] = item['objectScreenName']  # 关注者id的信息
    sub1['fansCount'] = item['fansCount']
    sub1['followCount'] = item['followCount']
    sub1['collectSeq'] = str(collect_seq)
    sub1['collectTime'] = int(time.time())
    sub1['requestId'] = str(request_id)
    sub1["type"] = type_
    return sub1


def parse_leader(item, request_id, collect_seq, type_):  # 解析关注者
    """
        id ==  2803301701  的关注
            {
        "followCount": 83,
        "objectId": "6307317302",
        "createDate": null,
        "subjectId": "2803301701",
        "objectScreenName": "暖视频",
        "type": "leader",
        "fansCount": 3386,
        "publishDate": null,
        "publishDateStr": null
            }
    """
    sub1 = dict()
    sub1['id'] = str(item['objectId'])  # 拿到的是被关注者的信息
    sub1['userId'] = str(item['subjectId'])  # 关注者id
    sub1['followUserId'] = str(item['objectId'])  # 被关注者的id
    sub1['objectScreenName'] = item['objectScreenName']  # 被关注者id的信息
    sub1['fansCount'] = item['fansCount']
    sub1['followCount'] = item['followCount']
    sub1['collectSeq'] = str(collect_seq)
    sub1['collectTime'] = int(time.time())
    sub1['requestId'] = str(request_id)
    sub1["type"] = type_
    return sub1


def parse_user_post(item, request_id, collect_seq, type_):
    sub1 = dict()
    sub1['id'] = str(item['id'])
    source = item['mblog']['source']
    """ <a href=\"http://app.weibo.com/t/feed/6NndUT\" rel=\"nofollow\">Weibo.intl</a> """
    source = re.search('.*?nofollow.*?>(.*?)</a>.*?|.*', source).group(1)
    sub1['source'] = source
    sub1['content'] = item['content']
    if 'retweeted_status' in item['mblog']:  # 判断是否有转发内容
        sub1['originalContent'] = item['mblog']['retweeted_status']['text']
    else:
        sub1['originalContent'] = None
    sub1['likeCount'] = item['likeCount']
    sub1['commentCount'] = item['commentCount']
    if 'topic_struct' in item['mblog']:  # 判断是否有topicTitles主题
        topicTitles = []
        for i in item['mblog']['topic_struct']:
            topic_title = i['topic_title']
            topicTitles.append(topic_title)
    else:
        topicTitles = None
    sub1['topicTitles'] = topicTitles
    author = dict()
    author['id'] = str(item['from']['id'])
    author['name'] = item['from']['name']
    author['userAvatar'] = item['from']['extend']['avatar_large']
    author['description'] = item['from']['description']
    author['followCount'] = item['from']['friendCount']
    author['friendCount'] = item['from']['friendCount']
    author['fansCount'] = item['from']['fansCount']
    author['postCount'] = item['from']['postCount']
    author['idVerified'] = item['from']['extend']['verified']
    verified_type = item['from']['extend']['verified_type']
    author['idVerifiedType'] = verified_type
    author['idVerifiedInfo'] = item['from']['extend']['verified_reason']
    author['idGrade'] = item['from']['extend']['urank']
    gender = item['from']['extend']['gender']
    if gender == 'm':
        gender = '男'
    else:
        gender = '女'
    author['gender'] = gender
    author['birthday'] = None
    author['tags'] = None
    author['trade'] = None
    author['educations'] = None
    author['location'] = item['from']['extend']['location']
    author['works'] = None
    author['registerDate'] = None
    """ author 字段判断  """
    sub1['author'] = author
    """ 判断发布位置 """
    sub1['publishDate'] = item['publishDate']
    if 'annotations' in item['mblog']:
        if 'place' in item['mblog']['annotations'][0]:
            publishAddress = item['mblog']['annotations'][0]['place']['title']
            sub1['publishAddress'] = publishAddress
        else:
            publishAddress = None
            sub1['publishAddress'] = publishAddress
    else:
        publishAddress = None
        sub1['publishAddress'] = publishAddress
    sub1['url'] = item['url']
    sub1['imageUrls'] = item['imageURLs']
    sub1['videoUrls'] = item['videoURLs']
    sub1['shareCount'] = item['shareCount']
    """ originalAuthor字段映射 """
    originalAuthor = {}
    if 'retweeted_status' in item['mblog']:  # 判断是否有转发内容
        if 'user' in item['mblog']['retweeted_status']:
            user = item['mblog']['retweeted_status']['user']
            originalAuthor['id'] = str(user['id'])
            originalAuthor['name'] = user['screen_name']
            originalAuthor['userAvatar'] = user['profile_image_url']
            originalAuthor['description'] = user['description']
            originalAuthor['followCount'] = user['followers_count']
            originalAuthor['friendCount'] = user['friends_count']
            originalAuthor['fansCount'] = user['friends_count']
            originalAuthor['postCount'] = user['statuses_count']
            originalAuthor['idVerified'] = user['verified']
            verified_type = user['verified_type']
            originalAuthor['idVerifiedType'] = verified_type
            originalAuthor['idVerifiedInfo'] = user['verified_reason']
            originalAuthor['idGrade'] = user['urank']
            gender = user['gender']
            if gender == 'm':
                gender = '男'
            else:
                gender = '女'
            originalAuthor['gender'] = gender
            originalAuthor['birthday'] = None
            originalAuthor['tags'] = None
            originalAuthor['trade'] = None
            originalAuthor['educations'] = None
            originalAuthor['works'] = None
            originalAuthor['registerDate'] = None
    sub1['originalAuthor'] = originalAuthor
    sub1['collectSeq'] = str(collect_seq)
    sub1['collectTime'] = int(time.time())
    sub1['requestId'] = str(request_id)
    sub1["type"] = type_
    return sub1


def parse_post(item, request_id, collect_seq, type_):
    sub1 = dict()
    sub1['id'] = str(item['id'])
    """ deal source filed"""
    if 'mblog' in item:
        source = item['mblog']['source']
        """<a href=\"http://app.weibo.com/t/feed/6NndUT\" rel=\"nofollow\">Weibo.intl</a>"""
        source = re.search('.*?nofollow.*?>(.*?)</a>.*?|.*', source)
        if source:
            source = source.group(1)
        else:
            source = None
    else:
        source = None
    sub1['source'] = source
    sub1['content'] = item['content']
    """ 判断是否有topicTitles主题 """
    if 'topic_struct' in item['mblog']:
        topicTitles = []
        for i in item['mblog']['topic_struct']:
            topic_title = i['topic_title']
            topicTitles.append(topic_title)
    else:
        topicTitles = None
    sub1['topicTitles'] = topicTitles
    """ 判断是否有转发内容 """
    if 'mblog' in item:
        if 'retweeted_status' in item['mblog']:
            sub1['originalContent'] = item['mblog']['retweeted_status']['text']
        else:
            sub1['originalContent'] = None
    sub1['likeCount'] = item['likeCount']
    sub1['commentCount'] = item['commentCount']
    sub1['shareCount'] = item['shareCount']
    """ author字段 """
    author = {}
    author['id'] = str(item['from']['id'])
    author['name'] = item['from']['name']
    author['userAvatar'] = item['from']['extend']['avatar_large']
    author['description'] = item['from']['description']
    author['followCount'] = item['from']['friendCount']
    author['friendCount'] = item['from']['friendCount']
    author['fansCount'] = item['from']['fansCount']
    author['postCount'] = item['from']['postCount']
    author['idVerified'] = item['from']['extend']['verified']
    verified_type = item['from']['extend']['verified_type']
    author['idVerifiedType'] = verified_type
    author['idVerifiedInfo'] = item['from']['extend']['verified_reason']
    author['idGrade'] = item['from']['extend']['urank']
    gender = item['from']['extend']['gender']
    if gender == 'm':
        gender = '男'
    else:
        gender = '女'
    author['gender'] = gender
    author['birthday'] = None
    author['tags'] = None
    author['trade'] = None
    author['educations'] = None
    author['location'] = item['from']['extend']['location']
    author['works'] = None
    author['registerDate'] = None
    sub1['author'] = author
    """ 判断发布位置 """
    sub1['publishDate'] = item['publishDate']
    if 'annotations' in item['mblog']:
        if item['mblog']['annotations']:
            if 'place' in item['mblog']['annotations'][0]:
                publishAddress = item['mblog']['annotations'][0]['place']['title']
                sub1['publishAddress'] = publishAddress
            else:
                sub1['publishAddress'] = None
    else:
        publishAddress = None
        sub1['publishAddress'] = publishAddress
    sub1['url'] = item['url']
    sub1['imageUrls'] = item['imageURLs']
    sub1['videoUrls'] = item['videoURLs']
    """ originalAuthor字段映射 """
    originalAuthor = {}
    if 'retweeted_status' in item['mblog']:  # 判断是否有转发内容
        if 'user' in item['mblog']['retweeted_status']:
            user = item['mblog']['retweeted_status']['user']
            originalAuthor['id'] = user['id']
            originalAuthor['name'] = user['screen_name']
            originalAuthor['userAvatar'] = user['profile_image_url']
            originalAuthor['description'] = user['description']
            originalAuthor['followCount'] = user['followers_count']
            originalAuthor['friendCount'] = user['friends_count']
            originalAuthor['fansCount'] = user['friends_count']
            originalAuthor['postCount'] = user['statuses_count']
            originalAuthor['idVerified'] = user['verified']
            verified_type = user['verified_type']
            originalAuthor['idVerifiedType'] = verified_type
            originalAuthor['idVerifiedInfo'] = user['verified_reason']
            originalAuthor['idGrade'] = user['urank']
            gender = user['gender']
            if gender == 'm':
                gender = '男'
            else:
                gender = '女'
            originalAuthor['gender'] = gender
            originalAuthor['birthday'] = None
            originalAuthor['tags'] = None
            originalAuthor['trade'] = None
            originalAuthor['educations'] = None
            originalAuthor['works'] = None
            originalAuthor['registerDate'] = None
    sub1['originalAuthor'] = originalAuthor
    sub1['collectSeq'] = str(collect_seq)
    sub1['collectTime'] = int(time.time())
    sub1['requestId'] = str(request_id)
    sub1["type1"] = type_
    """ 时间判断 """
    time_str = datetime.now().replace(microsecond=0)  # 2018-11-21 18:19:16
    datetimes = int(time.mktime(time.strptime(str(time_str), '%Y-%m-%d %H:%M:%S')))
    if sub1['publishDate'] >= int(datetimes - 3600 * 2):  # 小于当前2小的数据入库
        return sub1


def parse_comment_time(item, request_id, collect_seq, type_):
    sub1 = {}
    sub1['id'] = str(item['id'])  # 评论id，楼层id
    sub1['type'] = 1
    sub1['content'] = item['content']
    sub1['weiboId'] = str(item['referId'])
    sub1['referId'] = None  # 回复评论时，被回复评论Id
    sub1['commenterId'] = str(item['commenterId'])  # 评论者id
    sub1['commenterAvatar'] = None  # 评论者头像
    sub1['commenterName'] = item['commenterScreenName']  # 评论者名字
    if 'commentCount' in item:
        sub1['commentCount'] = item['commentCount']  # 评论数
    else:
        sub1['commentCount'] = 0  # 评论数
    sub1['likeCount'] = item['likeCount']  # 点赞数
    sub1['publishDate'] = item['publishDate']
    if 'imageUrls' in item:
        sub1['imageUrls'] = item['imageUrls']
    else:
        sub1['imageUrls'] = None
    sub1['collectSeq'] = str(collect_seq)
    sub1['collectTime'] = int(time.time())  # 采集时间
    sub1['requestId'] = str(request_id)
    sub1["type1"] = type_
    return sub1


def parse_comment_hot(item, request_id, collect_seq, type_):
    sub1 = {}
    sub1['id'] = str(item['id'])  # 评论id，楼层id
    sub1['type'] = 1
    sub1['content'] = item['content']
    sub1['weiboId'] = str(item['referId'])
    sub1['referId'] = None  # 回复评论时，被回复评论Id
    sub1['commenterId'] = str(item['commenterId'])  # 评论者id
    sub1['commenterAvatar'] = None  # 评论者头像
    sub1['commenterName'] = item['commenterScreenName']  # 评论者名字
    if 'commentCount' in item:
        sub1['commentCount'] = item['commentCount']  # 评论数
    else:
        sub1['commentCount'] = None     # 评论数
    sub1['likeCount'] = item['likeCount']  # 点赞数
    sub1['publishDate'] = item['publishDate']
    if 'imageUrls' in item:
        sub1['imageUrls'] = item['imageUrls']
    else:
        sub1['imageUrls'] = None
    sub1['collectSeq'] = str(collect_seq)
    sub1['collectTime'] = int(time.time())
    sub1['requestId'] = str(request_id)
    sub1["type1"] = type_
    return sub1


def parse_comment_share(item, request_id, collect_seq, type_):
    sub1 = {}
    sub1['id'] = str(item['sharerPostId'])  # 评论id，楼层id
    sub1['type'] = 3
    sub1['content'] = item['content']
    sub1['weiboId'] = str(item['referId'])
    sub1['referId'] = None  # 回复评论时，被回复评论Id
    sub1['commenterId'] = str(item['sharerId'])  # 评论者id
    sub1['commenterAvatar'] = None  # 评论者头像
    sub1['commenterName'] = item['sharerScreenName']  # 评论者名字
    sub1['commentCount'] = None  # 评论数
    sub1['likeCount'] = item['likeCount']  # 点赞数
    sub1['publishDate'] = item['publishDate']
    sub1['imageUrls'] = None
    sub1['collectSeq'] = str(collect_seq)
    sub1['collectTime'] = int(time.time())
    sub1['requestId'] = str(request_id)
    sub1["type1"] = type_
    return sub1


def parse_comment_reply(item, request_id, collect_seq, type_):
    sub1 = {}
    sub1['id'] = str(item['id'])  # 评论id，评论层id
    sub1['type'] = 2
    sub1['content'] = item['content']
    sub1['weiboId'] = str(item['rootId'])
    sub1['referId'] = str(item['referId'])  # 回复评论时，被回复评论Id
    sub1['commenterId'] = str(item['replierId'])  # 评论者id
    sub1['commenterAvatar'] = None  # 评论者头像
    sub1['commenterName'] = item['replierScreenName']  # 评论者名字
    sub1['commentCount'] = item['commentCount']  # 评论数
    sub1['likeCount'] = item['likeCount']  # 点赞数
    sub1['publishDate'] = item['publishDate']
    sub1['imageUrls'] = None
    sub1['collectSeq'] = str(collect_seq)
    sub1['collectTime'] = int(time.time())
    sub1['requestId'] = request_id
    sub1["type1"] = type_
    return sub1


def parse_post_status(item, request_id, collect_seq, type_):
    sub = item
    """ 判断是否有删除的文章 """
    if 'user' in sub:
        sub1 = {}
        sub1['id'] = str(sub['id'])
        """ 对source进行处理 
            <a href=\"http://app.weibo.com/t/feed/6NndUT\" rel=\"nofollow\">Weibo.intl</a>
        """
        source = re.search('.*?nofollow.*?>(.*?)</a>.*?|.*', sub['source'])
        if source:
            sub1['source'] = source.group(1)
        else:
            sub1['source'] = None
        if sub['isLongText']:
            sub1['content'] = sub['longText']['longTextContent']
        else:
            sub1['content'] = sub['text']
        """ 判断是否有转发内容 """
        if 'retweeted_status' in sub:
            if 'isLongText' in sub['retweeted_status']:
                if sub['retweeted_status']['isLongText']:
                    sub1['originalContent'] = sub['retweeted_status']['longText']['longTextContent']
                else:
                    sub1['originalContent'] = sub['retweeted_status']['text']
            else:
                sub1['originalContent'] = None
        else:
            sub1['originalContent'] = None
        sub1['likeCount'] = sub['attitudes_count']
        sub1['commentCount'] = sub['comments_count']
        sub1['shareCount'] = sub['reposts_count']
        """ 判断是否有topicTitles主题 """
        if 'topic_struct' in sub:
            topicTitles = []
            for i in sub['topic_struct']:
                topic_title = i['topic_title']
                topicTitles.append(topic_title)
        else:
            topicTitles = None
        sub1['topicTitles'] = topicTitles
        """ author字段 """
        author = {}
        author['id'] = str(sub['user']['id'])
        author['name'] = sub['user']['screen_name']
        author['userAvatar'] = sub['user']['avatar_large']
        author['description'] = sub['user']['description']
        author['folowCount'] = sub['user']['friends_count']
        author['friendCount'] = sub['user']['friends_count']
        author['fansCount'] = sub['user']['followers_count']
        author['postCount'] = sub['user']['statuses_count']
        author['idVerified'] = sub['user']['verified']
        verified_type = sub['user']['verified_type']
        author['idVerifiedType'] = verified_type
        author['idVerifiedInfo'] = sub['user']['verified_reason']
        author['idGrade'] = sub['user']['urank']
        gender = sub['user']['gender']
        if gender == 'm':
            gender = '男'
        else:
            gender = '女'
        author['gender'] = gender
        author['birthday'] = None
        author['tags'] = None
        author['trade'] = None
        author['educations'] = None
        author['location'] = sub['user']['location']
        author['works'] = None
        author['registerDate'] = None
        sub1['author'] = author
        """ 转换发布时间 """
        datetime = parser.parse(sub['created_at'])
        time_tuple = (
            datetime.year, datetime.month, datetime.day, datetime.hour, datetime.minute, datetime.second, 0, 0,
            0)
        sub1['publishDate'] = int(time.mktime(time_tuple) - 3600 * 8)
        if 'annotations' in sub:
            if sub['annotations']:
                if 'place' in sub['annotations'][0]:
                    publishAddress = sub['annotations'][0]['place']['title']
                    sub1['publishAddress'] = publishAddress
                else:
                    sub1['publishAddress'] = None
        else:
            publishAddress = None
            sub1['publishAddress'] = publishAddress
        sub1['url'] = 'https://m.weibo.cn/status/{}'.format(sub1['id'])
        if 'pic_urls' in sub:
            sub1['imageUrls'] = sub['pic_urls']
        sub1['videoUrls'] = None
        """ originalAuthor字段映射 """
        originalAuthor = {}
        if 'retweeted_status' in sub:
            if 'user' in sub['retweeted_status']:
                user = sub['retweeted_status']['user']
                originalAuthor['id'] = str(user['id'])
                originalAuthor['name'] = user['screen_name']
                originalAuthor['userAvatar'] = user['profile_image_url']
                originalAuthor['description'] = user['description']
                originalAuthor['folowCount'] = user['followers_count']
                originalAuthor['friendCount'] = user['friends_count']
                originalAuthor['fansCount'] = user['friends_count']
                originalAuthor['postCount'] = user['statuses_count']
                originalAuthor['idVerified'] = user['verified']
                verified_type = user['verified_type']
                originalAuthor['idVerifiedType'] = verified_type
                originalAuthor['idVerifiedInfo'] = user['verified_reason']
                originalAuthor['idGrade'] = user['urank']
                gender = user['gender']
                if gender == 'm':
                    gender = '男'
                else:
                    gender = '女'
                originalAuthor['gender'] = gender
                originalAuthor['birthday'] = None
                originalAuthor['tags'] = None
                originalAuthor['trade'] = None
                originalAuthor['educations'] = None
                originalAuthor['works'] = None
                originalAuthor['registerDate'] = None
        sub1['originalAuthor'] = originalAuthor
        sub1['collectSeq'] = str(collect_seq)
        sub1['collectTime'] = int(time.time())
        sub1['requestId'] = str(request_id)
        sub1["type1"] = type_
        return sub1
    else:
        logging.info('mid is not ok: {}'.format(str(item)))

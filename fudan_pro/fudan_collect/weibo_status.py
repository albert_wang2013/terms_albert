import re
import json
import time
# import dateutil.parser
import redis
from dateutil import parser
import asyncio
import hashlib
import logging
from urllib.parse import quote_plus, urlencode
from idataapi_transform import ProcessFactory, GetterConfig, WriterConfig
from idataapi_transform.DataProcess.Config.ConnectorConfig import session_manger
from datetime import datetime, timedelta



""" 无该条件，就无法输出到控制台 """
logging.basicConfig(level=logging.INFO)


class FunctionCreator(object):
    def __init__(self, weibo_crawler):
        super().__init__()
        self.weibo_crawler = weibo_crawler
        self.weibo_id_set = set()
        self.apikey_ = 'fAV2GSoZ4OY0IoIyRZQdnB4bLcB2fyDe4DwIRkX1pAQuKbu1qcaNHPuOMV5J70N1'

    def filter_creator(self, request_id, collect_seq, type_):

        def parse_post(item):
            sub = item
            """ 判断是否有删除的文章 """
            if 'user' in sub:
                sub1 = {}
                sub1['id'] = str(sub['id'])
                """ 对source进行处理 
                    <a href=\"http://app.weibo.com/t/feed/6NndUT\" rel=\"nofollow\">Weibo.intl</a>
                """
                source = re.search('.*?nofollow.*?>(.*?)</a>.*?|.*', sub['source'])
                if source:
                    sub1['source'] = source.group(1)
                else:
                    sub1['source'] = None
                if sub['isLongText']:
                    sub1['content'] = sub['longText']['longTextContent']
                else:
                    sub1['content'] = sub['text']
                """ 判断是否有转发内容 """
                if 'retweeted_status' in sub:
                    if 'isLongText' in sub['retweeted_status']:
                        if sub['retweeted_status']['isLongText']:
                            sub1['originalContent'] = sub['retweeted_status']['longText']['longTextContent']
                        else:
                            sub1['originalContent'] = sub['retweeted_status']['text']
                    else:
                        sub1['originalContent'] = None
                else:
                    sub1['originalContent'] = None
                sub1['likeCount'] = sub['attitudes_count']
                sub1['commentCount'] = sub['comments_count']
                sub1['shareCount'] = sub['reposts_count']
                """ 判断是否有topicTitles主题 """
                if 'topic_struct' in sub:
                    topicTitles = []
                    for i in sub['topic_struct']:
                        topic_title = i['topic_title']
                        topicTitles.append(topic_title)
                else:
                    topicTitles = None
                sub1['topicTitles'] = topicTitles
                """ author字段 """
                author = {}
                author['id'] = str(sub['user']['id'])
                author['name'] = sub['user']['screen_name']
                author['userAvatar'] = sub['user']['avatar_large']
                author['description'] = sub['user']['description']
                author['folowCount'] = sub['user']['friends_count']
                author['friendCount'] = sub['user']['friends_count']
                author['fansCount'] = sub['user']['followers_count']
                author['postCount'] = sub['user']['statuses_count']
                author['idVerified'] = sub['user']['verified']
                verified_type = sub['user']['verified_type']
                author['idVerifiedType'] = verified_type
                author['idVerifiedInfo'] = sub['user']['verified_reason']
                author['idGrade'] = sub['user']['urank']
                gender = sub['user']['gender']
                if gender == 'm':
                    gender = '男'
                else:
                    gender = '女'
                author['gender'] = gender
                author['birthday'] = None
                author['tags'] = None
                author['trade'] = None
                author['educations'] = None
                author['location'] = sub['user']['location']
                author['works'] = None
                author['registerDate'] = None
                sub1['author'] = author
                """ 转换发布时间 """
                datetime = parser.parse(sub['created_at'])
                time_tuple = (
                datetime.year, datetime.month, datetime.day, datetime.hour, datetime.minute, datetime.second, 0, 0,
                0)
                sub1['publishDate'] = int(time.mktime(time_tuple) - 3600*8)
                if 'annotations' in sub:
                    if sub['annotations']:
                        if 'place' in sub['annotations'][0]:
                            publishAddress = sub['annotations'][0]['place']['title']
                            sub1['publishAddress'] = publishAddress
                        else:
                            sub1['publishAddress'] = None
                else:
                    publishAddress = None
                    sub1['publishAddress'] = publishAddress
                sub1['url'] = 'https://m.weibo.cn/status/{}'.format(sub1['id'])
                if 'pic_urls' in sub:
                    sub1['imageUrls'] = sub['pic_urls']
                sub1['videoUrls'] = None
                """ originalAuthor字段映射 """
                originalAuthor = {}
                if 'retweeted_status' in sub:
                    if 'user' in sub['retweeted_status']:
                        user = sub['retweeted_status']['user']
                        originalAuthor['id'] = str(user['id'])
                        originalAuthor['name'] = user['screen_name']
                        originalAuthor['userAvatar'] = user['profile_image_url']
                        originalAuthor['description'] = user['description']
                        originalAuthor['folowCount'] = user['followers_count']
                        originalAuthor['friendCount'] = user['friends_count']
                        originalAuthor['fansCount'] = user['friends_count']
                        originalAuthor['postCount'] = user['statuses_count']
                        originalAuthor['idVerified'] = user['verified']
                        verified_type = user['verified_type']
                        originalAuthor['idVerifiedType'] = verified_type
                        originalAuthor['idVerifiedInfo'] = user['verified_reason']
                        originalAuthor['idGrade'] = user['urank']
                        gender = user['gender']
                        if gender == 'm':
                            gender = '男'
                        else:
                            gender = '女'
                        originalAuthor['gender'] = gender
                        originalAuthor['birthday'] = None
                        originalAuthor['tags'] = None
                        originalAuthor['trade'] = None
                        originalAuthor['educations'] = None
                        originalAuthor['works'] = None
                        originalAuthor['registerDate'] = None
                sub1['originalAuthor'] = originalAuthor
                sub1['collectSeq'] = str(collect_seq)
                sub1['collectTime'] = int(time.time())
                sub1['requestId'] = str(request_id)
                sub1["type1"] = type_
                return sub1
            else:
                logging.info('mid is not ok: {}'.format(str(item)))

        def parse_comment_time(item):
            sub1 = {}
            sub1['id'] = str(item['id'])  # 评论id，楼层id
            sub1['type'] = 1
            sub1['content'] = item['content']
            sub1['weiboId'] = str(item['referId'])
            sub1['referId'] = None  # 回复评论时，被回复评论Id
            sub1['commenterId'] = str(item['commenterId'])  # 评论者id
            sub1['commenterAvatar'] = None  # 评论者头像
            sub1['commenterName'] = item['commenterScreenName']  # 评论者名字
            if 'commentCount' in item:
                sub1['commentCount'] = item['commentCount']  # 评论数
            else:
                sub1['commentCount'] = 0  # 评论数
            sub1['likeCount'] = item['likeCount']  # 点赞数
            sub1['publishDate'] = item['publishDate']
            if 'imageUrls' in item:
                sub1['imageUrls'] = item['imageUrls']
            else:
                sub1['imageUrls'] = None
            sub1['collectSeq'] = str(collect_seq)
            sub1['collectTime'] = int(time.time())  # 采集时间
            sub1['requestId'] = str(request_id)
            sub1["type1"] = type_
            """ 评论只存取两个小时类的数据进行更新入库 """
            if int(time.time()) - int(sub1['publishDate']) <= 7300:
                return sub1

        def parse_comment_hot(item):
            sub1 = {}
            sub1['id'] = str(item['id'])  # 评论id，楼层id
            sub1['type'] = 1
            sub1['content'] = item['content']
            sub1['weiboId'] = str(item['referId'])
            sub1['referId'] = None  # 回复评论时，被回复评论Id
            sub1['commenterId'] = str(item['commenterId'])  # 评论者id
            sub1['commenterAvatar'] = None  # 评论者头像
            sub1['commenterName'] = item['commenterScreenName']  # 评论者名字
            if 'commentCount' in item:
                sub1['commentCount'] = item['commentCount']  # 评论数
            else:
                sub1['commentCount'] = None  # 评论数
            sub1['likeCount'] = item['likeCount']  # 点赞数
            sub1['publishDate'] = item['publishDate']
            if 'imageUrls' in item:
                sub1['imageUrls'] = item['imageUrls']
            else:
                sub1['imageUrls'] = None
            sub1['collectSeq'] = str(collect_seq)
            sub1['collectTime'] = int(time.time())
            sub1['requestId'] = str(request_id)
            sub1["type1"] = type_
            return sub1

        def parse_comment_share(item):
            sub1 = {}
            sub1['id'] = str(item['sharerPostId'])  # 评论id，楼层id
            sub1['type'] = 3
            sub1['content'] = item['content']
            sub1['weiboId'] = str(item['referId'])
            sub1['referId'] = None  # 回复评论时，被回复评论Id
            sub1['commenterId'] = str(item['sharerId'])  # 评论者id
            sub1['commenterAvatar'] = None  # 评论者头像
            sub1['commenterName'] = item['sharerScreenName']  # 评论者名字
            sub1['commentCount'] = None  # 评论数
            sub1['likeCount'] = item['likeCount']  # 点赞数
            sub1['publishDate'] = item['publishDate']
            sub1['imageUrls'] = None
            sub1['collectSeq'] = str(collect_seq)
            sub1['collectTime'] = int(time.time())
            sub1['requestId'] = str(request_id)
            sub1["type1"] = type_
            """ 评论只存取两个小时类的数据进行更新入库 """
            if int(time.time()) - int(sub1['publishDate']) <= 7300:
                return sub1


        def parse_comment_reply(item):
            sub1 = {}
            sub1['id'] = str(item['id'])  # 评论id，评论层id
            sub1['type'] = 2
            sub1['content'] = item['content']
            sub1['weiboId'] = str(item['rootId'])
            sub1['referId'] = str(item['referId'])  # 回复评论时，被回复评论Id
            sub1['commenterId'] = str(item['replierId'])  # 评论者id
            sub1['commenterAvatar'] = None  # 评论者头像
            sub1['commenterName'] = item['replierScreenName']  # 评论者名字
            sub1['commentCount'] = item['commentCount']  # 评论数
            sub1['likeCount'] = item['likeCount']  # 点赞数
            sub1['publishDate'] = item['publishDate']
            sub1['imageUrls'] = None
            sub1['collectSeq'] = str(collect_seq)
            sub1['collectTime'] = int(time.time())
            sub1['requestId'] = request_id
            sub1["type1"] = type_
            if int(time.time()) - int(sub1['publishDate']) <= 7300:
                return sub1

        def done_if(items):
            if items:
                if int(time.time()) - int(items[-1]["publishDate"]) <= 7200:
                    return False
                return True
            return True

        async def fetch_comment(items):
            host, profile, relation, post, r, c = self.weibo_crawler.parameter_select()
            comment_time_ = host + "/comment/weibo?id={}&sort=time&type=1&apikey=" + self.apikey_
            comment_hot_ = host + "/comment/weibo?id={}&sort=hot&type=1&apikey=" + self.apikey_
            comment_share_ = host + "/share/weibo?id={}&apikey=" + self.apikey_
            ret_items = items
            urls = []
            for item in items:
                if item["commentCount"] > 0:
                    call_back = self.filter_creator(request_id, collect_seq, "10001")
                    urls.append(GetterConfig.RAPIConfig(comment_time_.format(item["id"]),
                        filter_=self.filter_creator(request_id, collect_seq, "3"), call_back=call_back, done_if=done_if))
                    urls.append(GetterConfig.RAPIConfig(comment_hot_.format(item["id"]),
                        filter_=self.filter_creator(request_id, collect_seq, "4"), max_limit=50, trim_to_max_limit=True))
                if item["shareCount"] > 0:
                    urls.append(GetterConfig.RAPIConfig(comment_share_.format(item["id"], done_if=done_if),
                        filter_=self.filter_creator(request_id, collect_seq, "5")))
            getter = ProcessFactory.create_getter(GetterConfig.RAPIBulkConfig(urls))
            async for items in getter:
                ret_items.extend(items)
            return ret_items


        async def fetch_reply(items):
            host, profile, relation, post, r, c = self.weibo_crawler.parameter_select()
            comment_reply_ = host + "/reply/weibo?id={}&apikey=" + self.apikey_
            ret_items = items
            urls = []
            for item in items:
                if item['commentCount']:
                    if item["commentCount"] > 0:
                        urls.append(GetterConfig.RAPIConfig(comment_reply_.format(item["id"]),
                            filter_=self.filter_creator(request_id, collect_seq, "6"), done_if=done_if))
            getter = ProcessFactory.create_getter(GetterConfig.RAPIBulkConfig(urls))
            async for items in getter:
                ret_items.extend(items)
            return ret_items


        if type_ in ('9'):
            return parse_post
        elif type_ == "3":
            return parse_comment_time
        elif type_ == '4':
            return parse_comment_hot
        elif type_ == '5':
            return parse_comment_share
        elif type_ == '6':
            return parse_comment_reply
        elif type_ == '10000':
            return fetch_comment
        elif type_ == '10001':
            return fetch_reply

    @staticmethod
    def id_hash_func(item):
        if "type1" in item:
            del item["type1"]
        return hashlib.md5((item["id"] + item['collectSeq'] + item['requestId']).encode("utf8")).hexdigest()

class WeiBoCrawler(object):
    def __init__(self):
        super().__init__()
        host, profile, relation, post, r, c = self.parameter_select()
        self.apikey = "fAV2GSoZ4OY0IoIyRZQdnB4bLcB2fyDe4DwIRkX1pAQuKbu1qcaNHPuOMV5J70N1"
        self.type_map = {
            "9": host + "post/weibo?id=%s&type=status&apikey=%s",
        }
        self.current_hour = 0
        self.function_creator = FunctionCreator(self)
        self.change_collection_and_curr_hour()

        self.weibo_post = list()
        self.weibo_comment_time_share_reply = list()
        self.weibo_comment_hot = list()
        self.id_set = set()

    """ 取mongo数据判断，根据时间拿取数据库 """
    def time_due(self):
        now = datetime.now()
        if (now.hour % 2) == 0:
            next_day = now.replace(microsecond=0, second=0, minute=0) + timedelta(hours=8)
            con, con1, con2 = str(next_day).split(':')
            con = con.replace(' ', '').replace('-', '')
            self.current_hour = next_day.hour
            # print(con)
            return con
        else:
            next_day = now.replace(microsecond=0, second=0, minute=0) - timedelta(hours=1) + timedelta(hours=8)
            con, con1, con2 = str(next_day).split(':')
            con = con.replace(' ', '').replace('-', '')
            self.current_hour = next_day.hour
            return con

    def if_need_change_collection(self):
        now = datetime.now()
        now = now.replace(microsecond=0, second=0, minute=0) + timedelta(hours=8)
        now = now.hour
        if now == 0 or now == 00:
            now = 24
        if now - self.current_hour >= 2:
            return True
        return False

    def change_collection_and_curr_hour(self):
        host, post, time_reply, hot, r, c = self.parameter_select()
        id_hash_func_ = self.function_creator.id_hash_func
        time_format = self.time_due()
        self.mongo_post = ProcessFactory.create_writer(
            WriterConfig.WMongoConfig('{}_{}'.format(post, time_format), id_hash_func=id_hash_func_))
        self.mongo_time = ProcessFactory.create_writer(
            WriterConfig.WMongoConfig('{}_{}'.format(time_reply, time_format), id_hash_func=id_hash_func_))
        self.mongo_hot = ProcessFactory.create_writer(
            WriterConfig.WMongoConfig('{}_{}'.format(hot, time_format), id_hash_func=id_hash_func_))

    async def url_generator(self):
        host, post, time_reply, hot, r, c = self.parameter_select()
        r_redis_config = GetterConfig.RRedisConfig(key=r['key'], host=r['host'], port=r['port'],
                                                   db=r['db'], password=r['password'])
        cli = await r_redis_config.get_redis_pool_cli()
        while True:
            task = await cli.lpop(r['key'], encoding="utf8")
            if task is None:
                await asyncio.sleep(10)
                if self.if_need_change_collection():
                    self.change_collection_and_curr_hour()
                return
            if self.if_need_change_collection():
                self.change_collection_and_curr_hour()
            type_, id_, key = task.split('|')
            request_id, collect_seq, rate = key.split('&')  # 请求的id号，采集序列，采集时间的限制
            request_id = re.search('.*?=(.*)', request_id).group(1)
            collect_seq = re.search('.*?=(.*)', collect_seq).group(1)
            id_ = re.search('mid=(.*)', id_).group(1)
            start_url = self.type_map[type_] % (id_, self.apikey)
            target_func = self.function_creator.filter_creator(request_id, collect_seq, type_)
            call_back = self.function_creator.filter_creator(request_id, collect_seq, "10000")
            if type_ == "9":
                logging.info(id_)
                yield GetterConfig.RAPIConfig(start_url, filter_=target_func, call_back=call_back, max_limit=10)
            else:
                logging.error("type_ != 9: id: %s" % (id_, ))

    async def start(self):
        host, post, time_reply, hot, r, c = self.parameter_select()
        url_generator = self.url_generator()
        getter = ProcessFactory.create_getter(GetterConfig.RAPIBulkConfig(url_generator, concurrency=c))
        async for items in getter:
            for item in items:
                if item["type1"] == "9":
                    self.weibo_post.append(item)
                elif item["type1"] in ("3", "5", "6"):
                    self.weibo_comment_time_share_reply.append(item)
                elif item["type1"] == "4":
                    self.weibo_comment_hot.append(item)
                else:
                    logging.error("unknown type1: %s" % (item["type1"], ))
            await self.perform_write()


    async def perform_write(self):
        self.id_set.update({item["id"] for item in self.weibo_post})
        tasks = [
            self.mongo_post.write(self.weibo_post),
            self.mongo_time.write(self.weibo_comment_time_share_reply),
            self.mongo_hot.write(self.weibo_comment_hot)
        ]
        await asyncio.gather(*tasks)
        self.weibo_post.clear()
        self.weibo_comment_time_share_reply.clear()
        self.weibo_comment_hot.clear()
        logging.info("after write len self.id_set: %d" % (len(self.id_set)))


    def parameter_select(self):
        select_number = 'server_test'
        select_redis = 'test'
        concurrency_ = 50
        """ redis Configuration"""
        redis_parameter = {}
        if select_redis == "formal":
            redis_parameter['key'] = 'fudan_task_queue2'
            redis_parameter['host'] = "120.25.255.56"
            redis_parameter['port'] = 8000
            redis_parameter['db'] = 3
            redis_parameter['password'] = '7ef615c6c32f427d713144f67e2ef14d248c3ccd4bfd' \
                                          '6ddf33a4b56878bbcb2f8c72ae185427d853e08de0aa4a8aa7b3a'
        else:
            redis_parameter['key'] = 'fudan_task_queue2'
            redis_parameter['host'] = "127.0.0.1"
            redis_parameter['port'] = 6379
            redis_parameter['db'] = 3
            redis_parameter['password'] = None
        """ Api/mongo Configuration"""
        if select_number != 'server_formal':
            if select_number == 'server_test':
                host = "http://10.26.222.219:8000/"
            else:
                host = "http://api01.bitspaceman.com:8000/"
            mongo_1 = 'weibo_post_%s_' % (select_number,)
            mongo_2 = 'weibo_comment_time_share_reply_%s_' % (select_number,)
            mongo_3 = 'weibo_comment_hot_%s_' % (select_number,)
            return host, mongo_1, mongo_2, mongo_3, redis_parameter, concurrency_
        elif select_number == 'server_formal':
            host = "http://10.26.222.219:8000/"
            mongo_1 = 'weibo_post'
            mongo_2 = 'weibo_comment_time_share_reply_'
            mongo_3 = 'weibo_comment_hot_'
            return host, mongo_1, mongo_2, mongo_3, redis_parameter, concurrency_


if __name__ == "__main__":
    while True:
        time.sleep(10)
        loop = asyncio.get_event_loop()
        w = WeiBoCrawler()
        loop.run_until_complete(w.start())

import re
import time
import asyncio
import hashlib
import logging
from urllib.parse import quote_plus
from idataapi_transform import ProcessFactory, GetterConfig, WriterConfig


""" 无该条件，就无法输出到控制台 """
logging.basicConfig(level=logging.INFO)


class FunctionCreator(object):
    def __init__(self, weibo_crawler):
        super().__init__()
        self.weibo_crawler = weibo_crawler


    def filter_creator(self, request_id, collect_seq, rate, type_):
        async def parse_profile(items):
            item = items[0]
            ret_items = list()
            start_url = self.weibo_crawler.host + 'profile/weibo?type=2&origin=1&id=%s&apikey=%s'\
                        % (item["id"], self.weibo_crawler.apikey)
            getter = ProcessFactory.create_getter(GetterConfig.RAPIConfig(start_url, filter_=detail_parse))
            async for items in getter:
                ret_items.extend(items)
            return ret_items


        def detail_parse(item):
            sub1 = dict()
            sub1['id'] = str(item['id'])
            sub1['name'] = item['screenName']
            sub1['userAvatar'] = item['avatarUrl']
            sub1['description'] = item['biography']
            sub1['followCount'] = item['followCount']
            sub1['friendCount'] = item['friendCount']
            sub1['fansCount'] = item['fansCount']
            sub1['postCount'] = item['postCount']
            sub1['idVerified'] = item['idVerified']
            sub1['idVerifiedType'] = item['idVerifiedType']
            sub1['idVerifiedInfo'] = item['favorMotto']
            sub1['idGrade'] = item['idGrade']
            sub1['gender'] = item['gender']
            sub1['birthday'] = item['birthday']
            sub1['tags'] = item['tags']
            sub1['trade'] = item['industries']
            """ deal educations field """
            if item['educations']:
                if item['educations'] == None:
                    item['educations'] = None
                else:
                    educations = item['educations']
                    educations.append(item['educations'][0]['schoolName'])
                    del educations[0]
            sub1['educations'] = item['educations']
            sub1['location'] = item['location']
            """ deal works field """
            if item['works']:
                if item['works'] == None:
                    work = []
                    work_ = {
                        'position': None,
                        'location': None,
                        'employer': None
                    }
                    work.append(work_)
                else:
                    work = item['works'][0]
                    if 'position' not in work:
                        work['position'] = None
                    if 'location' not in work:
                        work['location'] = None
            sub1['works'] = item['works']
            sub1['registerDate'] = item['registerDate']
            sub1['collectSeq'] = str(collect_seq)  # 采集序号
            sub1['collectTime'] = int(time.time())  # 采集时间
            sub1['requestId'] = str(request_id)  # 采集ID
            sub1["type"] = type_
            return sub1


        def parse_follower(item):   # 解析粉丝
            """
                id == 2803301701 的粉丝
                {
                "type": "follower",
                "createDate": null,
                "fansCount": 1,
                "objectScreenName": "千哥不会飞拈脑",
                "publishDate": null,
                "publishDateStr": null,
                "followCount": 0,
                "subjectId": "2803301701",
                "objectId": "5714686989"
                }
            """
            sub1 = dict()
            sub1['id'] = str(item['objectId'])   # 拿到的是关注者的信息        # id字段我先保留  比较重要  需要做个区分
            sub1['userId'] = str(item['objectId'])      # 关注者id
            sub1['followUserId'] = str(item['subjectId'])    # 被关注者的id
            sub1['objectScreenName'] = item['objectScreenName']     # 关注者id的信息
            sub1['fansCount'] = item['fansCount']
            sub1['followCount'] = item['followCount']
            sub1['collectSeq'] = str(collect_seq)
            sub1['collectTime'] = int(time.time())
            sub1['requestId'] = str(request_id)
            sub1["type"] = type_
            return sub1


        def parse_leader(item):     # 解析关注者
            """
                id ==  2803301701  的关注
                    {
                "followCount": 83,
                "objectId": "6307317302",
                "createDate": null,
                "subjectId": "2803301701",
                "objectScreenName": "暖视频",
                "type": "leader",
                "fansCount": 3386,
                "publishDate": null,
                "publishDateStr": null
                    }
            """
            sub1 = dict()
            sub1['id'] = str(item['objectId'])   # 拿到的是被关注者的信息
            sub1['userId'] = str(item['subjectId'])  # 关注者id
            sub1['followUserId'] = str(item['objectId'])  # 被关注者的id
            sub1['objectScreenName'] = item['objectScreenName']  # 被关注者id的信息
            sub1['fansCount'] = item['fansCount']
            sub1['followCount'] = item['followCount']
            sub1['collectSeq'] = str(collect_seq)
            sub1['collectTime'] = int(time.time())
            sub1['requestId'] = str(request_id)
            sub1["type"] = type_
            return sub1


        def parse_post(item):
            sub1 = dict()
            sub1['id'] = str(item['id'])
            source = item['mblog']['source']
            """ <a href=\"http://app.weibo.com/t/feed/6NndUT\" rel=\"nofollow\">Weibo.intl</a> """
            source = re.search('.*?nofollow.*?>(.*?)</a>.*?|.*', source).group(1)
            sub1['source'] = source
            sub1['content'] = item['content']
            if 'retweeted_status' in item['mblog']:  # 判断是否有转发内容
                sub1['originalContent'] = item['mblog']['retweeted_status']['text']
            else:
                sub1['originalContent'] = None
            sub1['likeCount'] = item['likeCount']
            sub1['commentCount'] = item['commentCount']
            if 'topic_struct' in item['mblog']:  # 判断是否有topicTitles主题
                topicTitles = []
                for i in item['mblog']['topic_struct']:
                    topic_title = i['topic_title']
                    topicTitles.append(topic_title)
            else:
                topicTitles = None
            sub1['topicTitles'] = topicTitles
            author = dict()
            author['id'] = str(item['from']['id'])
            author['name'] = item['from']['name']
            author['userAvatar'] = item['from']['extend']['avatar_large']
            author['description'] = item['from']['description']
            author['followCount'] = item['from']['friendCount']
            author['friendCount'] = item['from']['friendCount']
            author['fansCount'] = item['from']['fansCount']
            author['postCount'] = item['from']['postCount']
            author['idVerified'] = item['from']['extend']['verified']
            verified_type = item['from']['extend']['verified_type']
            author['idVerifiedType'] = verified_type
            author['idVerifiedInfo'] = item['from']['extend']['verified_reason']
            author['idGrade'] = item['from']['extend']['urank']
            gender = item['from']['extend']['gender']
            if gender == 'm':
                gender = '男'
            else:
                gender = '女'
            author['gender'] = gender
            author['birthday'] = None
            author['tags'] = None
            author['trade'] = None
            author['educations'] = None
            author['location'] = item['from']['extend']['location']
            author['works'] = None
            author['registerDate'] = None
            """ author 字段判断  """
            sub1['author'] = author
            """ 判断发布位置 """
            sub1['publishDate'] = item['publishDate']
            if 'annotations' in item['mblog']:
                if 'place' in item['mblog']['annotations'][0]:
                    publishAddress = item['mblog']['annotations'][0]['place']['title']
                    sub1['publishAddress'] = publishAddress
                else:
                    publishAddress = None
                    sub1['publishAddress'] = publishAddress
            else:
                publishAddress = None
                sub1['publishAddress'] = publishAddress
            sub1['url'] = item['url']
            sub1['imageUrls'] = item['imageURLs']
            sub1['videoUrls'] = item['videoURLs']
            sub1['shareCount'] = item['shareCount']
            """ originalAuthor字段映射 """
            originalAuthor = {}
            if 'retweeted_status' in item['mblog']:  # 判断是否有转发内容
                if 'user' in item['mblog']['retweeted_status']:
                    user = item['mblog']['retweeted_status']['user']
                    originalAuthor['id'] = str(user['id'])
                    originalAuthor['name'] = user['screen_name']
                    originalAuthor['userAvatar'] = user['profile_image_url']
                    originalAuthor['description'] = user['description']
                    originalAuthor['followCount'] = user['followers_count']
                    originalAuthor['friendCount'] = user['friends_count']
                    originalAuthor['fansCount'] = user['friends_count']
                    originalAuthor['postCount'] = user['statuses_count']
                    originalAuthor['idVerified'] = user['verified']
                    verified_type = user['verified_type']
                    originalAuthor['idVerifiedType'] = verified_type
                    originalAuthor['idVerifiedInfo'] = user['verified_reason']
                    originalAuthor['idGrade'] = user['urank']
                    gender = user['gender']
                    if gender == 'm':
                        gender = '男'
                    else:
                        gender = '女'
                    originalAuthor['gender'] = gender
                    originalAuthor['birthday'] = None
                    originalAuthor['tags'] = None
                    originalAuthor['trade'] = None
                    originalAuthor['educations'] = None
                    originalAuthor['works'] = None
                    originalAuthor['registerDate'] = None
            sub1['originalAuthor'] = originalAuthor
            sub1['collectSeq'] = str(collect_seq)
            sub1['collectTime'] = int(time.time())
            sub1['requestId'] = str(request_id)
            sub1["type"] = type_
            return sub1


        if type_ == "5":
            return parse_profile
        elif type_ == "6":
            return parse_follower
        elif type_ == '7':
            return parse_leader
        elif type_ == '8':
            return parse_post


    @staticmethod
    def id_hash_func(item):
        """
        func: filter data 
        """
        if "type" in item:
            type_ = item["type"]
            del item["type"]
            if type_ in ["6", '7']:
                return hashlib.md5((item["userId"] + item['followUserId'] +
                                    item['requestId']).encode("utf8")).hexdigest()
            else:
                return hashlib.md5((item["id"] + item['collectSeq'] + item['requestId']).encode("utf8")).hexdigest()


class WeiBoCrawler(object):
    def __init__(self):
        super().__init__()
        host, profile, relation, post, r, c = self.parameter_select()
        """ bGTWo7Az0k4Mcqr2gPnnJ6a2yxTXcb0ij9pOI0Ftkib56SeHkSZLjJ8bV7Tw2QNf """
        self.apikey = "fAV2GSoZ4OY0IoIyRZQdnB4bLcB2fyDe4DwIRkX1pAQuKbu1qcaNHPuOMV5J70N1"
        self.type_map = {
            "5": host + "profile/weibo?type=1&url=%s&apikey=%s",
            "6": host + "relation/weibo?type=follower&id=%s&apikey=%s",
            "7": host + "relation/weibo?type=leader&id=%s&apikey=%s",
            "8": host + "post/weibo?type=all&uid=%s&apikey=%s"
        }
        self.function_creator = FunctionCreator(self)
        self.mongo_writers_weibo_profile = ProcessFactory.create_writer(
            WriterConfig.WMongoConfig(profile, id_hash_func=self.function_creator.id_hash_func))
        self.mongo_writers_fans_leader = ProcessFactory.create_writer(
            WriterConfig.WMongoConfig(relation, id_hash_func=self.function_creator.id_hash_func))
        self.mongo_writers_profile_post = ProcessFactory.create_writer(
            WriterConfig.WMongoConfig(post, id_hash_func=self.function_creator.id_hash_func))

        self.weibo_profile = list()
        self.weibo_fans_leader = list()
        self.weibo_profile_post = list()


    async def url_generator(self):
        host, profile, relation, post, r, c = self.parameter_select()
        r_redis_config = GetterConfig.RRedisConfig(key=r['key'], host=r['host'], port=r['port'],
                                                   db=r['db'], password=r['password'])
        cli = await r_redis_config.get_redis_pool_cli()
        while True:
            task = await cli.lpop(r['key'], encoding="utf8")
            if task is None:
                logging.info("All task done")
                return
            type_, id_, key = task.split('|')
            id_ = re.search('.*?uid=(\d+)', id_).group(1)
            request_id, collect_seq, rate = key.split('&')  # 请求的id号，采集序列，采集时间的限制
            request_id = re.search('.*?=(.*)', request_id).group(1)
            collect_seq = re.search('.*?=(.*)', collect_seq).group(1)
            rate = re.search('.*?=(.*)', rate).group(1)
            if type_ == '5':
                param = 'https://www.weibo.com/' + id_
            else:
                param = id_

            start_url = self.type_map[type_] % (param, self.apikey)
            target_func = self.function_creator.filter_creator(request_id, collect_seq, rate, type_)
            if type_ == "5":
                yield GetterConfig.RAPIConfig(start_url, call_back=target_func)
            elif type_ == '8':
                yield GetterConfig.RAPIConfig(start_url, filter_=target_func, max_limit=200, trim_to_max_limit=True)
            else:
                yield GetterConfig.RAPIConfig(start_url, filter_=target_func)


    async def start(self):
        url_generator = self.url_generator()
        host, profile, relation, post, r, c = self.parameter_select()
        getter = ProcessFactory.create_getter(GetterConfig.RAPIBulkConfig(url_generator, concurrency=c))
        async for items in getter:
            for item in items:
                if item["type"] == "5":
                    self.weibo_profile.append(item)
                elif item["type"] in ("6", "7"):
                    self.weibo_fans_leader.append(item)
                elif item["type"] == "8":
                    self.weibo_profile_post.append(item)
            await self.perform_write()


    async def perform_write(self):
        tasks = [
            self.mongo_writers_weibo_profile.write(self.weibo_profile),
            self.mongo_writers_fans_leader.write(self.weibo_fans_leader),
            self.mongo_writers_profile_post.write(self.weibo_profile_post)
        ]
        await asyncio.gather(*tasks)
        self.weibo_profile.clear()
        self.weibo_fans_leader.clear()
        self.weibo_profile_post.clear()


    def parameter_select(self):
        select_number = 'server_test'
        select_redis = 'test'
        concurrency_ = 50
        """ redis Configuration"""
        redis_parameter = {}
        if select_redis == "formal":
            redis_parameter['key'] = 'fudan_task_queue'
            redis_parameter['host'] = "120.25.255.56"
            redis_parameter['port'] = 8000
            redis_parameter['db'] = 3
            redis_parameter['password'] = '7ef615c6c32f427d713144f67e2ef14d248c3ccd4bfd' \
                                          '6ddf33a4b56878bbcb2f8c72ae185427d853e08de0aa4a8aa7b3a'
        else:
            redis_parameter['key'] = 'fudan_task_queue'
            redis_parameter['host'] = "127.0.0.1"
            redis_parameter['port'] = 6379
            redis_parameter['db'] = 3
            redis_parameter['password'] = None
        """ Api/mongo Configuration"""
        if select_number != 'server_formal':
            if select_number == 'server_test':
                host = "http://10.26.222.219:8000/"
            else:
                host = "http://api01.bitspaceman.com:8000/"
            mongo_writers_profile = 'weibo_profile_%s' % (select_number,)
            mongo_writers_relation = 'weibo_relation_%s' % (select_number,)
            mongo_writers_post = 'weibo_post_%s' % (select_number,)
            return host, mongo_writers_profile, mongo_writers_relation, mongo_writers_post, redis_parameter, concurrency_
        elif select_number == 'server_formal':
            host = "http://10.26.222.219:8000/"
            mongo_writers_profile = 'weibo_profile'
            mongo_writers_relation = 'weibo_relation'
            mongo_writers_post = 'weibo_post'
            return host, mongo_writers_profile, mongo_writers_relation, mongo_writers_post, redis_parameter, concurrency_



if __name__ == "__main__":
    while True:
        loop = asyncio.get_event_loop()
        w = WeiBoCrawler()
        loop.run_until_complete(w.start())
        time.sleep(10)

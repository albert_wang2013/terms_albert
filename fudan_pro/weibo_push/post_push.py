# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2018/11/22 18:18
@Author  : Albert
@Func    : 
@File    : post_push.py
@Software: PyCharm
"""
import json
import asyncio
import logging
import time
from idataapi_transform import ProcessFactory, GetterConfig, WriterConfig
from idataapi_transform.DataProcess.Config.ConnectorConfig import session_manger
from datetime import datetime, timedelta
from pymongo import MongoClient


class PostDeal(object):
    def __init__(self):
        self.session = session_manger.get_session()
        self.current_hour = 0
        self.data_len = 0


    async def url_task(self, mongo_, datas, url_type):
        if url_type == 'test':
            host = 'http://gs.haoshuimian365.com:9000/'
        elif url_type == 'server':
            host = 'http://58.246.184.85:18088/'
        else:
            pass
        if 'weibo_post_' in mongo_:
            data = json.dumps(datas, ensure_ascii=False)
            url = host + 'api/weibo?appkey=4dkg33'
            await self.push(data, url)
        if 'weibo_comment_time_share' in mongo_:
            data = json.dumps(datas, ensure_ascii=False)
            url = host + 'api/weibo/comment?appkey=4dkg33'
            await self.push(data, url)
        if 'weibo_comment_hot' in mongo_:
            data = json.dumps(datas, ensure_ascii=False)
            url = host + 'api/weibo/hotcomment?appkey=4dkg33'
            await self.push(data, url)


    async def push(self, data, url, retry=0):
        data_size = json.loads(data)
        self.data_len += len(data_size['data'])
        type_ = 'server'
        if type_ == 'server':
            try:
                async with self.session.post(url=url, json=data) as resp:
                    logging.info(await resp.text())
                    text = await resp.text()
                    assert text == '{"appmsg":"success","appcode":"200"}'
                    logging.info('push sum:' + str(self.data_len))
            except Exception as e:
                if retry < 3:
                    return await self.push(data, url, retry + 1)
                else:
                    with open('error_push.txt', 'a+') as ff:
                        ff.write(data + '\n')
                        ff.flush()
                    logging.error("not ok ,please try again")
        else:
            logging.info('push sum:' + str(self.data_len))


    async def mongo_task_txt(self):
        txt_s = ['weibo_comment_time_share_reply_20181105_20181023005',]
        for txt_ in txt_s:
            logging.info('pre data_txt: %s' % (txt_s,))
            datas = {'data': []}
            with open('../weibo_post_data/{}.txt'.format(txt_), 'r') as f:
                lines = f.readlines()
                for line in lines:
                    line = line.replace("\n", "").strip()
                    data = json.loads(line)
                    datas['data'].append(data)
                    if len(datas['data']) == 500:
                        await self.url_task(txt_, datas)
                        datas = {'data': []}
                await self.url_task(txt_, datas)


    async def mongo_task(self, mongos, url_type):
        weibo_ids = self.weibo_ids()
        logging.info('pre weibo_ids_len: %s' % (len(weibo_ids),))
        for mongo_del in mongos:
            self.data_len = 0
            mongo_config = GetterConfig.RMongoConfig(mongo_del, per_limit=50000)
            mongo_getter = ProcessFactory.create_getter(mongo_config)
            async for items in mongo_getter:
                while items:
                    datas = {
                        'data': []
                    }
                    for i in range(800):
                        if len(items) > 0:
                            item = items.pop()
                            data = self.item_deal(item, weibo_ids)
                            if data:
                                datas['data'].append(data)
                        else:
                            break
                    if len(datas['data']) > 0:
                        await self.url_task(mongo_del, datas, url_type)


    def item_deal(self, item, weibo_set):
        item.pop('_id')
        """ 判断当前item 的数据是否属于要保留的数据 """
        if 'weiboId' in item:
            if item['weiboId'] in weibo_set:
                item = item
        elif 'id' in item:
            if item['id'] in weibo_set:
                item = item
        return item


    def mongo_col(self, mongo_list):
        """
        :param mongo_list: 需要处理的mongo列表
        :return: 返回 mongos 列表，可以直接用于处理
        """
        mongos = []
        for mongo_ in mongo_list:
            for data_ in range(20181113, 20181121):
                mongo_i = mongo_ + str(data_)
                mongos.append(mongo_i)
        return mongos


    def weibo_ids(self):
        """
        func: 将有效的id，比如出现84次，进行判断，进行 mongo 数据筛选
        :return:
        """
        weibo_set = set()
        with open(file='../ids_data/mid.txt', mode='r') as f:
            lines = f.readlines()
            for line in lines:
                id = line.replace("\n", "").strip()
                weibo_set.add(id)
        return weibo_set


if __name__ == "__main__":
    mongo_list = ['post', 'comment_hot', 'comment_time']
    url_type = 'test'
    mongos = PostDeal().mongo_col(mongo_list)
    task_ = PostDeal().mongo_task(mongos, url_type)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(task_)
    time.sleep(1000000)

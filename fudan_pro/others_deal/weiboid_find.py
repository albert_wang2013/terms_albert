# -*- coding: utf-8 -*-

import time
from datetime import datetime, timedelta
import json
import asyncio
import logging
from idataapi_transform import ProcessFactory, GetterConfig, WriterConfig
from idataapi_transform.DataProcess.Config.ConnectorConfig import session_manger
import logging
logger = logging.getLogger(__name__)


class UserPush(object):
    def __init__(self):
        self.session = session_manger.get_session()
        self.current_hour = 0

    def post(self, docs):
        doc = docs
        doc.pop('_id')
        data = {}
        data['id'] = doc['id']
        data['collectSeq'] = doc['collectSeq']
        return data


    async def example(self):    # for find你想要找的东西
        with open('id_seq.txt', 'a+') as f:
            for data_ in range(20181113, 20181121):
                if data_ in [20181113, 20181121]:
                    list_hour = ['16', '18', '20', '22']
                else:
                    list_hour = ['00', '02', '04', '06', '08', '10', '12', '14', '16', '18', '20', '22']
                for hour_ in list_hour:
                    mongo_del = 'weibo_post_' + str(data_) + str(hour_)
                    mongo_config = GetterConfig.RMongoConfig(mongo_del, per_limit=20000)
                    mongo_getter = ProcessFactory.create_getter(mongo_config)
                    async for items in mongo_getter:
                        for item in items:
                            data = self.post(item)
                            f.write(json.dumps(data, ensure_ascii=False) + '\n')
                            f.flush()




if __name__ == "__main__":
    logger.info('run start========')
    now = (datetime.now())
    ex = UserPush()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(ex.example())
    logger.info('its stop========')
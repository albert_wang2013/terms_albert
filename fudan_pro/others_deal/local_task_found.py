# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2018/11/22 15:49
@Author  : Albert
@Func    :  本地任务建立
@File    : local_task_found.py
@Software: PyCharm
"""

import redis
_local_pool = redis.ConnectionPool(host="127.0.0.1",
                                   port=6379,
                                   db=0)
_local_redis = redis.StrictRedis(connection_pool=_local_pool)

count = 0
with open("id.txt", "r", encoding="utf8") as f:
    for line in f:
        r = '9|mid={}|requestId=a18eebd7a5bd4f42a14826c470f19f50&collectSeq=1&rate=1'.format(line.strip())
        _local_redis.lpush("fudan_task_queue_test", r)
        count += 1
        if count >= 10000:
           break
# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2018/11/22 15:08
@Author  : Albert
@Func    : 
@File    : weibo_task_found.py
@Software: PyCharm
"""
import asyncio
import json
import logging
import time
import aiohttp



""" 无该条件，就无法输出到控制台 """
logging.basicConfig(level=logging.INFO)

def file_open(filename):
    with open(file=filename, mode='r') as f:
        lines = f.readlines()
        return lines


async def weibo_task(filename, requestid, rate, count, type_):
    lines = file_open(filename)
    weibo_ids = []
    for id in lines:
        id = id.replace("\n", "").strip()
        weibo_ids.append(id)
    if type_ == 'user':
        url = 'http://120.76.157.46:8800/weibo/user?apikey=6NVx1qta7XN6oSGCv4Bf1ieKK3cguZ9y5XyfH7aXMLz4xDjIUn7QJbEWLXRutRfx'
    elif type_ == 'post':
        url = 'http://120.76.157.46:8800/weibo?apikey=6NVx1qta7XN6oSGCv4Bf1ieKK3cguZ9y5XyfH7aXMLz4xDjIUn7QJbEWLXRutRfx'
    else:
        url = None
    data = {
        'id': requestid,
        'uids': weibo_ids,
        'rate': rate,
        'count': count,
    }
    data = json.dumps(data, ensure_ascii=False)
    async with aiohttp.ClientSession() as session:
        async with session.post(url=url, data=data) as resp:
            logging.info(await resp.text())



if __name__ == '__main__':
    """ for ins:
                ../ids_data/test_id.txt
    """
    filename = input('please input filename:\n')
    requestid = '20181023'
    rate = 1
    count = 1
    type_ = 'user'  # 判断启用那种任务请求
    task_ = weibo_task(filename, requestid, rate, count, type_)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(task_)




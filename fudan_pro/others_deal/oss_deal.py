# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2018/11/22 12:02
@Author  : Albert
@Func    : 
@File    : oss_deal.py
@Software: PyCharm
"""
import logging
import re
import oss2
import sys



""" 无该条件，就无法输出到控制台 """
logging.basicConfig(level=logging.INFO)
auth = oss2.Auth('7JT8SvX8zSsudTIb', 'XawzZPcAXLdapOx5Rn1nptjD7r3Z5U')
""" 声明使用的所处环境"""
type_ = 'local'
if type_ == 'server':
    endpoint = 'oss-cn-shenzhen-internal.aliyuncs.com'
elif type_ == 'local':
    endpoint = 'oss-cn-shenzhen.aliyuncs.com'
bucket_name = 'common-data'


def oss_upload(filename):
    bucket = oss2.Bucket(auth, endpoint, bucket_name)
    logging.info('oss upload start, please wait a min')
    if '/' in filename:
        file = re.search('.*/(.*)', filename).group(1)
    else:
        file = filename
    oss2.resumable_upload(
        bucket=bucket, key=file, filename=filename, store=oss2.ResumableStore(root='/tmp'),
        multipart_threshold=4 * 1024 * 1024 * 1024, part_size=1024 * 1024 * 1024, num_threads=1,
    )
    logging.info('oss upload stop: %s file is ok' % (file,))
    url = bucket.sign_url('GET', key=filename, expires=3600*18)
    logging.info('oss_url :\n %s ' % (url,))



if __name__ == '__main__':
    """ for ins:
                ../weibo_user_data/data_test.txt
    """
    filename = input('please input filename  which you want upload \n')
    oss_upload(filename)


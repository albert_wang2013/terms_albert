# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2018/11/22 11:49
@Author  : Albert
@Func    : 
@File    : mongo_collection_delete.py
@Software: PyCharm
"""
import logging
from pymongo import MongoClient


""" 无该条件，就无法输出到控制台 """
logging.basicConfig(level=logging.INFO)

def mongo_select(mongo_):
    if mongo_ == 'server':
        MONGODB_HOST = '120.79.97.220'
        MONGODB_PORT = 55555
        MONGODB_USER = 'writer'
        MONGODB_PASSWD = 'writer'
        mongodb_cli = MongoClient(host=MONGODB_HOST, port=MONGODB_PORT)
        db = mongodb_cli['weibo']
        return db.authenticate(MONGODB_USER, MONGODB_PASSWD)
    elif mongo_ == 'local':
        MONGODB_HOST = '127.0.0.1'
        MONGODB_PORT = 12707
        MONGODB_USER = None
        MONGODB_PASSWD = None
        mongodb_cli = MongoClient(host=MONGODB_HOST, port=MONGODB_PORT)
        db = mongodb_cli['weibo']
        return db.authenticate(MONGODB_USER, MONGODB_PASSWD)

def mongo_delete():
    mongo_ = 'local'
    db = mongo_select(mongo_)
    col = db.collection_names()  # 连接到数据库
    for c in col:
        logging.info(c)
        if 'xxx' in c:
            db.drop_collection(c)
            logging.info('mongo_collect %s : is deleted!' % (c,))


if __name__ == '__main__':
    mongo_delete()
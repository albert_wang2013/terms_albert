# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/9/19 16:22
@Author  : Albert
@Func    :
@File    : mongo_to_mongo.py
@Software: PyCharm
"""
import asyncio

from idataapi_transform import ProcessFactory, GetterConfig, WriterConfig


class DataDeal():
    def __init__(self):
        self.max_limit = None
        self.col_name = 'lzy_twitter_relations'
        self.host = '123.196.116.97'

    async def get_mongo_data(self):
        mongo_config = GetterConfig.RMongoConfig(
            self.col_name,
            host=self.host,
            port=20020,
            username='wm_terms',
            password='qwer',
            database='wm',
            encoding='utf8',
            per_limit=1000,
            max_limit=self.max_limit
        )
        mongo_getter = ProcessFactory.create_getter(mongo_config)
        async for items in mongo_getter:
            items = await self.deal_(items)
            if items:
                await self.push_mongo_data(items)

    async def deal_(self, items):
        uids = [
            '',
            'MilliOyghunush',
            'MarinaWalkerG',
            'patrickpoon',
            'sarkikebir',
            'Junmai1103',
            'ak_mack',
            'ihhinsaniyardim',
            'RobbieGramer',
            'YY56936953',
            'Bsintash',
            'BelgiumUyghur',
            'HalmuratU',
            'He_Shumei',
            'ICIJorg',
            'shirafu',
            'turkistan_tv',
            'sashachavkin',
            'JewherIlham',
            'amytheblue',
            'ChuBailiang',
            'arslan_hidayat',
            'dakekang',
            'antoniocuga',
            'deanstarkman',
            'Hamish SSBoland-Rudder',
            'turkistaner',
            'KuzzatAltay',
            'xu_xiuzhong',
            'ChrisRickleton',
            'wang_maya',
            'rhpsia',
            'mervesebnem',
            'AbdugheniSabit',
            'NoRightsNoGames',
            'Sophiemcneill',
            'ssbilir_',
            'JNBPage',
            'tomstites',
            'NEDemocracy',
            'BethanyAllenEbr',
            'AidUyghur',
            'Salih_Hudayar',
            'Memettohti',
            'austinramzy',
            'Nytimes',
            'Turkistantmes',
            'eastturkistann',
            'FergusShiel',
            'Jenn_Chowdhury',
            'sedat_peker',
            'joshchin',

        ]
        datas = []
        for item in items:
            if item['subjectId'] in uids:
                datas.append(item)
        if datas:
            return datas
        else:
            return None


    async def push_mongo_data(self, items):
        mongo_ = WriterConfig.WMongoConfig(
            'lzy_twitter_relations_pro',
            host=self.host,
            port=20020,
            username='wm_terms',
            password='qwer',
            database='wm',
            encoding='utf8',
            )
        await ProcessFactory.create_writer(mongo_).write([i for i in items if i])


if __name__ == '__main__':
    d = DataDeal()
    asyncio.run(d.get_mongo_data())




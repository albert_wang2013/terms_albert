# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/8/6 17:27
@Author  : Albert
@Func    :
@File    : schedule_main.py
@Software: PyCharm
"""
import asyncio
import datetime
import logging
import time
from crawl_spiders.post import PostCrawl
from crawl_spiders.comment_share_liker import CommentShareLikeCrawl
from crawl_spiders.lzy_oss_deal import LZYOSSDeal
from crawl_spiders.mongo_to_local_main import MongoToLocal
from crawl_spiders.profile_relation import ProfileRelationCrawlFB
from crawl_spiders.profile_relation_twitter import ProfileRelationCrawlTW

logging.basicConfig(level=logging.INFO)


class Main():
    async def start(self):
        # await PostCrawl().post_main()     # 采集post
        tasks = [
            ProfileRelationCrawlFB().profile_relation_main(),     # 采集fb用户信息、用户关系
            # ProfileRelationCrawlTW().profile_relation_main(),     # 采集twitter用户信息、用户关系
            # CommentShareLikeCrawl().comment_share_liker_main(),     # 采集评论等
            # LZYOSSDeal().lzy_main()     # 处理图片视频等，并且上传至oss
        ]
        # await asyncio.gather(*tasks)
        await MongoToLocal().main()
        logging.info('tasks  running over!!! ')

    async def main(self):
        await self.start()
        logging.info('schedule_main ending!!!! time_sleep 1day')
        """程序定时循环"""
        while True:
            start_ = datetime.datetime.now().replace(microsecond=0)
            if '07:00:00' in str(start_):
                await self.start()
                logging.info('schedule_main ending!!!! time_sleep 1day')

    async def test(self):
        await ProfileRelationCrawlFB().profile_relation_main()     # 采集用户信息、用户关系


if __name__ == '__main__':
    """程序启动"""
    loop = asyncio.get_event_loop()
    loop.run_until_complete(Main().main())
    # loop.run_until_complete(Main().test())
    # loop.run_forever()
    loop.close()
    # time.sleep(3600)

# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/8/6 18:23
@Author  : Albert
@Func    :
@File    : mongo_to_local_main.py
@Software: PyCharm
"""
import zipfile
import aiofiles
import aiohttp
import oss2
import asyncio
import configparser
import json
import logging
import os
import time
from idataapi_transform import ProcessFactory, GetterConfig

logging.basicConfig(level=logging.INFO)


class ConfigPar(object):
    """
    func:配置理解就行
    """

    def __init__(self):
        super().__init__()
        self.conf = configparser.ConfigParser()
        self.conf.read('config.ini')

    def config_func(self):
        max_sema = self.conf.get('lzy_term', 'max_sema')
        max_retries = self.conf.get('lzy_term', 'max_retries')
        host = self.conf.get('lzy_term', 'host')
        apikey = self.conf.get('lzy_term', 'apikey')
        get_id_mode = self.conf.get('lzy_term', 'get_id_mode')
        max_limit = self.conf.get('lzy_term', 'max_limit')
        per_limit = self.conf.get('lzy_term', 'per_limit')
        max_conn_time_out = self.conf.get('lzy_term', 'max_conn_time_out')
        run_env = self.conf.get('lzy_term', 'run_env')
        oss_endpoint = self.conf.get('lzy_term', 'oss_endpoint')
        oss_endpoint_commmon = self.conf.get(
            'lzy_term', 'oss_endpoint_commmon')
        oss_bucket_name = self.conf.get('lzy_term', 'oss_bucket_name')
        return int(max_sema), int(max_retries), host, apikey, get_id_mode, int(max_limit) if max_limit != 'None' else None, int(
            per_limit), int(max_conn_time_out), run_env, oss_endpoint, oss_endpoint_commmon, oss_bucket_name


class MongoToLocal():
    def __init__(self):
        super().__init__()
        config_list = ConfigPar().config_func()
        self.con = config_list[0]
        self.max_retry = config_list[1]
        self.host = config_list[2]
        self.apikey = config_list[3]
        self.get_id_mode = config_list[4]
        self.max_limit = config_list[5]
        self.per_limit = config_list[6]
        self.time_out = config_list[7]
        self.run_env = config_list[8]
        self.oss_endpoint = config_list[9]
        self.oss_endpoint_commmon = config_list[10]
        self.oss_bucket_name = config_list[11]
        self.current_time = time.strftime("%Y%m%d", time.localtime())  # 当前时间
        self.root_dir = os.getcwd() + '/{}_datas'.format(self.current_time)  # 下载文件的保存目录
        self.root_dir_children = 'profiles_relations_20200612'
        self.root_dir1 = os.getcwd() + '/{}'.format(self.root_dir_children)  # 下载文件的保存目录

    async def main(self):
        os.makedirs(self.root_dir, exist_ok=True)
        os.makedirs(self.root_dir1, exist_ok=True)
        list_ = [
            # 'posts',
            # 'comments',
            # 'likes',
            # 'shares',
            'twitter_profiles', 'twitter_relations',
            'facebook_relations', 'facebook_profiles',
        ]
        tasks = []
        for i in list_:
            mongo_config, col_name = await self.mongo_(i)
            task1 = asyncio.ensure_future(
                self.mongo_get(mongo_config, col_name))
            tasks.append(task1)
        await asyncio.gather(*tasks)
        if os.path.isdir(self.root_dir):
            await self.zip_files()
        if os.path.isdir(self.root_dir1):
            await self.zip_files1()     # profile等
        await self.oss_upload()
        logging.info('MongoToLocal().main()  running over!!! ')

    async def zip_files(self):
        zipf = zipfile.ZipFile(
            '{}.zip'.format(self.root_dir_children), 'w')
        pre_len = len(os.path.dirname(self.root_dir1))
        for parent, dir_names, file_names in os.walk(self.root_dir1):
            for filename in file_names:
                path_file = os.path.join(parent, filename)
                arc_name = path_file[pre_len:].strip(os.path.sep)  # 相对路径
                zipf.write(path_file, arc_name)
        zipf.close()

    async def zip_files1(self):
        zipf = zipfile.ZipFile(
            '{}_datas.zip'.format(
                self.current_time), 'w')
        pre_len = len(os.path.dirname(self.root_dir))
        for parent, dir_names, file_names in os.walk(self.root_dir):
            for filename in file_names:
                path_file = os.path.join(parent, filename)
                arc_name = path_file[pre_len:].strip(os.path.sep)  # 相对路径
                zipf.write(path_file, arc_name)
        zipf.close()

    async def oss_upload(self):
        logging.info('oss upload start, please wait a min')
        auth = oss2.Auth('7JT8SvX8zSsudTIb', 'XawzZPcAXLdapOx5Rn1nptjD7r3Z5U')
        bucket = oss2.Bucket(auth, self.oss_endpoint, self.oss_bucket_name)
        for file_ in [
                # '{}_datas.zip'.format(
                #     self.current_time),
                '{}.zip'.format(self.root_dir_children)]:
            logging.info(file_)
            filename = os.getcwd() + '/' + file_
            logging.info(filename)
            oss2.resumable_upload(
                bucket=bucket,
                key=file_,
                filename=filename,
                store=oss2.ResumableStore(
                    root='/tmp'),
                multipart_threshold=4 *
                1024 *
                1024 *
                1024,
                part_size=1024 *
                1024,
                num_threads=1,
            )
            logging.info('oss upload stop: %s file is ok' % (file_,))
            bucket = oss2.Bucket(
                auth,
                self.oss_endpoint_commmon,
                self.oss_bucket_name)
            url = bucket.sign_url('GET', key=file_, expires=3600 * 24 * 10)
            logging.info('oss_url :\n {} \n'.format(url))

    async def mongo_get(self, mongo_config, col_name):
        if col_name in [
            'lzy_twitter_profiles', 'lzy_twitter_relations',
            'lzy_facebook_relations', 'lzy_facebook_profiles',
        ]:
            file_path = self.root_dir1 + '/{}.json'.format(col_name)
        else:
            file_path = self.root_dir + '/{}.json'.format(col_name)
        logging.info('file_path: {}'.format(file_path))
        async with aiofiles.open(file_path, encoding='utf8', mode='a+') as f:
            mongo_getter = ProcessFactory.create_getter(mongo_config)
            async for items in mongo_getter:
                for item in items:
                    del item['_id']
                    await f.write(json.dumps(item))
                    await f.write('\n')

    async def mongo_(self, name_):
        if name_ in [
            'twitter_profiles', 'twitter_relations',
            'facebook_relations', 'facebook_profiles',
        ]:
            col_name = 'lzy_{}'.format(name_)
        else:
            col_name = 'lzy_facebook_{}_{}'.format('20200612', name_)    # lzy_facebook_20200309_posts
        mongo_config = GetterConfig.RMongoConfig(
            col_name,
            host=self.host,
            port=20020,
            username='wm_terms',
            password='qwer',
            database='wm',
            encoding='utf8',
            per_limit=self.per_limit,
            max_limit=self.max_limit)
        return mongo_config, col_name

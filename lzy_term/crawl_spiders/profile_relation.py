# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/8/5 10:22
@Author  : Albert
@Func    :
@File    : profile_relation.py
@Software: PyCharm
"""
import asyncio
import configparser
import hashlib
import logging
import os
import sys
import time

from idataapi_transform import ProcessFactory, GetterConfig, WriterConfig
import aiofiles
logging.basicConfig(level=logging.INFO)


class ConfigPar(object):
    """
    func:配置理解就行
    """

    def __init__(self):
        super().__init__()
        self.conf = configparser.ConfigParser()
        self.conf.read('config.ini')

    def config_func(self):
        max_sema = self.conf.get('lzy_term', 'max_sema')
        max_retries = self.conf.get('lzy_term', 'max_retries')
        host = self.conf.get('lzy_term', 'host')
        apikey = self.conf.get('lzy_term', 'apikey')
        get_id_mode = self.conf.get('lzy_term', 'get_id_mode')
        return int(max_sema), int(max_retries), host, apikey, get_id_mode


class ProfileRelationCrawlFB():
    def __init__(self):
        super().__init__()
        config_list = ConfigPar().config_func()
        self.profile = 'https://api02.bitspaceman.com/profile/facebook?id={}&apikey={}'
        self.follower = 'https://api02.bitspaceman.com/relation/facebook?type=friends&uid={}&apikey={}'
        self.con = config_list[0]
        self.max_retry = config_list[1]
        self.host = config_list[2]
        self.apikey = config_list[3]
        self.get_id_mode = config_list[4]

    async def get_ids(self):
        file_dir = os.path.split(os.path.realpath(sys.argv[0]))[0]
        file = 'facebook_uids.txt'
        file_path = file_dir + '/' + file
        logging.info('file path is:{}'.format(file_path))
        async with aiofiles.open(file_path, encoding='utf8', mode='r') as f:
            ids_ = await f.readlines()
            ids = []
            for id_ in ids_:
                ids.append(id_.strip())
            return ids

    async def get_api_ids(self):
        pass

    async def start_(self, ids):
        logging.info('ids length:{}'.format(len(ids)))
        tasks = [
            self.request_profile(ids),
            self.request_relation(ids),
        ]
        await asyncio.gather(*tasks)

    async def request_profile(self, ids):
        tasks = []
        for id_ in ids:
            url_profile = self.profile.format(id_, self.apikey)
            api_config = GetterConfig.RAPIConfig(
                url_profile, max_retry=self.max_retry)
            tasks.append(api_config)
        api_getter = ProcessFactory.create_getter(
            GetterConfig.RAPIBulkConfig(
                tasks, concurrency=self.con))
        datas_list = []
        async for items in api_getter:
            for item in items:
                if len(datas_list) >= 50:  # 每获取50条数据，存储一次进redis
                    await ProcessFactory.create_writer(self.mongo_sel('facebook_profiles')).write(
                        [i for i in datas_list if i])
                    logging.info(
                        'writer facebook_profiles-items to mongo length:{}'.format(len(datas_list)))
                    datas_list.clear()
                    datas_list.append(item)
                else:
                    datas_list.append(item)
        if datas_list:
            await ProcessFactory.create_writer(self.mongo_sel('facebook_profiles')).write(
                [i for i in datas_list if i])
            logging.info(
                'writer facebook_profiles-items to redis length:{}'.format(len(datas_list)))
            datas_list.clear()
        # await ProcessFactory.create_writer(self.mongo_sel('facebook_profiles')).write([i for i in datas_list if i])

    async def request_relation(self, ids):
        tasks = []
        for id_ in ids:
            url_realtion = self.follower.format(id_, self.apikey)
            api_config = GetterConfig.RAPIConfig(
                url_realtion, max_retry=self.max_retry)
            tasks.append(api_config)
        api_getter = ProcessFactory.create_getter(
            GetterConfig.RAPIBulkConfig(
                tasks, concurrency=self.con))
        datas_list = []
        async for items in api_getter:
            for item in items:
                item['type'] = 'follower'
                # datas_list.append(item)
                if len(datas_list) >= 50:  # 每获取50条数据，存储一次进redis
                    await ProcessFactory.create_writer(self.mongo_sel('facebook_relations')).write(
                        [i for i in datas_list if i])
                    logging.info(
                        'writer facebook_relations-items to mongo length:{}'.format(len(datas_list)))
                    datas_list.clear()
                    datas_list.append(item)
                else:
                    datas_list.append(item)
        if datas_list:
            await ProcessFactory.create_writer(self.mongo_sel('facebook_relations')).write(
                [i for i in datas_list if i])
            logging.info(
                'writer facebook_relations-items to redis length:{}'.format(len(datas_list)))
            datas_list.clear()
        # await ProcessFactory.create_writer(self.mongo_sel('facebook_relations')).write([i for i in datas_list if i])

    async def data_write(self):
        pass

    def mongo_sel(self, name_):
        col_name = 'lzy_{}'.format(name_)
        mongo_ = WriterConfig.WMongoConfig(
            col_name,
            host=self.host,
            port=20020,
            username='wm_terms',
            password='qwer',
            database='wm',
            encoding='utf8',
            id_hash_func=self.id_hash_func)
        return mongo_

    @staticmethod
    def id_hash_func(item):
        if "id" in item and item["id"]:
            value = (item["id"]).encode("utf8")
        elif "subjectId" in item and item["subjectId"] and "objectId" in item and item["objectId"]:
            value = (item["subjectId"] + "_" + item["objectId"]).encode("utf8")
        else:
            value = str(item).encode("utf8")
        return hashlib.md5(value).hexdigest()

    async def query_mongo(self, ids):
        ids_s = []
        for id_ in ids:
            try:
                float(id_)
                query_body = {'id': str(id_)}
            except BaseException:
                query_body = {'userName': str(id_)}
            mongo_config = GetterConfig.RMongoConfig(
                'lzy_facebook_profiles',
                host=self.host,
                port=20020,
                username='wm_terms',
                password='qwer',
                database='wm',
                max_retry=2,
                query_body=query_body,
                return_source=False)
            mongo_getter = ProcessFactory.create_getter(mongo_config)
            async for items in mongo_getter:
                if items:
                    ids_s.append(id_)
        ids_ss = list(filter(lambda x: x not in ids_s, ids))
        return ids_ss

    async def profile_relation_main(self):
        """
        :func: 主程序
        """
        if self.get_id_mode == 'file':
            ids = await self.get_ids()
        elif self.get_id_mode == 'api':
            ids = await self.get_api_ids()
        else:
            raise Exception("Invalid params error!")
        logging.info('get ids length:{}'.format(len(ids)))
        # ids = await self.query_mongo(ids)   # 动态更新新需要采集的用户id，采集其用户信息，用户关系
        # logging.info('need deal ids length:{}'.format(len(ids)))
        if ids:
            ids_ = []
            for id_ in ids:
                if len(ids_) >= 100:     # 每次从内存中取固定size的id，去请求。
                    await self.start_(ids_)
                    ids_.clear()
                    ids_.append(id_)
                else:
                    ids_.append(id_)
            if ids_:
                await self.start_(ids_)
        logging.info(
            'ProfileRelationCrawl().profile_relation_main()  running over!!! ')

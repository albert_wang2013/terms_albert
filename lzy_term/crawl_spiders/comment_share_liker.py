# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/8/6 10:08
@Author  : Albert
@Func    :
@File    : comment_share_liker.py
@Software: PyCharm
"""
import asyncio
import configparser
import hashlib
import logging
import time
from idataapi_transform import ProcessFactory, GetterConfig, WriterConfig

logging.basicConfig(level=logging.INFO)


class ConfigPar(object):
    """
    func:配置理解就行
    """

    def __init__(self):
        super().__init__()
        self.conf = configparser.ConfigParser()
        self.conf.read('config.ini')

    def config_func(self):
        max_sema = self.conf.get('lzy_term', 'max_sema')
        max_retries = self.conf.get('lzy_term', 'max_retries')
        host = self.conf.get('lzy_term', 'host')
        apikey = self.conf.get('lzy_term', 'apikey')
        get_id_mode = self.conf.get('lzy_term', 'get_id_mode')
        max_limit = self.conf.get('lzy_term', 'max_limit')
        per_limit = self.conf.get('lzy_term', 'per_limit')
        req_host = self.conf.get('lzy_term', 'req_host')
        return int(max_sema), int(max_retries), host, apikey, get_id_mode, int(
            max_limit) if max_limit != 'None' else None, int(per_limit), req_host


class CommentShareLikeCrawl():
    def __init__(self):
        super().__init__()
        config_list = ConfigPar().config_func()
        self.share = '{}/post/facebook?type=3&pid={}&apikey={}'
        self.like = '{}/post/facebook?type=2&pid={}&apikey={}'
        self.comment = '{}/comment/facebook?id={}&apikey={}'
        self.con = config_list[0]
        self.max_retry = config_list[1]
        self.host = config_list[2]
        self.apikey = config_list[3]
        self.get_id_mode = config_list[4]
        self.max_limit = config_list[5]
        self.per_limit = config_list[6]
        self.req_host = config_list[7]

    async def get_mongo_ids(self):
        col_name = 'lzy_facebook_{}_posts'.format(self.current_time)
        mongo_config = GetterConfig.RMongoConfig(
            col_name,
            host=self.host,
            port=20020,
            username='wm_terms',
            password='qwer',
            database='wm',
            encoding='utf8',
            per_limit=self.per_limit,
            max_limit=self.max_limit)
        mongo_getter = ProcessFactory.create_getter(mongo_config)
        ids = []
        async for items in mongo_getter:
            for item in items:
                id_ = item['id']
                ids.append(id_)
        return ids

    def mongo_sel(self, name_):
        col_name = 'lzy_facebook_{}_{}'.format(self.current_time, name_)
        mongo_ = WriterConfig.WMongoConfig(
            col_name,
            host=self.host,
            port=20020,
            username='wm_terms',
            password='qwer',
            database='wm',
            encoding='utf8',
            id_hash_func=self.id_hash_func)
        return mongo_

    @staticmethod
    def id_hash_func(item):
        if "id" in item and item["id"]:
            value = (item["id"]).encode("utf8")
        elif "subjectId" in item and item["subjectId"] and "objectId" in item and item["objectId"]:
            value = (item["subjectId"] + "_" + item["objectId"]).encode("utf8")
        elif "likerId" in item and item["likerId"]:
            value = (item["likerId"]).encode("utf8")
        elif "sharerPostId" in item and item["sharerPostId"]:
            value = (item["sharerPostId"]).encode("utf8")
        else:
            value = str(item).encode("utf8")
        return hashlib.md5(value).hexdigest()

    async def start_(self, ids):
        logging.info('ids length:{}'.format(len(ids)))
        tasks = []
        for id_ in ids:
            url_comment = self.comment.format(self.req_host, id_, self.apikey)
            url_share = self.share.format(self.req_host, id_, self.apikey)
            url_like = self.like.format(self.req_host, id_, self.apikey)
            task1 = asyncio.ensure_future(self.request_comment(url_comment))
            task2 = asyncio.ensure_future(self.request_share(url_share))
            task3 = asyncio.ensure_future(self.request_like(url_like))
            tasks.append(task1)
            tasks.append(task2)
            tasks.append(task3)
        await asyncio.gather(*tasks)

    async def request_comment(self, url_comment):
        api_config = GetterConfig.RAPIConfig(
            url_comment,
            max_retry=self.max_retry * 3,
            random_min_sleep=1,
            random_max_sleep=3)
        api_getter = ProcessFactory.create_getter(
            GetterConfig.RAPIBulkConfig(
                [api_config], concurrency=self.con))
        datas_list = []
        async for items in api_getter:
            for item in items:
                datas_list.append(item)
            await ProcessFactory.create_writer(self.mongo_sel('comments')).write([i for i in datas_list if i])

    async def request_share(self, url_share):
        api_config = GetterConfig.RAPIConfig(
            url_share,
            max_retry=self.max_retry,
            random_min_sleep=1,
            random_max_sleep=3)
        api_getter = ProcessFactory.create_getter(
            GetterConfig.RAPIBulkConfig(
                [api_config], concurrency=self.con))
        datas_list = []
        async for items in api_getter:
            for item in items:
                datas_list.append(item)
            await ProcessFactory.create_writer(self.mongo_sel('shares')).write([i for i in datas_list if i])

    async def request_like(self, url_like):
        api_config = GetterConfig.RAPIConfig(
            url_like,
            max_retry=self.max_retry,
            random_min_sleep=1,
            random_max_sleep=3)
        api_getter = ProcessFactory.create_getter(
            GetterConfig.RAPIBulkConfig(
                [api_config], concurrency=self.con))
        datas_list = []
        async for items in api_getter:
            for item in items:
                datas_list.append(item)
            await ProcessFactory.create_writer(self.mongo_sel('likes')).write([i for i in datas_list if i])

    async def start_main(self):
        """
        :func: 程序开始
        """
        self.current_time = time.strftime("%Y%m%d", time.localtime())  # 当前时间
        self.time_int = int(
            time.mktime(
                time.strptime(
                    self.current_time,
                    '%Y%m%d')))
        ids = await self.get_mongo_ids()
        logging.info(
            'comment_share_liker-schedule-ids length:{}'.format(len(ids)))
        ids_ = []
        for id_ in ids:
            if len(ids_) >= 100:  # 每次从内存中取固定size的id，去请求。
                await self.start_(ids_)
                ids_.clear()
                ids_.append(id_)
            else:
                ids_.append(id_)
        if ids_:
            await self.start_(ids_)

    async def comment_share_liker_main(self):
        """
        func：调度主程序
        """
        await self.start_main()
        logging.info(
            'CommentShareLikeCrawl().comment_share_liker_main()  running over!!! ')

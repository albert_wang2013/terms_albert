# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/8/6 11:35
@Author  : Albert
@Func    :
@File    : lzy_oss_deal.py
@Software: PyCharm
"""
import zipfile
import aiohttp
import oss2
from retrying import retry
from PIL import Image
from io import BytesIO
import asyncio
import configparser
import hashlib
import json
import logging
import os
import time
from idataapi_transform import ProcessFactory, GetterConfig, WriterConfig
from tqdm import tqdm


logging.basicConfig(level=logging.INFO)


class ConfigPar(object):
    """
    func:配置理解就行
    """

    def __init__(self):
        super().__init__()
        self.conf = configparser.ConfigParser()
        self.conf.read('config.ini')

    def config_func(self):
        max_sema = self.conf.get('lzy_term', 'max_sema')
        max_retries = self.conf.get('lzy_term', 'max_retries')
        host = self.conf.get('lzy_term', 'host')
        apikey = self.conf.get('lzy_term', 'apikey')
        get_id_mode = self.conf.get('lzy_term', 'get_id_mode')
        max_limit = self.conf.get('lzy_term', 'max_limit')
        per_limit = self.conf.get('lzy_term', 'per_limit')
        max_conn_time_out = self.conf.get('lzy_term', 'max_conn_time_out')
        run_env = self.conf.get('lzy_term', 'run_env')
        oss_endpoint = self.conf.get('lzy_term', 'oss_endpoint')
        oss_endpoint_commmon = self.conf.get(
            'lzy_term', 'oss_endpoint_commmon')
        oss_bucket_name = self.conf.get('lzy_term', 'oss_bucket_name')
        return int(max_sema), int(max_retries), host, apikey, get_id_mode, int(max_limit) if max_limit != 'None' else None, int(
            per_limit), int(max_conn_time_out), run_env, oss_endpoint, oss_endpoint_commmon, oss_bucket_name


class LZYOSSDeal():
    def __init__(self):
        super().__init__()
        config_list = ConfigPar().config_func()
        self.con = config_list[0]
        self.max_retry = config_list[1]
        self.host = config_list[2]
        self.apikey = config_list[3]
        self.get_id_mode = config_list[4]
        self.max_limit = config_list[5]
        self.per_limit = config_list[6]
        self.time_out = config_list[7]
        self.run_env = config_list[8]
        self.oss_endpoint = config_list[9]
        self.oss_endpoint_commmon = config_list[10]
        self.oss_bucket_name = config_list[11]
        self.current_time = time.strftime("%Y%m%d", time.localtime())  # 当前时间
        self.root_dir = os.getcwd() + '/{}_video_images'.format(self.current_time)  # 下载文件的保存目录

    async def get_mongo_urls(self):
        def deal_item(item):
            urls = []
            if item['imageUrls']:
                for i in item['imageUrls']:
                    dict_ = {}
                    dict_[
                        '{}_{}_imageUrls'.format(
                            item['id'],
                            item['imageUrls'].index(i))] = i
                    urls.append(dict_)
            if item['videoUrls']:
                for i in item['videoUrls']:
                    dict_ = {}
                    dict_[
                        '{}_{}_videoUrls'.format(
                            item['id'],
                            item['videoUrls'].index(i))] = i
                    urls.append(dict_)
            return urls
        col_name = 'lzy_facebook_{}_posts'.format(self.current_time)
        mongo_config = GetterConfig.RMongoConfig(
            col_name,
            host=self.host,
            port=20020,
            username='wm_terms',
            password='qwer',
            database='wm',
            encoding='utf8',
            per_limit=self.per_limit,
            max_limit=self.max_limit)
        mongo_getter = ProcessFactory.create_getter(mongo_config)
        all_dict_ids = []
        async for items in mongo_getter:
            for item in items:
                urls = deal_item(item)
                if urls:
                    for i in urls:
                        all_dict_ids.append(i)
        return all_dict_ids

    def mongo_sel(self, name_):
        col_name = 'lzy_facebook_{}_{}'.format(self.current_time, name_)
        mongo_ = WriterConfig.WMongoConfig(
            col_name,
            host=self.host,
            port=20020,
            username='wm_terms',
            password='qwer',
            database='wm',
            encoding='utf8',
            id_hash_func=self.id_hash_func)
        return mongo_

    @staticmethod
    def id_hash_func(item):
        if "id" in item and item["id"]:
            value = (item["id"]).encode("utf8")
        elif "subjectId" in item and item["subjectId"] and "objectId" in item and item["objectId"]:
            value = (item["subjectId"] + "_" + item["objectId"]).encode("utf8")
        elif "likerId" in item and item["likerId"]:
            value = (item["likerId"]).encode("utf8")
        elif "sharerPostId" in item and item["sharerPostId"]:
            value = (item["sharerPostId"]).encode("utf8")
        else:
            value = str(item).encode("utf8")
        return hashlib.md5(value).hexdigest()

    async def start_(self, ids):
        logging.info('ids length:{}'.format(len(ids)))
        tasks = []
        for id_ in ids:
            task1 = asyncio.ensure_future(self.request_oss(id_))
            tasks.append(task1)
        await asyncio.gather(*tasks)

    @retry(stop_max_attempt_number=5)
    async def request_oss(self, id_):
        # print(self.root_dir)
        os.makedirs(self.root_dir, exist_ok=True)
        file_name = list(id_.keys())[0]
        try:
            os.remove(file_name)
        except BaseException:
            pass
        try:
            url = list(id_.values())[0]
            logging.info('downloading:{}'.format(url))
            if self.run_env == 'local':
                proxies = 'http://127.0.0.1:12222'
            else:
                proxies = None
            async with aiohttp.ClientSession(read_timeout=self.time_out * 5) as session:
                async with session.get(url=url, headers={}, proxy=proxies) as response_:
                    content_type = (
                        response_.headers['Content-Type'].split('/'))[1]
                    file_size = int(response_.headers['content-length'])
                    if 'webp' in content_type:
                        try:
                            file_name = file_name + '.jpg'
                            output = os.path.join(self.root_dir, file_name)  # 带格式的全名
                            im = Image.open(BytesIO(await response_.read()))
                            im.save(output)
                        except:
                            file_name = file_name + '.png'
                            output = os.path.join(self.root_dir, file_name)  # 带格式的全名
                            im = Image.open(BytesIO(await response_.read()))
                            im.save(output)

                    else:
                        file_name = file_name + '.' + content_type
                        output = os.path.join(self.root_dir, file_name)  # 带格式的全名
                        logging.info(f"all video length:{file_size}")
                        if os.path.exists(output):
                            first_byte = os.path.getsize(output)
                        else:
                            first_byte = 0
                        if first_byte >= file_size:
                            return file_size
                        header = {"Range": f"bytes={first_byte}-{file_size}"}
                        pbar = tqdm(
                            total=file_size, initial=first_byte,
                            unit='B', unit_scale=True, desc=output)
                        await self.fetch_(session, url, output, pbar=pbar, headers=header, proxies=proxies)
                        logging.info(
                            'download  success :{}, filename:{}'.format(
                                url, file_name))
        except:
            print('error: {}'.format(list(id_.values())[0]))

    async def fetch_(self, session, url, output, pbar=None, headers=None, proxies=None):
        async with session.get(url, headers=headers, proxy=proxies) as req:
            with(open(output, 'ab')) as f:
                while True:
                    chunk = await req.content.read(1024 * 256)
                    if not chunk:
                        break
                    f.write(chunk)
                    pbar.update(1024 * 256)
            pbar.close()

    async def zip_files(self):
        zipf = zipfile.ZipFile(
            '{}_video_images.zip'.format(
                self.current_time), 'w')
        pre_len = len(os.path.dirname(self.root_dir))
        for parent, dir_names, file_names in os.walk(self.root_dir):
            for filename in file_names:
                path_file = os.path.join(parent, filename)
                arc_name = path_file[pre_len:].strip(os.path.sep)  # 相对路径
                zipf.write(path_file, arc_name)
        zipf.close()

    async def oss_upload(self):
        logging.info('oss upload start, please wait a min')
        auth = oss2.Auth('7JT8SvX8zSsudTIb', 'XawzZPcAXLdapOx5Rn1nptjD7r3Z5U')
        bucket = oss2.Bucket(auth, self.oss_endpoint, self.oss_bucket_name)
        file_ = '{}_video_images.zip'.format(self.current_time)
        filename = os.getcwd() + '/' + file_
        oss2.resumable_upload(
            bucket=bucket,
            key=file_,
            filename=filename,
            store=oss2.ResumableStore(
                root='/tmp'),
            multipart_threshold=4 *
            1024 *
            1024 *
            1024,
            part_size=1024 *
            1024 *
            1024,
            num_threads=1,
        )
        logging.info('oss upload stop: %s file is ok' % (file_,))
        bucket = oss2.Bucket(
            auth,
            self.oss_endpoint_commmon,
            self.oss_bucket_name)
        url = bucket.sign_url('GET', key=file_, expires=3600 * 24 * 10)
        logging.info('oss_url :\n {} \n'.format(url))

    async def start_main(self):
        """
        :func: 程序开始
        """
        self.time_int = int(
            time.mktime(
                time.strptime(
                    self.current_time,
                    '%Y%m%d')))
        ids = await self.get_mongo_urls()
        # print(json.dumps(ids))
        # ids = ids[:3]
        if ids:
            logging.info('lzy_oss_deal-ids length:{}'.format(len(ids)))
            ids_ = []
            for id_ in ids:
                if len(ids_) >= 10:  # 每次从内存中取固定size的id，去请求。
                    await self.start_(ids_)
                    ids_.clear()
                    ids_.append(id_)
                else:
                    ids_.append(id_)
            if ids_:
                await self.start_(ids_)
        else:
            logging.info('lzy_oss_deal-schedule ending!!!!')
        if os.path.isdir(self.root_dir):
            await self.zip_files()
            await self.oss_upload()

    async def lzy_main(self):
        """
        func：调度主程序
        """
        await self.start_main()
        logging.info('LZYOSSDeal().lzy_main()  running over!!! ')

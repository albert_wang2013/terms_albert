# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/8/5 10:20
@Author  : Albert
@Func    :
@File    : post.py
@Software: PyCharm
"""
import asyncio
import configparser
import hashlib
import logging
import os
import sys
import time

import aiofiles
from idataapi_transform import ProcessFactory, GetterConfig, WriterConfig

logging.basicConfig(level=logging.INFO)


class ConfigPar(object):
    """
    func:配置理解就行
    """

    def __init__(self):
        super().__init__()
        self.conf = configparser.ConfigParser()
        self.conf.read('config.ini')

    def config_func(self):
        max_sema = self.conf.get('lzy_term', 'max_sema')
        max_retries = self.conf.get('lzy_term', 'max_retries')
        host = self.conf.get('lzy_term', 'host')
        apikey = self.conf.get('lzy_term', 'apikey')
        get_id_mode = self.conf.get('lzy_term', 'get_id_mode')
        max_limit = self.conf.get('lzy_term', 'max_limit')
        req_host = self.conf.get('lzy_term', 'req_host')
        return int(max_sema), int(max_retries), host, apikey, get_id_mode, int(
            max_limit) if max_limit != 'None' else None, req_host


class PostCrawl():
    def __init__(self):
        super().__init__()
        config_list = ConfigPar().config_func()
        self.post = '{}/post/facebook?uid={}&size=20&apikey={}'
        self.con = config_list[0]
        self.max_retry = config_list[1]
        self.host = config_list[2]
        self.apikey = config_list[3]
        self.get_id_mode = config_list[4]
        self.max_limit = config_list[5]
        self.req_host = config_list[6]

    async def get_ids(self):
        file_dir = os.path.split(os.path.realpath(sys.argv[0]))[0]
        file = 'facebook_uids.txt'
        file_path = file_dir + '/' + file
        logging.info('file path is:{}'.format(file_path))
        async with aiofiles.open(file_path, encoding='utf8', mode='r') as f:
            ids_ = await f.readlines()
            ids = []
            for id_ in ids_:
                ids.append(id_.strip())
            return ids

    async def get_api_ids(self):
        pass

    async def start_(self, ids):
        logging.info('post-schedule-ids length:{}'.format(len(ids)))
        tasks = []
        for id_ in ids:
            url_post = self.post.format(self.req_host, id_, self.apikey)
            task1 = asyncio.ensure_future(self.request_post(url_post))
            tasks.append(task1)
        await asyncio.gather(*tasks)

    async def request_post(self, url_post):
        self.current_time = time.strftime("%Y%m%d", time.localtime())  # 当前时间
        self.time_int = int(
            time.mktime(
                time.strptime(
                    self.current_time,
                    '%Y%m%d')))
        api_config = GetterConfig.RAPIConfig(
            url_post,
            max_retry=self.max_retry,
            done_if=self.done_if,
            filter_=self.target_func)
        api_getter = ProcessFactory.create_getter(
            GetterConfig.RAPIBulkConfig(
                [api_config], concurrency=self.con))
        datas_list = []
        # print(self.max_retry, self.max_limit, self.con)
        async for items in api_getter:
            for item in items:
                if item['publishDate'] < self.time_int:     # 只将介于当天00点子昨天00点的数据返回,所以要先返回大于昨天00点
                    datas_list.append(item)
            await ProcessFactory.create_writer(self.mongo_sel('posts')).write([i for i in datas_list if i])

    def target_func(self, item):
        """
        :param item: 返回的是一次请求当中的所有items的其中一个item
        func: 判断item的发布时间是否在指定的时间内：是，返回；不是，扔掉
        """
        # logging.info('length of item are:{}'.format(len(item)))   # 判断字典长度
        if item['publishDate']:
            if (
                    self.time_int -
                    3600 *
                    24) <= int(
                    item['publishDate']) <= self.time_int:  # 只将介于当天00点子昨天00点的数据返回，所以返回当天00点前的
                return item

    def done_if(self, items):
        """
        func: 返回的是一次请求当中的所有items,判断一次请求当中最后一个item的发布时间是否在指定的时间内：是，继续翻页；不是，停止翻页
        """
        # logging.info('length of items are:{}'.format(len(items)))
        if items:
            if items[-1]["publishDate"]:
                if int(items[-1]["publishDate"]
                       ) <= int(self.time_int - 3600 * 24):
                    return True
                return False
        return True

    def mongo_sel(self, name_):
        col_name = 'lzy_facebook_{}_{}'.format(self.current_time, name_)
        mongo_ = WriterConfig.WMongoConfig(
            col_name,
            host=self.host,
            port=20020,
            username='wm_terms',
            password='qwer',
            database='wm',
            encoding='utf8',
            id_hash_func=self.id_hash_func)
        return mongo_

    @staticmethod
    def id_hash_func(item):
        if "id" in item and item["id"]:
            value = (item["id"]).encode("utf8")
        elif "subjectId" in item and item["subjectId"] and "objectId" in item and item["objectId"]:
            value = (item["subjectId"] + "_" + item["objectId"]).encode("utf8")
        else:
            value = str(item).encode("utf8")
        return hashlib.md5(value).hexdigest()

    async def start_main(self):
        """
        :func: 程序开始
        """
        if self.get_id_mode == 'file':
            ids = await self.get_ids()
        elif self.get_id_mode == 'api':
            ids = await self.get_api_ids()
        else:
            raise Exception("Invalid params error!")
        logging.info('post-schedule-ids length:{}'.format(len(ids)))
        ids_ = []
        for id_ in ids:
            if len(ids_) >= 100:  # 每次从内存中取固定size的id，去请求。
                await self.start_(ids_)
                ids_.clear()
                ids_.append(id_)
            else:
                ids_.append(id_)
        if ids_:
            await self.start_(ids_)

    async def post_main(self):
        """
        func：调度主程序
        """
        await self.start_main()
        logging.info('PostCrawl().post_main()  running over!!! ')

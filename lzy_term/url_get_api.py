# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/8/8 10:56
@Author  : Albert
@Func    :
@File    : url_get_api.py
@Software: PyCharm
"""
import configparser
import logging
import time
import tornado.ioloop
import tornado.web
import tornado.options
import oss2

logging.basicConfig(level=logging.INFO)


class ConfigPar(object):
    """
    func:配置理解就行
    """

    def __init__(self):
        super().__init__()
        self.conf = configparser.ConfigParser()
        self.conf.read('config.ini')

    def config_func(self):
        oss_endpoint = self.conf.get('lzy_term', 'oss_endpoint')
        oss_endpoint_commmon = self.conf.get(
            'lzy_term', 'oss_endpoint_commmon')
        oss_bucket_name = self.conf.get('lzy_term', 'oss_bucket_name')
        return oss_endpoint, oss_endpoint_commmon, oss_bucket_name


class APIGET():
    def __init__(self, date):
        super().__init__()
        config_list = ConfigPar().config_func()
        self.date = date
        self.oss_endpoint = config_list[0]
        self.oss_endpoint_commmon = config_list[1]
        self.oss_bucket_name = config_list[2]

    async def deal_url(self):
        auth = oss2.Auth('7JT8SvX8zSsudTIb', 'XawzZPcAXLdapOx5Rn1nptjD7r3Z5U')
        bucket = oss2.Bucket(
            auth,
            self.oss_endpoint_commmon,
            self.oss_bucket_name)
        files = ['{}_datas.zip'.format(self.date),
                 '{}_video_images.zip'.format(self.date),
                 'profiles_relations_datas.zip']
        urls_json = {'data': []}
        for file_ in files:
            url = bucket.sign_url('GET', key=file_, expires=3600 * 24 * 10)
            url_json = {
                '{}'.format(file_): url
            }
            urls_json['data'].append(url_json)
        return urls_json


""" 启动tornado服务监控 """


class MainHandler(tornado.web.RequestHandler):
    """
    启动tornado服务监控
    """
    async def get(self):
        date = self.get_argument('date', None)
        if not date:
            date = time.strftime("%Y%m%d", time.localtime())  # 当前时间
        logging.info('uri:{}, date:{}'.format(self.request.uri, date))
        spider_ = APIGET(date)
        data = await spider_.deal_url()
        self.write(data)
        logging.info(
            "GET API for url,current time:{}".format(
                time.strftime("%Y%m%d %X", time.localtime())))


if __name__ == '__main__':
    """
    http://localhost:20022/download_url_json?date=20190808
    http://183.17.229.113:20022/download_url_json?date=20190808
    """
    logging.info('program  running!!!')

    application = tornado.web.Application([
        (r"/download_url_json", MainHandler),
    ])
    application.listen(20022, address='0.0.0.0')

    tornado.ioloop.IOLoop.instance().start()

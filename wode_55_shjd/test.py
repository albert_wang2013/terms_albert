

# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/5/27 11:52
@Author  : Albert
@Func    : 
@File    : redis_data_to_txt.py
@Software: PyCharm
"""
import time
from datetime import datetime, timedelta
import logging
import random
import re
import asyncio
from idataapi_transform import ProcessFactory, GetterConfig, WriterConfig
from idataapi_transform.DataProcess.Config.ConfigUtil.GetterConfig import RRedisConfig
from tornado import escape

""" 无该条件，就无法输出到控制台 """
logging.basicConfig(level=logging.INFO)



async def redis_del(posts):
    cli = await posts.get_redis_pool_cli()

    await cli.delete('posts')
    logging.info('del ok')



async def redis_json():
    for i, j in data_list.items():
        redis_getter = ProcessFactory.create_getter(j, )
        json_config = WriterConfig.WJsonConfig("facebook_{}.json".format(i), keep_other_fields=True)
        with ProcessFactory.create_writer(json_config) as json_writer:
            async for items in redis_getter:
                logging.info('length items:{}'.format(len(items)))
                for item in items:  # 一行一行的写进去
                    if 'facebook' in item['interface']:
                        json_writer.write([item])


async def test(comments):       # 获取id
    redis_getter = ProcessFactory.create_getter(comments)
    async for items in redis_getter:
        logging.info('length items:{}'.format(len(items)))
        for i in items:
            ids = [j['commenterId'] for j in i['data']]
            print(ids)
            print(len(ids))





if __name__ == '__main__':
    max_ = 10000000
    per_ = 30
    posts = GetterConfig.RRedisConfig('posts', host='127.0.0.1', port=6379, db=2,
                                      encoding='utf8', max_limit=max_, per_limit=per_)  # 采集数据后默认保存地址及db
    comments = GetterConfig.RRedisConfig('comments', host='127.0.0.1', port=6379, db=2,
                                         encoding='utf8', max_limit=max_, per_limit=per_)  # 采集数据后默认保存地址及db
    shares = GetterConfig.RRedisConfig('shares', host='127.0.0.1', port=6379, db=2,
                                       encoding='utf8', max_limit=max_, per_limit=per_)  # 采集数据后默认保存地址及db
    relations = GetterConfig.RRedisConfig('relations', host='127.0.0.1', port=6379, db=2,
                                       encoding='utf8', max_limit=max_, per_limit=per_)  # 采集数据后默认保存地址及db
    profiles = GetterConfig.RRedisConfig('profiles', host='127.0.0.1', port=6379, db=2,
                                         encoding='utf8', max_limit=max_, per_limit=per_)  # 采集数据后默认保存地址及db
    data_list = dict()
    data_list['posts'] = posts
    data_list['comments'] = comments
    # data_list['relations'] = relations
    # data_list['profiles'] = profiles
    loop = asyncio.get_event_loop()
    loop.run_until_complete(redis_json())
    # loop.run_until_complete(test(comments))
    # loop.run_until_complete(redis_del(posts))
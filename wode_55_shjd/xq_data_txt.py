# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/7/2 17:29
@Author  : Albert
@Func    : 把数据从mongo取出来，并且分类文件夹的形式存储txt文件！
@File    : xq_data_txt.py
@Software: PyCharm
"""
import json
import os
import time
import zipfile
from datetime import datetime, timedelta
import logging
import random
import re
import asyncio
import aiofiles
import oss2
from idataapi_transform import ProcessFactory, GetterConfig, WriterConfig

""" 无该条件，就无法输出到控制台 """
logging.basicConfig(level=logging.INFO)


class DownData():
    def __init__(self):
        collections = 'facebook'
        self.data_list = dict()
        # self.host = '123.196.116.97'    # 127.0.0.1
        self.host = '127.0.0.1'    # 127.0.0.1
        self.max_ = None
        self.per_ = 10000
        self.file_ = 'xq0107'
        self.ids = [
            '46251501064',
            '1863023523934803',
            '491399324358361',
        ]
        for i in ['post', 'comment', 'profile', 'liker', 'share', 'relation']:
        # for i in ['profile']:
            self.data_list['{}_{}'.format(collections,
                                          i)] = GetterConfig.RMongoConfig('{}_{}'.format(collections,
                                                                                         i),
                                                                          host=self.host,
                                                                          port=20020,
                                                                          username='wm_terms',
                                                                          password='qwer',
                                                                          database='wm',
                                                                          encoding='utf8',
                                                                          per_limit=self.per_,
                                                                          max_limit=self.max_)

    async def read_mongo(self):
        for i, j in self.data_list.items():
            redis_getter = ProcessFactory.create_getter(j, )
            async for items in redis_getter:
                for item in items:
                    item.pop('_id')
                    if 'post' in i:
                        await self.deal_posts(item)
                    elif 'comment' in i:
                        await self.deal_comments(item)
                    elif 'share' in i:
                        await self.deal_shares(item)
                    elif 'liker' in i:
                        await self.deal_likers(item)
                    elif 'profile' in i:
                        await self.deal_profiles(item)
                    elif 'relation' in i:
                        await self.deal_relations(item)
                    else:
                        pass

    async def deal_relations(self, i):
        file_ = os.getcwd()
        file_1 = file_ + '/{}'.format(self.file_)
        if not os.path.exists(file_1):  # 判断当前路径是否存在，没有则创建file_1文件夹
            os.makedirs(file_1)
        file_post = file_1 + '/relations.txt'
        async with aiofiles.open(file=file_post, mode='a+', encoding='utf8') as f:
            await f.write(str(i))
            await f.flush()

    async def deal_posts(self, i):
        file_ = os.getcwd()
        file_1 = file_ + '/{}/'.format(self.file_) + i['posterId']
        if not os.path.exists(file_1):  # 判断当前路径是否存在，没有则创建file_1文件夹
            os.makedirs(file_1)
        file_post = file_1 + '/' + '{}_post.txt'.format(i['posterId'])
        async with aiofiles.open(file=file_post, mode='a+', encoding='utf8') as f:
            await f.write(str(i))
            await f.flush()

    async def deal_comments(self, i):
        file_ = os.getcwd()
        comment_postid = i['referId'].split('_')[0]
        file_1 = file_ + '/{}/'.format(self.file_) + comment_postid
        if not os.path.exists(file_1):  # 判断当前路径是否存在，没有则创建file_1文件夹
            os.makedirs(file_1)
        file_comment = file_1 + '/' + \
            '{}_comment.txt'.format(comment_postid)
        async with aiofiles.open(file=file_comment, mode='a+', encoding='utf8') as f:
            await f.write(str(i))
            await f.flush()

    async def deal_shares(self, i):
        file_ = os.getcwd()
        share_postid = i['posterId'].split('_')[0]
        file_1 = file_ + '/{}/'.format(self.file_) + share_postid
        if not os.path.exists(file_1):  # 判断当前路径是否存在，没有则创建file_1文件夹
            os.makedirs(file_1)
        file_share = file_1 + '/' + '{}_share.txt'.format(share_postid)
        async with aiofiles.open(file=file_share, mode='a+', encoding='utf8') as f:
            await f.write(str(i))
            await f.flush()

    async def deal_likers(self, i):
        file_ = os.getcwd()
        liker_postid = i['posterId'].split('_')[0]
        file_1 = file_ + '/{}/'.format(self.file_) + liker_postid
        if not os.path.exists(file_1):  # 判断当前路径是否存在，没有则创建file_1文件夹
            os.makedirs(file_1)
        file_liker = file_1 + '/' + '{}_liker.txt'.format(liker_postid)
        async with aiofiles.open(file=file_liker, mode='a+', encoding='utf8') as f:
            await f.write(str(i))
            await f.flush()

    async def deal_profiles(self, i):
        file_ = os.getcwd()
        profile_postid = i['id'].split('_')[0]
        if profile_postid in self.ids:
            file_1 = file_ + '/{}/'.format(self.file_) + profile_postid
            if not os.path.exists(file_1):  # 判断当前路径是否存在，没有则创建file_1文件夹
                os.makedirs(file_1)
            file_profile = file_1 + '/' + '{}_profile.txt'.format(profile_postid)
        else:
            file_1 = file_ + '/{}'.format(self.file_)
            if not os.path.exists(file_1):  # 判断当前路径是否存在，没有则创建file_1文件夹
                os.makedirs(file_1)
            file_profile = file_1 + '/profiles.txt'
        async with aiofiles.open(file=file_profile, mode='a+', encoding='utf8') as f:
            await f.write(str(i))
            await f.flush()

    async def start(self):
        await self.read_mongo()
        await self.zip_files()
        await self.oss_deal()

    async def zip_files(self):
        zipf = zipfile.ZipFile(
            '{}.zip'.format(self.file_), 'w')
        file_ = os.getcwd() + '/{}'.format(self.file_)
        pre_len = len(os.path.dirname(file_))
        for parent, dir_names, file_names in os.walk(file_):
            for filename in file_names:
                path_file = os.path.join(parent, filename)
                arc_name = path_file[pre_len:].strip(os.path.sep)  # 相对路径
                zipf.write(path_file, arc_name)
        zipf.close()

    async def oss_deal(self):
        logging.info('oss upload start, please wait a min')
        auth = oss2.Auth('7JT8SvX8zSsudTIb', 'XawzZPcAXLdapOx5Rn1nptjD7r3Z5U')
        bucket = oss2.Bucket(
            auth,
            'oss-cn-shenzhen.aliyuncs.com',
            'common-data')
        # file_ = '{}.zip'.format(self.file_)
        file_ = '{}.zip'.format('profiles_relations_20200327_2')
        filename = os.getcwd() + '/' + file_
        print(filename)
        oss2.resumable_upload(
            bucket=bucket,
            key=file_,
            filename=filename,
            store=oss2.ResumableStore(
                root='/tmp'),
            multipart_threshold=4 *
            1024 *
            1024 *
            1024,
            part_size=1024 *
            1024 *
            1024,
            num_threads=1,
        )
        logging.info('oss upload stop: %s file is ok' % (file_,))
        bucket = oss2.Bucket(
            auth,
            'oss-cn-shenzhen.aliyuncs.com', 'common-data')
        url = bucket.sign_url('GET', key=file_, expires=3600 * 24 * 10)
        logging.info('oss_url :\n {} \n'.format(url))

        # files = [
        #     'profiles_relations_20200327.zip'
        # ]
        # for file_ in files:
        #     bucket = oss2.Bucket(
        #         auth,
        #         'oss-cn-shenzhen.aliyuncs.com', 'common-data')
        #     url = bucket.sign_url('GET', key=file_, expires=3600 * 24 * 10)
        #     logging.info('oss_url :\n {} \n'.format(url))


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(DownData().start())

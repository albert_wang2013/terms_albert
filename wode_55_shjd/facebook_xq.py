# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/5/27 10:13
@Author  : Albert
@Func    :
@File    : facebook_xq.py
@Software: PyCharm
"""
import hashlib
import time
from datetime import datetime, timedelta
import logging
import random
import re
import asyncio
from idataapi_transform import ProcessFactory, GetterConfig, WriterConfig
from tornado import escape

""" 无该条件，就无法输出到控制台 """
logging.basicConfig(level=logging.INFO)


class post_commentCrawl():
    def __init__(self, type_):
        if type_ == 'facebook':
            # self.post = 'https://api02.bitspaceman.com/post/facebook?kw={}&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
            self.post = 'https://api02.bitspaceman.com/post/facebook?uid={}&size=20&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
            self.emo = 'https://api02.bitspaceman.com/post/facebook?type=4&pid={}&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
            self.comment = 'https://api02.bitspaceman.com/comment/facebook?id={}&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
            self.share = 'https://api02.bitspaceman.com/post/facebook?type=3&pid={}&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
            self.like = 'https://api02.bitspaceman.com/post/facebook?type=2&pid={}&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
            self.profile = 'https://api02.bitspaceman.com/profile/facebook?id={}&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
            self.relation = 'https://api02.bitspaceman.com/relation/facebook?type=friends&uid={}&apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'
            self.con_ = 15  # 设置并发
        self.post_inter = '{}_'.format(type_)
        sel = 'local'
        if sel == 'server':
            self.host = '127.0.0.1'
        elif sel == 'local':
            self.host = '123.196.116.97'
        self.max_limit = None

    def done_if(self, items):
        """
        the APIGetter will automatically fetch next page until max_limit or no more page,
        if you provide a function, APIGetter will terminate fetching next page when done_if(items) return True
        :return: 返回的是一次请求当中的所有items
        func: 如果你提供一个函数，当done_if（items）返回True时，APIGetter将终止获取下一页
        """
        if items:
            if items[-1]["publishDate"]:
                if int(items[-1]["publishDate"]) <= 1577203200:     # 1542988800
                    return True
                else:
                    return False
            return True
        return True

    def mongo_sel(self, name_):
        col_name = self.post_inter + name_
        mongo_ = WriterConfig.WMongoConfig(
            col_name,
            host=self.host,
            port=20020,
            username='wm_terms',
            password='qwer',
            database='wm',
            encoding='utf8',
            id_hash_func=self.id_hash_func(col_name)
        )
        return mongo_

    # @staticmethod
    # def id_hash_func(item):
    #     if "id" in item and item["id"]:
    #         value = (item["id"]).encode("utf8")
    #         return hashlib.md5(value).hexdigest()

    @staticmethod
    def id_hash_func(col_name):
        def inner(item):
            if 'post' in col_name:
                value = (item["id"] + "_" + item["posterId"]).encode("utf8")
            elif 'comment' in col_name:
                value = (item["id"] + "_" + item["commenterId"]).encode("utf8")
            elif 'profile' in col_name:
                value = (item["id"] + "_" + item["idType"]).encode("utf8")
            elif 'share' in col_name:
                value = (item["sharerId"] + "_" + item["posterId"]).encode("utf8")
            elif 'like' in col_name:
                value = (item["likerId"] + "_" + item["posterId"]).encode("utf8")
            elif 'relation' in col_name:
                value = (item["objectId"] + "_" + item["subjectId"]).encode("utf8")
            else:
                value = item["id"].encode("utf8")
            return value
        return inner



    async def get_mongo_ids(self):
        col_name = 'lzy_facebook_profiles'
        mongo_config = GetterConfig.RMongoConfig(
            col_name,
            host=self.host,
            port=20020,
            username='wm_terms',
            password='qwer',
            database='wm',
            encoding='utf8',
            per_limit=1000,
            max_limit=self.max_limit)
        mongo_getter = ProcessFactory.create_getter(mongo_config)
        ids = []
        async for items in mongo_getter:
            for item in items:
                id_ = item['id']
                ids.append(id_)
        return ids

    async def get_mongo_user_ids(self):
        col_name = 'facebook_comment'
        mongo_config = GetterConfig.RMongoConfig(
            col_name,
            host=self.host,
            port=20020,
            username='wm_terms',
            password='qwer',
            database='wm',
            encoding='utf8',
            per_limit=10000,
            max_limit=self.max_limit)
        mongo_getter = ProcessFactory.create_getter(mongo_config)
        ids = set()
        async for items in mongo_getter:
            for item in items:
                id_ = item['commenterId']
                ids.add(id_)
        return list(ids)

    async def deal_post(self, kws):
        urls_ = []
        for i in kws:
            url = self.post.format(i)
            url_generator = GetterConfig.RAPIConfig(
                url, max_retry=5, max_limit=self.max_limit, done_if=self.done_if)  # , max_limit=10
            urls_.append(url_generator)
        datas_list = []
        getter = ProcessFactory.create_getter(GetterConfig.RAPIBulkConfig(
            urls_, concurrency=self.con_))  # 采集文章
        if getter:
            items = []
            async for ds in getter:
                for d in ds:  # 1542988800
                    if d['publishDate'] >= 1577203200:
                        items.append(d)
            if items:
                tasks = [self.emo_func(item) for item in items]
                items = await asyncio.gather(*tasks)
            if items:
                """ 第1次 post存储"""
                for item in items:  # 存数据
                    if len(datas_list) >= 50:  # 每获取50条数据，存储一次进redis
                        await ProcessFactory.create_writer(self.mongo_sel('post')).write([i for i in datas_list if i])
                        logging.info(
                            'writer post-items to mongo length:{}'.format(len(datas_list)))
                        datas_list.clear()
                        datas_list.append(item)
                    else:
                        datas_list.append(item)
                if datas_list:
                    await ProcessFactory.create_writer(self.mongo_sel('post')).write([i for i in datas_list if i])
                    logging.info(
                        'writer post-items to mongo length:{}'.format(len(datas_list)))
                    datas_list.clear()

    async def start(self, kws):
        # 第一次处理
        tasks1 = [
            self.deal_post(kws),
            self.profile_func(kws),
        ]
        # await asyncio.gather(*tasks1)
        # 第二次处理
        ids = await self.get_mongo_ids()
        print(ids)
        logging.info('ids length:{}'.format(len(ids)))
        # tasks2 = [
        #     self.comment_del(ids),
        #     self.share_func(ids),
        #     self.like_func(ids),
        # ]
        # await asyncio.gather(*tasks2)
        # # 第三次处理
        # user_ids = await self.get_mongo_user_ids()
        # user_ids += kws     # 将原本的也添加进去
        # logging.info('ids length:{}'.format(len(user_ids)))
        # tasks3 = [
        #     self.profile_func(user_ids),
        #     self.relations_func(user_ids),
        # ]
        # await asyncio.gather(*tasks3)

    async def profile_func(self, profile_list):
        urls_ = []
        for i in profile_list:
            url = self.profile.format(i)
            url_generator = GetterConfig.RAPIConfig(
                url,
                max_retry=5,
                random_min_sleep=0,
                random_max_sleep=1,
                max_limit=self.max_limit)  # , max_limit=10
            urls_.append(url_generator)
        getter = ProcessFactory.create_getter(
            GetterConfig.RAPIBulkConfig(
                urls_, concurrency=self.con_))  # 采集文章
        if getter:
            async for items in getter:
                await ProcessFactory.create_writer(self.mongo_sel('profile')).write(items)
                logging.info(
                    'writer profile_inter-items to mongo length:{}'.format(len(items)))

    async def like_func(self, like_list):
        urls_ = []
        for i in like_list:
            url = self.like.format(i)
            url_generator = GetterConfig.RAPIConfig(
                url,
                max_retry=3,
                random_min_sleep=0,
                random_max_sleep=1,
                max_limit=self.max_limit)  # , max_limit=10
            urls_.append(url_generator)
        datas_list = []
        getter = ProcessFactory.create_getter(
            GetterConfig.RAPIBulkConfig(
                urls_, concurrency=self.con_))  # 采集文章
        if getter:
            async for items in getter:
                for item in items:  # 存数据
                    if len(datas_list) >= 50:  # 每获取50条数据，存储一次进redis
                        await ProcessFactory.create_writer(self.mongo_sel('liker')).write([i for i in datas_list if i])
                        logging.info(
                            'writer liker_inter-items to mongo length:{}'.format(len(datas_list)))
                        datas_list.clear()
                        datas_list.append(item)
                    else:
                        datas_list.append(item)
                if datas_list:
                    await ProcessFactory.create_writer(self.mongo_sel('liker')).write([i for i in datas_list if i])
                    logging.info(
                        'writer liker_inter-items to mongo length:{}'.format(len(datas_list)))
                    datas_list.clear()

    async def share_func(self, share_list):
        urls_ = []
        for i in share_list:
            url = self.share.format(i)
            url_generator = GetterConfig.RAPIConfig(
                url,
                max_retry=3,
                # random_min_sleep=0,
                # random_max_sleep=1,
                max_limit=self.max_limit)  # , max_limit=10
            urls_.append(url_generator)
        datas_list = []
        getter = ProcessFactory.create_getter(
            GetterConfig.RAPIBulkConfig(
                urls_, concurrency=self.con_))  # 采集文章
        if getter:
            async for items in getter:
                for item in items:  # 存数据
                    if len(datas_list) >= 50:  # 每获取50条数据，存储一次进redis
                        await ProcessFactory.create_writer(self.mongo_sel('share')).write([i for i in datas_list if i])
                        logging.info(
                            'writer share_inter-items to mongo length:{}'.format(len(datas_list)))
                        datas_list.clear()
                        datas_list.append(item)
                    else:
                        datas_list.append(item)
                if datas_list:
                    await ProcessFactory.create_writer(self.mongo_sel('share')).write([i for i in datas_list if i])
                    logging.info(
                        'writer share_inter-items to mongo length:{}'.format(len(datas_list)))
                    datas_list.clear()

    async def emo_func(self, data):
        pid = data['id']
        url = self.emo.format(pid)
        getter = ProcessFactory.create_getter(GetterConfig.RAPIBulkConfig(
            [GetterConfig.RAPIConfig(url, max_retry=1)], concurrency=self.con_))
        if getter:
            async for items in getter:
                for item in items:
                    data['reactions'] = item['reactions']   # 表情
                    return data

    async def comment_del(self, comments_list):
        urls_ = []
        for i in comments_list:
            url = self.comment.format(i)
            url_generator = GetterConfig.RAPIConfig(
                url, max_retry=3, max_limit=self.max_limit)  # , max_limit=10
            urls_.append(url_generator)
        datas_list = []
        getter = ProcessFactory.create_getter(
            GetterConfig.RAPIBulkConfig(
                urls_, concurrency=self.con_))  # 采集文章
        if getter:
            async for items in getter:
                for item in items:  # 存数据
                    if len(datas_list) >= 50:   # 每获取50条数据，存储一次进redis
                        await ProcessFactory.create_writer(self.mongo_sel('comment')).write([i for i in datas_list if i])
                        logging.info(
                            'writer comment-items to mongo length:{}'.format(len(datas_list)))
                        datas_list.clear()
                        datas_list.append(item)
                    else:
                        datas_list.append(item)
                if datas_list:
                    await ProcessFactory.create_writer(self.mongo_sel('comment')).write([i for i in datas_list if i])
                    logging.info(
                        'writer comment-items to redis length:{}'.format(len(datas_list)))
                    datas_list.clear()


    async def relations_func(self, user_ids):
        urls_ = []
        for i in user_ids:
            url = self.relation.format(i)
            url_generator = GetterConfig.RAPIConfig(
                url, max_retry=3, max_limit=self.max_limit)  # , max_limit=10
            urls_.append(url_generator)
        datas_list = []
        getter = ProcessFactory.create_getter(
            GetterConfig.RAPIBulkConfig(
                urls_, concurrency=self.con_))  # 采集文章
        if getter:
            async for items in getter:
                for item in items:  # 存数据
                    if len(datas_list) >= 50:   # 每获取50条数据，存储一次进redis
                        await ProcessFactory.create_writer(self.mongo_sel('relation')).write([i for i in datas_list if i])
                        logging.info(
                            'writer relation-items to mongo length:{}'.format(len(datas_list)))
                        datas_list.clear()
                        datas_list.append(item)
                    else:
                        datas_list.append(item)
                if datas_list:
                    await ProcessFactory.create_writer(self.mongo_sel('relation')).write([i for i in datas_list if i])
                    logging.info(
                        'writer relation-items to redis length:{}'.format(len(datas_list)))
                    datas_list.clear()


if __name__ == '__main__':
    # with open('/root/ids.txt', 'r') as f:
    #     ids = f.readlines()
    #     kws = [i.strip().replace('\n', '') for i in ids]
    kws = [
        '46251501064',
        '1863023523934803',
        '491399324358361',
    ]
    type_ = 'facebook'  # weibo  facebook
    loop = asyncio.get_event_loop()
    loop.run_until_complete(post_commentCrawl(type_=type_).start(kws))
